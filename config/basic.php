<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Basic 64 Authentication
    |--------------------------------------------------------------------------
    |
    | Basic 64 Authentication config.
    |
    */

    'active' => env('BASIC_ACTIVE', false),
    'username' => env('BASIC_USER', ''),
    'password' => env('BASIC_PASSWORD', ''),
];
