<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Faceswap
    |--------------------------------------------------------------------------
    |
    | Faceswap Config Path.
    |
    */

    'python_path'           => env('PYTHON_PATH', "/root/virtual_env/venv_with_python3.10/bin/"),
    'faceswap_app_path'     => env('FACE_SWAP_APP_PATH', "/root/roop/"),
];
