<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductNote extends Model
{
    protected $table = 'product_notes';

    protected $fillable = ['id', 'product_id', 'content', 'is_important', 'created_at', 'updated_at'];
}
