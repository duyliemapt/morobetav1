<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeoFormat extends BaseModel
{
    protected $table = 'seo_formats';

    protected $fillable = ['id', 'type', 'title', 'keywords', 'description', 'created_at', 'updated_at'];
}
