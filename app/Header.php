<?php

namespace App;

class Header extends BaseModel
{
    protected $table = 'headers';

    protected $fillable = ['id', 'uri', 'title', 'image', 'color', 'type', 'is_activated', 'is_deleted', 'created_at', 'updated_at'];
}
