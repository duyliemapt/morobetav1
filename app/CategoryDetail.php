<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryDetail extends Model
{
    protected $table = 'category_details';

    protected $fillable = ['id', 'article_id', 'category_id', 'created_at', 'updated_at'];

    public function category() {
        return $this->belongsTo(Category::class, 'category_id', 'id')->select('id', 'name', 'slug', 'parent_id')->where('is_deleted', false);
    }
}
