<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductDetail extends BaseModel
{
    protected $table = 'product_details';

    protected $fillable = ['id', 'product_id', 'sku', 'slug', 'code', 'price', 'position', 'is_activated', 'is_deleted', 'created_at', 'updated_at'];

    public function option_detail() {
        return $this->hasMany(ProductDetailOptionDetail::class, 'product_detail_id', 'id')->with('detail');
    }

    public function product() {
        return $this->belongsTo(Product::class, 'product_id', 'id')->with('manufacturer')->select('id', 'manufacturer_id', 'name', 'slug', 'image', 'regular_price_from', 'regular_price_to', 'price_from', 'price_to', 'discount', 'standard_construction_fee', 'shipping_cost', 'note', 'possible_delivery_time');
    }

    public function short_option_detail() {
        return $this->hasMany(ProductDetailOptionDetail::class, 'product_detail_id', 'id')->with('short_detail')->select('id', 'product_detail_id', 'product_option_detail_id');
    }
}
