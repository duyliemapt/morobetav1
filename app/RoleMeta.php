<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleMeta extends Model
{
    protected $table = 'role_meta';

    protected $fillable = ['id', 'role_id', 'description', 'created_at', 'updated_at'];
}
