<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    use HasFactory;

    protected $fillable;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function scopeIsDeleted($query)
    {
        return $query->where('is_deleted', true);
    }

    public function scopeNotDeleted($query)
    {
        return $query->where('is_deleted', false);
    }

    public function scopeIsActivated($query)
    {
        return $query->where('is_activated', true);
    }

    public function scopeNotActivated($query)
    {
        return $query->where('is_activated', false);
    }

    public function scopeIsPublished($query)
    {
        return $query->where('is_deleted', false)->where('is_activated', true);
    }
}
