<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryManufacturer extends Model
{
    protected $table = 'category_manufacturers';

    protected $fillable = ['id', 'category_id', 'manufacturer_id', 'created_at', 'updated_at'];

    public function manufacturer_detail() {
        return $this->belongsTo(Manufacturer::class, 'manufacturer_id', 'id');
    }

    public function category() {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
}
