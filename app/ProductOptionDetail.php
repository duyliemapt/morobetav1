<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductOptionDetail extends Model
{
    protected $table = 'product_option_details';

    protected $casts = [
        'is_check' => 'boolean'
    ];

    protected $fillable = ['id', 'product_option_id', 'name', 'slug', 'image', 'description', 'position', 'is_check', 'created_at', 'updated_at'];

    public function option()
    {
        return $this->belongsTo(ProductOption::class, 'product_option_id', 'id');
    }
}
