<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductDetailOptionDetail extends Model
{
    protected $table = 'product_detail_option_details';

    protected $fillable = ['id', 'product_detail_id', 'product_option_detail_id', 'created_at', 'updated_at'];

    public function detail() {
        return $this->belongsTo(ProductOptionDetail::class, 'product_option_detail_id', 'id')->with('option');
    }

    public function short_detail() {
        return $this->belongsTo(ProductOptionDetail::class, 'product_option_detail_id', 'id')->select('id', 'product_option_id');
    }
}
