<?php

namespace App;

class Menu extends BaseModel
{
    protected $table = 'menus';

    protected $fillable = ['id', 'name', 'slug', 'type', 'link', 'category_id', 'parent_id', 'position', 'order', 'is_activated', 'is_deleted', 'created_at', 'updated_at'];

    public function slider() {
        return $this->hasMany(MenuSlider::class, 'menu_id', 'id')->select('id', 'menu_id', 'image', 'title', 'description', 'link');
    }
}
