<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug', 'first_name', 'last_name', 'full_name', 'furigana_name', 'furigana_first_name', 'furigana_last_name',
        'username', 'email', 'email2', 'password', 'avatar', 'gender', 'career', 'date_of_birth', 'age', 'post_code', 'city', 'district', 'address', 'address2',
        'phone_number', 'fax_number', 'mobile_tel', 'emg_tel', 'introduction', 'reminder_question', 'reminder_answer', 'line_id', 'point', 'point_rate',
        'reset_password_code', 'reset_password_time', 'email_verified_at', 'email_subscribe', 'email_format', 'login_url',
        'is_temp', 'is_activated', 'is_deleted', 'created_by', 'created_at', 'updated_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
