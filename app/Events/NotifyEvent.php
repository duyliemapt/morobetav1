<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use App\Notify;
use App\Contact;

class NotifyEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $notify;

    /**
     * Create a new event instance.
     * @param $notify
     *
     * @return void
     */
    public function __construct()
    {
        $notifyCount = Notify::where('is_check', false)->count();
        $notifyTop = Notify::select('*')->orderBy('created_at', 'DESC')->paginate(15);
        $contactCount = Contact::where('is_read', false)->where('is_deleted', false)->count();
        $this->notify = [
            'all'     => $notifyCount,
            'contact' => $contactCount,
            'list'    => $notifyTop
        ];
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('notify.all');
    }
}
