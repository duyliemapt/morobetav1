<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $table = 'product_images';

    protected $fillable = ['id', 'product_id', 'name', 'file_name', 'position', 'created_at', 'updated_at'];
}
