<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends BaseModel
{
    protected $table = 'files';

    protected $fillable = ['id', 'name', 'slug', 'file', 'file_type', 'extension', 'size', 'width', 'height', 'important', 'alt', 'description', 'type', 'parent_id', 'is_deleted', 'created_at', 'updated_at'];

    public function parent()
    {
        return $this->belongsTo(Media::class, 'parent_id', 'id')->select('id', 'name', 'slug', 'file', 'type', 'parent_id')->where('is_deleted', false);
    }

    public function item()
    {
        return $this->hasMany(Media::class, 'parent_id', 'id')->select('id', 'name', 'slug', 'file', 'file_type', 'extension', 'size', 'width', 'height', 'important', 'alt', 'description', 'type', 'parent_id')->where('is_deleted', false);
    }
}
