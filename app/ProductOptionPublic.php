<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductOptionPublic extends Model
{
    protected $table = 'product_options';

    protected $appends = ['is_show', 'value'];

    protected $casts = [
        'is_check_all'     => 'boolean',
        'is_indeterminate' => 'boolean',
    ];

    protected $fillable = ['id', 'product_id', 'name', 'slug', 'title', 'description', 'position', 'allow_option', 'is_template', 'is_check_all', 'is_indeterminate', 'created_at', 'updated_at'];

    public function details()
    {
        return $this->hasMany(ProductOptionDetail::class, 'product_option_id', 'id')->where('is_check', true);
    }

    public function getIsShowAttribute()
    {
        return true;
    }

    public function getValueAttribute()
    {
        return "";
    }
}
