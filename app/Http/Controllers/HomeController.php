<?php

namespace App\Http\Controllers;

use App\ProductDetail;
use Barryvdh\DomPDF\Facade\Pdf;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Stripe\Exception\SignatureVerificationException;
use Stripe\StripeClient;
use Stripe\Webhook;
use App\Seo;
use Cache;

/**
 * Class HomeController
 *
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    public function index(Request $request)
    {
        /*$seoMeta = Seo::where('uri', $uri)->first();
        $cacheName = str_replace("/", "-", trim($uri));
        $seoMeta = Cache::remember($cacheName, 30, function() use ($uri) {
            return Seo::where('uri', $uri)->first();
        });*/

        $uri = $request->getRequestUri();
        $uri = urldecode($uri);
        $uri = $uri . "||";
        $uri = str_replace("//", "/", "/" . $uri);
        $uri = str_replace("/?", "?", $uri);
        $uri = str_replace("?&", "?", $uri);
        $uri = str_replace("&&", "&", $uri);
        $uri = str_replace("/||", "", $uri);
        $uri = str_replace("||", "", $uri);
        $uri = preg_replace('/(?:&|(\?))page=[^&]*(?(1)&|)?/i', "$1", $uri);
        if ($uri == null || $uri == "") $uri = "/";

        $seoMeta = Seo::where('uri', $uri)->first();
        if (!$seoMeta && strpos($uri, "/column/detail/") !== false) {
            $idPostArr = str_replace("/column/detail/", "", $uri);
            $idPostArr = explode("/", $idPostArr);
            $idPost = $idPostArr[0];
            $seoMeta = Seo::where('uri', 'like', "/column/detail/" . $idPost . "/%")->first();
        } else if (!$seoMeta && strpos($uri, "/bbs/detail/") !== false) {
            $idPostArr = str_replace("/bbs/detail/", "", $uri);
            $idPostArr = explode("/", $idPostArr);
            $idPost = $idPostArr[0];
            $seoMeta = Seo::where('uri', 'like', "/bbs/detail/" . $idPost . "/%")->first();
        }

        if (isset($seoMeta) && $seoMeta->id != null) {
            $data['title'] = $seoMeta->title;
            $data['keywords'] = $seoMeta->keywords;
            $data['description'] = $seoMeta->description;
            $imgSeo = ($seoMeta->image != "" && $seoMeta->image != null) ? $seoMeta->image : 'favicon-16x16.png';
            $data['image'] = url("files/" . $imgSeo);
        } else {
            $data = ['title' => config('settings.site_name'), 'keywords' => '', 'description' => '', 'image' => url('favicon-16x16.png')];
        }

        return view('home', $data);
    }

    public function note(Request $request)
    {
        $uri = $request->getRequestUri();
        $uri = urldecode($uri);
        $uri = $uri . "||";
        $uri = str_replace("/profile/", "", $uri);
        $uri = str_replace("//", "/", "/" . $uri);
        $uri = str_replace("/?", "?", $uri);
        $uri = str_replace("?&", "?", $uri);
        $uri = str_replace("&&", "&", $uri);
        $uri = str_replace("/||", "", $uri);
        $uri = str_replace("||", "", $uri);
        $uri = preg_replace('/(?:&|(\?))page=[^&]*(?(1)&|)?/i', "$1", $uri);
        if (strpos($uri, "search=1") !== false) $uri = "/fortune-teller?search=1";
        $seoMeta = Seo::where('uri', $uri)->first();

        if (isset($seoMeta) && $seoMeta->id != null) {
            $data['title'] = $seoMeta->title;
            $data['keywords'] = $seoMeta->keywords;
            $data['description'] = $seoMeta->description;
            $imgSeo = ($seoMeta->image != "" && $seoMeta->image != null) ? $seoMeta->image : 'no-image.png';
            $data['image'] = url("files/" . $imgSeo);
        } else {
            $data = ['title' => config('settings.site_name'), 'keywords' => '', 'description' => '', 'image' => url('favicon-16x16.png')];
        }

        return view('note', $data);
    }

    public function stripePaymentWebhook()
    {
        $stripe = new StripeClient('xxxx');
        $endpoint_secret = 'xxxx';
        $payload = @file_get_contents('php://input');
        $sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
        $event = null;

        try {
            $event = Webhook::constructEvent($payload, $sig_header, $endpoint_secret);
        } catch (\UnexpectedValueException $e) {
            // Invalid payload
            http_response_code(400);
            exit();

        } catch (SignatureVerificationException $e) {
            // Invalid signature
            http_response_code(400);
            exit();
        }

        http_response_code(200);
    }

    public function productExportPdf(Request $request, $purchase, $code)
    {
        if (empty($purchase) || ($purchase != 1 && $purchase != 2 && $purchase != '1' && $purchase != '2')) {
            return abort(404);
        }

        $productDetail = ProductDetail::with('option_detail', 'product')->where('code', $code)->first();
        if (!$productDetail) return abort(404);

        $weekMap = ['日', '月', '火', '水', '木', '金', '土'];
        $possibleDeliveryTime = $productDetail->product->possible_delivery_time;
        $date = Carbon::now();
        $date->addDays($possibleDeliveryTime);
        $dayOfTheWeek = $date->dayOfWeek;
        $weekday = $weekMap[$dayOfTheWeek];
        $possibleDeliveryTime = $date->format("Y年m月d日") . "(" . $weekday . ")";
        $currentDate = Carbon::now()->format('Y.m.d');

        $originalPrice = $productDetail->price;
        $discountPercent = ($productDetail->product->discount != "" && $productDetail->product->discount != null) ? $productDetail->product->discount : 0;
        $discountAmount = 0;
        if($discountPercent > 0) $discountAmount = ceil($originalPrice * ($discountPercent / 100));
        $price = ceil($originalPrice - $discountAmount);
        $constructionFee = ($purchase == '2' || $purchase == 2) ? $productDetail->product->standard_construction_fee : 0;
        $totalWithTax = $price + $constructionFee;
        $discount = ceil($originalPrice - $price);
        $tax = ceil($totalWithTax * (10 / 100));
        $shippingCost = $productDetail->product->shipping_cost;
        $totalAmount = ceil($price + $constructionFee + $tax + $shippingCost);

        $data = [
            'imgPath'              => url('/uploads/files'),
            'currentDate'          => $currentDate,
            'purchase'             => $purchase,
            'product'              => $productDetail->product,
            'options'              => $productDetail->option_detail,
            'possibleDeliveryTime' => $possibleDeliveryTime,
            'originalPrice'        => number_format($originalPrice),
            'discount'             => number_format($discount),
            'price'                => number_format($price),
            'constructionFee'      => number_format($constructionFee),
            'tax'                  => number_format($tax),
            'shippingCost'         => number_format($shippingCost),
            'totalAmount'          => number_format($totalAmount),
        ];

        if (config('app.url') == 'http://127.0.0.1:8000') $data['imgPath'] = base_path() . "/public/uploads/files";

        return Pdf::loadView('pdf.invoice', $data)
            ->setPaper('A4', 'portrait')
            ->stream('invoice.pdf'); //->save(storage_path('app/public/invoice2.pdf'))
    }

    public function imageToBase64($imgPath)
    {
        $type = pathinfo($imgPath, PATHINFO_EXTENSION);
        $data = \File::get($imgPath);

        return ($type == "svg") ? "data:image/svg+xml;base64," . base64_encode($data) : "data:image/" . $type . ";base64," . base64_encode($data);
    }
}
