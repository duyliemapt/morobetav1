<?php
/**
 * File UserController.php
 *
 * @author Tuan Duong <bacduong@gmail.com>
 * @package Laravue
 * @version 1.0
 */

namespace App\Http\Controllers;

use App\Http\Resources\PermissionResource;
use App\Http\Resources\UserResource;
use App\Laravue\JsonResponse;
use App\Laravue\Models\Permission;
use App\Laravue\Models\Role;
use App\Laravue\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;

/**
 * Class UserController
 *
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    const ITEM_PER_PAGE = 20;
    private $_limitedItems;

    public function __construct()
    {
        $user = auth('api')->user();
        $this->_limitedItems = ['hasLimited' => false, 'data' => []];
        if ($user != null && !$user->hasRole('admin') && !$user->hasRole('manager') && !$user->hasPermissionTo('manage-user')) {
            $this->_limitedItems['hasLimited'] = true;
            $this->_limitedItems['data'] = User::select('id')->where('created_by', $user->id)->pluck('id')->toArray();
            if ($this->_limitedItems['data'] == null || count($this->_limitedItems['data']) <= 0) {
                $this->_limitedItems['data'][] = 0;
            }
        }
    }

    /**
     * Display a listing of the user resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response|ResourceCollection
     */
    public function index(Request $request)
    {
        $searchParams = $request->all();
        $userQuery = User::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $role = Arr::get($searchParams, 'role', '');
        $keyword = Arr::get($searchParams, 'keyword', '');

        if ($role != "" && $role != null) {
            $userQuery->whereHas('roles', function ($q) use ($role) {
                $q->where('name', $role);
            });
        }

        if ($keyword != "" && $keyword != null) {
            $userQuery->where(function ($query) use ($keyword) {
                $query->where('name', 'LIKE', '%' . $keyword . '%')
                    ->orWhere('email', 'LIKE', '%' . $keyword . '%');
            });
        }

        if ($this->_limitedItems['hasLimited']) {
            $userQuery->whereIn('id', $this->_limitedItems['data']);
        }

        return UserResource::collection($userQuery->paginate($limit));
    }

    public function assigned(Request $request)
    {
        $searchParams = $request->all();
        $userQuery = User::query();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);

        if ($this->_limitedItems['hasLimited']) {
            $userQuery->whereIn('id', $this->_limitedItems['data']);
        }

        return UserResource::collection($userQuery->paginate($limit));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
                [
                    'password'        => ['required', 'min:6'],
                    'confirmPassword' => 'same:password',
                ]
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            $params = $request->all();
            $user = User::create([
                'name'       => $params['name'],
                'point_rate' => 0,
                'email'      => $params['email'],
                'password'   => Hash::make($params['password']),
                'login_url'  => $params['login_url'],
                'created_by' => auth('api')->user()->id,
            ]);
            $role = Role::findByName($params['role']);
            $user->syncRoles($role);

            return new UserResource($user);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return UserResource|\Illuminate\Http\JsonResponse
     */
    public function show(User $user)
    {
        if ($this->_limitedItems['hasLimited']) {
            $userCheck = User::select('id')->where('id', $user->id)->whereIn('id', $this->_limitedItems['data'])->first();
            if ($userCheck === null || !isset($userCheck->id)) {
                return response()->json(['error' => 'User not found'], 404);
            }
        }

        return new UserResource($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param User $user
     * @return UserResource|\Illuminate\Http\JsonResponse
     */
    public function update(Request $request, User $user)
    {
        if ($this->_limitedItems['hasLimited']) {
            $userCheck = User::select('id')->where('id', $user->id)->whereIn('id', $this->_limitedItems['data'])->first();
            if ($userCheck === null || !isset($userCheck->id)) {
                return response()->json(['error' => 'User not found'], 404);
            }
        }

        if ($user === null) {
            return response()->json(['error' => 'User not found'], 404);
        }

        if ($user->isAdmin()) {
            return response()->json(['error' => 'Admin can not be modified'], 403);
        }

        /*$currentUser = Auth::user();
        if (
            !$currentUser->isAdmin()
            && $currentUser->id !== $user->id
            && !$currentUser->hasPermission(\App\Laravue\Acl::PERMISSION_USER_MANAGE)
            && !$currentUser->hasPermission(\App\Laravue\Acl::PERMISSION_USER_EDIT_MANAGE)
        ) {
            echo "<pre>";print_r('okoko');echo "</pre>";die();
            return response()->json(['error' => 'Permission denied'], 403);
        }*/

        $validator = Validator::make($request->all(), $this->getValidationRules(false));
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {
            $email = $request->get('email');
            $found = User::where('email', $email)->first();
            if ($found && $found->id !== $user->id) {
                return response()->json(['error' => 'Email has been taken'], 403);
            }

            if ($request->has('password') && trim($request->get('password')) != "" && trim($request->get('password')) != null) {
                $user->password = Hash::make($request->get('password'));
            }

            $user->name = $request->get('name');
            $user->point_rate = 0;
            $user->email = $email;
            $user->login_url = '/';
            $user->save();

            $role = Role::findByName($request->get('role'));
            if (isset($role)) {
                $user->syncRoles($role);
                $user->save();
            }

            return new UserResource($user);
        }
    }

    public function updateProfile(Request $request, User $user)
    {
        if ($user === null) {
            return response()->json(['error' => 'User not found'], 404);
        }

        $rules = [
            'first_name'          => [ 'required', 'string', 'max:255' ],
            'last_name'           => [ 'required', 'string', 'max:255' ],
            'furigana_first_name' => [ 'required', 'string', 'max:255' ],
            'furigana_last_name'  => [ 'required', 'string', 'max:255' ],
            'post_code'           => [ 'required', 'min:1', 'max:10' ],
            'address'             => [ 'required', 'string', 'max:255' ],
            'address2'            => [ 'required', 'string', 'max:255' ],
            'phone_number'        => [ 'required', 'min:8', 'max:15' ],
            'email'               => ['required', 'email'],
            'password'            => ['required', 'string', 'min:6', 'max:30'],
            'confirm_password'    => ['same:password'],
            'gender'              => [ 'required'],
            'dob_year'            => [ 'required'],
            'dob_month'           => [ 'required'],
            'dob_day'             => [ 'required'],
            'reminder'            => [ 'required', 'string', 'max:2' ],
            'reminder_answer'     => [ 'required', 'string', 'max:255' ],
            'mail_subscribe'      => [ 'required'],
            'mail_format'         => [ 'required'],
        ];

        /*if ($request->get('password') != null && $request->get('password') != '') {
            $rules['password'] = [ 'required', 'string', 'min:6', 'max:30' ];
            $rules['confirm_password'] = ['same:password'];
        }*/

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) return response()->json(['errors' => $validator->errors()], 403);
        $params = $request->all();

        $checkEmail = User::where('email', $params['email'])->where('id', '!=', $user->id)->count();
        if ($checkEmail > 0) return response()->json(['data' => ['status' => 'error', 'name' => 'email_exist', 'message' => 'メールは既に存在します。']], 200);

        $updateData = [
            'password'            => Hash::make($params['password']),
            'first_name'          => $params['first_name'],
            'last_name'           => $params['last_name'],
            'full_name'           => $params['first_name'] . ' ' . $params['last_name'],
            'furigana_first_name' => $params['furigana_first_name'],
            'furigana_last_name'  => $params['furigana_last_name'],
            'furigana_name'       => $params['furigana_first_name'] . ' ' . $params['furigana_last_name'],
            'post_code'           => $params['post_code'],
            'city'                => $params['address'],
            'address'             => $params['address2'],
            'address2'            => isset($params['address3']) ? $params['address3'] : '',
            'phone_number'        => $params['phone_number'],
            'fax_number'          => $params['fax_number'],
            'career'              => $params['career'],
            'mobile_tel'          => (isset($params['mobile_tel']) && !empty($params['mobile_tel'])) ? $params['mobile_tel'] : null,
            'emg_tel'             => (isset($params['emg_tel']) && !empty($params['emg_tel'])) ? $params['emg_tel'] : null,
            'gender'              => (isset($params['gender']) && $params['gender'] == 0) ? 0 : 1,
            'date_of_birth'       => $params['dob_year'] . '-' . $params['dob_month'] . '-' . $params['dob_day'],
            'reminder_question'   => $params['reminder'],
            'reminder_answer'     => $params['reminder_answer'],
            'email2'              => $params['email2'],
            'email_subscribe'     => $params['mail_subscribe'],
            'email_format'        => $params['mail_format'],
            'updated_at'          => date('Y-m-d H:i:s')
        ];

        //if ($params['password'] != "") $user->password = Hash::make($params['password']);

        if (!$user->isAdmin()) {
            //return response()->json(['error' => 'Admin can not be modified'], 403);
            $updateData['email'] = $params['email'];
        }
        $user->update($updateData);

        return response()->json(['data' => ['status' => 'success', 'message' => 'Updated successfully.']], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param User $user
     * @return UserResource|\Illuminate\Http\JsonResponse
     */
    public function updatePermissions(Request $request, User $user)
    {
        if ($user === null) {
            return response()->json(['error' => 'User not found'], 404);
        }

        if ($user->isAdmin()) {
            return response()->json(['error' => 'Admin can not be modified'], 403);
        }

        $permissionIds = $request->get('permissions', []);
        $rolePermissionIds = array_map(
            function ($permission) {
                return $permission['id'];
            },

            $user->getPermissionsViaRoles()->toArray()
        );

        $newPermissionIds = array_diff($permissionIds, $rolePermissionIds);
        $permissions = Permission::allowed()->whereIn('id', $newPermissionIds)->get();
        $user->syncPermissions($permissions);
        return new UserResource($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if ($user->isAdmin()) {
            response()->json(['error' => 'Ehhh! Can not delete admin user'], 403);
        }

        try {
            $user->delete();
        } catch (\Exception $ex) {
            response()->json(['error' => $ex->getMessage()], 403);
        }

        return response()->json(null, 204);
    }

    /**
     * Get permissions from role
     *
     * @param User $user
     * @return array|\Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function permissions(User $user)
    {
        try {
            return new JsonResponse([
                'user' => PermissionResource::collection($user->getDirectPermissions()),
                'role' => PermissionResource::collection($user->getPermissionsViaRoles()),
            ]);
        } catch (\Exception $ex) {
            response()->json(['error' => $ex->getMessage()], 403);
        }
    }

    /**
     * @param bool $isNew
     * @return array
     */
    private function getValidationRules($isNew = true)
    {
        return [
            'name'  => 'required',
            'email' => $isNew ? 'required|email|unique:users' : 'required|email',
            'roles' => [
                'required',
                'array'
            ],
        ];
    }
}
