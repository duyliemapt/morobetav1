<?php

namespace App\Http\Controllers\Admin;

use App\Events\SaveDataEvent;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use App\Http\Resources\Admin\SeoResource;
use Illuminate\Support\Facades\Artisan;
use Validator;
use App\Seo;
use App\SeoFormat;
use App\Article;

/**
 * Class SeoController
 *
 * @package App\Http\Controllers
 */
class SeoController extends Controller
{
    const ITEM_PER_PAGE = 20;

    public function index(Request $request)
    {
        $searchParams = $request->all();
        $list = Seo::select('*');
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        $status = Arr::get($searchParams, 'status', '');

        if (!empty($keyword)) {
            $list->where('uri', 'LIKE', '%' . $keyword . '%');
        }
        if ($status != '') {
            $list->where('is_activated', $status);
        }
        $list->orderBy('uri');

        return SeoResource::collection($list->paginate($limit));
    }

    public function all()
    {
        $list = Seo::select('id', 'uri', 'title', 'keywords', 'description', 'image', 'is_amp')->orderBy('id');

        return SeoResource::collection($list->get());
    }

    public function show($id = 0)
    {
        $seo = Seo::where('id', $id)->first();
        if (!isset($seo)) return response()->json(['errors' => 'Seo is not valid'], 403);

        return new SeoResource($seo);
    }

    public function seoFormat()
    {
        $formats = SeoFormat::all();
        return response()->json(['data' => $formats], 200);
    }

    public function seoCheck(Request $request)
    {
        $uri = $request->get('uri');
        $seo = Seo::where('uri', $uri)->first();
        $data = ['status' => 'error', 'id' => null];
        if (isset($seo) && $seo->id != null) {
            $data = ['status' => 'success', 'id' => $seo->id];
        }

        return response()->json($data, 200);
    }

    public function noteArticleSeo(Request $request)
    {
        $validator = Validator::make($request->all(), ['title' => ['required'], 'description' => ['required']]);
        if ($validator->fails()) return response()->json(['errors' => $validator->errors()], 403);
        $params = $request->all();

        //%content|160%
        $params['description'] = trim($params['description']);
        $descFormat = trim($params['description']);
        $descFormat = str_replace("% content |", "%content|", $descFormat);
        $descFormat = str_replace("%content |", "%content|", $descFormat);
        $descFormat = str_replace("% content|", "%content|", $descFormat);
        $lengthContent = "";
        $key = "%content|";
        if ($descFormat != "" && strpos($descFormat, $key) !== false) {
            for ($i = 1; $i <= 1000; $i++) {
                if (strpos($descFormat, $key . $i . "%") !== false || strpos($descFormat, $key . " " . $i . " %") !== false || strpos($descFormat, $key . " " . $i . "%") !== false || strpos($descFormat, $key . $i . " %") !== false) {
                    $lengthContent = $i;
                }
            }
        }

        $baseUri = '/note/article/';
        if ($params['override'] == true || $params['override'] == "true" || $params['override'] == 1 && $params['override'] == "1") {
            Seo::where('uri', 'like', $baseUri . "%")->delete();
        }

        $countUpdate = 0;
        $countCreate = 0;
        $list = Article::where('is_deleted', false)->get();
        foreach ($list as $item) {
            $uri = $baseUri . $item->slug;
            $title = str_replace("%title%", $item->title, $params['title']);
            $title = str_replace("%title %", $item->title, $title);
            $title = str_replace("% title%", $item->title, $title);
            $title = str_replace("% title %", $item->title, $title);

            //Description
            $description = str_replace("%title%", $item->title, $params['description']);
            $description = str_replace("% title%", $item->title, $description);
            $description = str_replace("%title %", $item->title, $description);
            $description = str_replace("% title %", $item->title, $description);

            if ($lengthContent != "" && is_numeric($lengthContent)) {
                $description = str_replace("% content |", "%content|", $description);
                $description = str_replace("%content |", "%content|", $description);
                $description = str_replace("% content |", "%content|", $description);
                $description = str_replace("%content| " . $lengthContent . " %", "%content|" . $lengthContent . "%", $description);
                $description = str_replace("%content|" . $lengthContent . " %", "%content|" . $lengthContent . "%", $description);
                $description = str_replace("%content| " . $lengthContent . "%", "%content|" . $lengthContent . "%", $description);

                if ($item->content != null && $item->content != "") {
                    $fullContent = strip_tags($item->content);
                    $fullContent = str_replace("&nbsp;", " ", $fullContent);
                    $shortContent = mb_substr(trim($fullContent), 0, $lengthContent);
                    $description = str_replace("%content|" . $lengthContent . "%", $shortContent, $description);
                }
            }

            $description = str_replace("\r\n", "", $description);
            $description = str_replace("\r", "", $description);
            $description = str_replace("\n", "", $description);
            $description = str_replace("&nbsp;", " ", $description);
            $description = trim($description);
            if (trim($description) == ("%content|" . $lengthContent . "%")) $description = "";

            //Keyword
            $keywords = str_replace("%title%", $item->title, $params['keywords']);
            $keywords = str_replace("% title%", $item->title, $keywords);
            $keywords = str_replace("%title %", $item->title, $keywords);
            $keywords = str_replace("% title %", $item->title, $keywords);

            $data = [
                'uri'         => $uri,
                'title'       => $title,
                'keywords'    => $keywords,
                'description' => $description,
                'type'        => 'note_article',
                'post_id'     => $item->id,
            ];

            $checkSeo = Seo::where('uri', $uri)->first();
            if (isset($checkSeo) && $checkSeo->id != null) {
                $appendData = [];
                if ($params['override'] == true || $params['override'] == "true" || $params['override'] == 1 && $params['override'] == "1") {
                    $appendData = $data;
                } else {
                    if ($checkSeo->title == null && $checkSeo->title == "") $appendData['title'] = $data['title'];
                    if ($checkSeo->keywords == null && $checkSeo->keywords == "") $appendData['keywords'] = $data['keywords'];
                    if ($checkSeo->description == null && $checkSeo->description == "") $appendData['description'] = $data['description'];
                }
                if (count($appendData) > 0) {
                    $appendData['updated_at'] = date('Y-m-d H:i:s');
                    Seo::where('uri', $uri)->update($appendData);
                    $countUpdate++;
                }
            } else {
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['updated_at'] = date('Y-m-d H:i:s');
                Seo::create($data);
                $countCreate++;
            }
        }

        SeoFormat::where('type', 'note_article')->update([
            'title'       => $params['title'],
            'keywords'    => $params['keywords'],
            'description' => $params['description'],
            'updated_at'  => date('Y-m-d H:i:s')
        ]);
        //event(new SaveDataEvent());
        Artisan::call('cache:clear');

        return response()->json(['data' => ['status' => 'success', 'created' => $countCreate, 'updated' => $countUpdate]], 200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), ['uri' => ['required'], 'title' => ['required']]);
        if ($validator->fails()) return response()->json(['errors' => $validator->errors()], 403);
        $params = $request->all();
        $uri = trim("/" . $params['uri']);
        $uri = str_replace("/ ", "/", $uri);
        $uri = str_replace(" / ", "/", $uri);
        $uri = str_replace(" /", "/", $uri);
        $uri = str_replace(" ", "-", $uri);
        $uri = str_replace("//", "/", $uri);
        $uri = str_replace("&&", "&", $uri);
        $uri = str_replace("/?", "?", $uri);
        $uri = str_replace("?/", "?", $uri);
        $uri = str_replace("??", "?", $uri);
        $uri = str_replace("?&", "?", $uri);
        if (strlen($uri) > 1 && $uri[strlen($uri) - 1] == "/") $uri = substr($uri, 0, -1);
        $seoExist = Seo::where('uri', $uri)->first();
        if (isset($seoExist)) return response()->json(['errors' => 'Uri is exist'], 403);
        $typeArr = $this->detectSeoType($uri);

        $seo = Seo::create([
            'uri'         => $uri,
            'title'       => $params['title'],
            'keywords'    => $params['keywords'],
            'description' => $params['description'],
            'note'        => $params['note'],
            'image'       => $params['image'],
            'type'        => $typeArr['type'],
            'post_id'     => $typeArr['post_id'],
            'is_amp'      => ($params['is_amp'] == true || $params['is_amp'] == 'true') ? 1 : 0,
            'created_at'  => date('Y-m-d H:i:s'),
            'updated_at'  => date('Y-m-d H:i:s')
        ]);
        //event(new SaveDataEvent());
        Artisan::call('cache:clear');

        return new SeoResource($seo);
    }

    public function update($id = 0, Request $request)
    {
        $validator = Validator::make($request->all(), ['uri' => ['required'], 'title' => ['required']]);
        if ($validator->fails()) return response()->json(['errors' => $validator->errors()], 403);
        $seo = Seo::where('id', $id)->first();
        if (!isset($seo)) return response()->json(['errors' => 'Seo is not valid'], 403);

        $params = $request->all();
        $seo->update([
            'title'       => $params['title'],
            'keywords'    => $params['keywords'],
            'description' => $params['description'],
            'note'        => $params['note'],
            'image'       => $params['image'],
            'is_amp'      => ($params['is_amp'] == true || $params['is_amp'] == 'true') ? 1 : 0,
            'updated_at'  => date('Y-m-d H:i:s')
        ]);
        /*event(new SaveDataEvent());*/
        Artisan::call('cache:clear');

        return response()->json(null, 204);
    }

    public function destroy($id = 0)
    {
        $seo = Seo::where('id', $id)->first();
        if (!isset($seo)) response()->json(['error' => 'Ehhh! Can not delete this seo'], 403);
        try {
            $seo->delete();
        } catch (\Exception $ex) {
            response()->json(['error' => $ex->getMessage()], 403);
        }
        //event(new SaveDataEvent());
        Artisan::call('cache:clear');

        return response()->json(null, 204);
    }

    public function destroyMultiple(Request $request)
    {
        $validator = Validator::make($request->all(), ['ids' => 'required']);
        if ($validator->fails()) return response()->json(['errors' => $validator->errors()], 403);
        $listIds = $request->get('ids', []);
        $seoList = Seo::whereIn('id', $listIds)->get();
        if ($seoList->count() <= 0) response()->json(['error' => 'Seo is not valid.'], 403);
        try {
            Seo::whereIn('id', $listIds)->delete();
        } catch (\Exception $ex) {
            response()->json(['error' => $ex->getMessage()], 403);
        }
        //event(new SaveDataEvent());
        Artisan::call('cache:clear');

        return response()->json(null, 204);
    }

    private function detectSeoType($uri = "") {
        $rs = ['type' => 'custom', 'post_id' => null];
        if (strpos($uri, "/bbs/") !== false) {
            $key = str_replace("/bbs/detail/", "", $uri);
            $uriArr = explode("/", $key);
            $article = Article::select('id')->where('is_deleted', false)->where('id', $uriArr[0])->first();
            $rs['post_id'] = isset($article) ? $article->id : null;
            $rs['type'] = "question";
        } else if (strpos($uri, "/column/") !== false) {
            $key = str_replace("/column/detail/", "", $uri);
            $uriArr = explode("/", $key);
            $article = Article::select('id')->where('is_deleted', false)->where('id', $uriArr[0])->first();
            $rs['post_id'] = isset($article) ? $article->id : null;
            $rs['type'] = "column";
        }

        return $rs;
    }
}
