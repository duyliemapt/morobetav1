<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use App\BlockIP;
use App\Http\Resources\Admin\BlockIpResource;
use Illuminate\Support\Facades\Cache;
use Validator;

/**
 * Class BlockIpController
 *
 * @package App\Http\Controllers
 */
class BlockIpController extends Controller
{
    const ITEM_PER_PAGE = 50;

    public function index(Request $request)
    {
        $searchParams = $request->all();
        $list = BlockIP::notDeleted();
        $limit = Arr::get($searchParams, 'limit', static::ITEM_PER_PAGE);
        $keyword = Arr::get($searchParams, 'keyword', '');
        $status = Arr::get($searchParams, 'status', '');

        if (!empty($keyword)) {
            $list->where('ip', 'LIKE', '%' . $keyword . '%');
        }
        if ($status != '') {
            $list->where('is_activated', $status);
        }
        $list->orderBy('id');

        return BlockIpResource::collection($list->paginate($limit));
    }

    public function available(Request $request)
    {

        $ignoreId = Arr::get($request->all(), 'ignoreId', '');
        $list = BlockIP::select('*')->isPublished();

        if ($ignoreId != null && $ignoreId != "") {
            $list->where('id', '!=', $ignoreId);
        }

        $list->orderBy('id');

        return BlockIpResource::collection($list->get());
    }

    public function show($id = 0)
    {
        $ip = BlockIP::notDeleted()->where('id', $id)->first();
        if (!isset($ip)) return response()->json([ 'errors' => 'IP address is not valid' ], 403);

        return new BlockIpResource($ip);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [ 'ip' => [ 'required' ], 'is_activated' => [ 'required' ] ]);
        if ($validator->fails()) return response()->json([ 'errors' => $validator->errors() ], 403);
        $params = $request->all();

        $ip = BlockIP::create([
            'ip'           => $params['ip'],
            'description'  => $params['description'],
            'is_activated' => ($params['is_activated'] === true) ? 1 : 0,
            'created_at'   => date('Y-m-d H:i:s'),
            'updated_at'   => date('Y-m-d H:i:s')
        ]);

        Cache::forget('white-ips');

        return new BlockIpResource($ip);
    }

    public function update(Request $request, $id = 0)
    {
        $validator = Validator::make($request->all(), [ 'ip' => [ 'required' ], 'is_activated' => [ 'required' ] ]);
        if ($validator->fails()) return response()->json([ 'errors' => $validator->errors() ], 403);
        $ip = BlockIP::notDeleted()->where('id', $id)->first();
        if (!isset($ip)) return response()->json([ 'errors' => 'IP address is not valid' ], 403);
        $params = $request->all();

        $ip->update([
            'ip'           => $params['ip'],
            'description'  => $params['description'],
            'incorrect'    => 0,
            'is_activated' => ($params['is_activated'] === true) ? 1 : 0,
            'updated_at'   => date('Y-m-d H:i:s')
        ]);

        Cache::forget('white-ips');

        return response()->json(null, 204);
    }

    public function destroy($id = 0)
    {
        $ip = BlockIP::notDeleted()->where('id', $id)->first();
        if (!isset($ip)) response()->json([ 'error' => 'Ehhh! Can not delete this IP address' ], 403);

        try {
            $ip->update([ 'is_deleted' => true ]);
            Cache::forget('white-ips');
        } catch (\Exception $ex) {
            response()->json([ 'error' => $ex->getMessage() ], 403);
        }

        return response()->json(null, 204);
    }

    public function destroyMultiple(Request $request)
    {
        $validator = Validator::make($request->all(), [ 'ids' => 'required' ]);
        if ($validator->fails()) return response()->json([ 'errors' => $validator->errors() ], 403);
        $listIds = $request->get('ids', []);
        $ips = BlockIP::notDeleted()->whereIn('id', $listIds)->get();
        if ($ips->count() <= 0) response()->json([ 'error' => 'IP address is not valid.' ], 403);
        try {
            BlockIP::notDeleted()->whereIn('id', $listIds)->update([ 'is_deleted' => true ]);
            Cache::forget('white-ips');
        } catch (\Exception $ex) {
            response()->json([ 'error' => $ex->getMessage() ], 403);
        }

        return response()->json(null, 204);
    }

    public function activateMultiple(Request $request)
    {
        $validator = Validator::make($request->all(), [ 'ids' => 'required' ]);
        if ($validator->fails()) return response()->json([ 'errors' => $validator->errors() ], 403);
        $listIds = $request->get('ids', []);
        $ips = BlockIP::notDeleted()->whereIn('id', $listIds)->get();
        if ($ips->count() <= 0) response()->json([ 'error' => 'IP address is not valid.' ], 403);
        try {
            BlockIP::notDeleted()->whereIn('id', $listIds)->update([ 'is_activated' => true ]);
            Cache::forget('white-ips');
        } catch (\Exception $ex) {
            response()->json([ 'error' => $ex->getMessage() ], 403);
        }

        return response()->json(null, 204);
    }

    public function deactivateMultiple(Request $request)
    {
        $validator = Validator::make($request->all(), [ 'ids' => 'required' ]);
        if ($validator->fails()) return response()->json([ 'errors' => $validator->errors() ], 403);
        $listIds = $request->get('ids', []);
        $ips = BlockIP::notDeleted()->whereIn('id', $listIds)->get();
        if ($ips->count() <= 0) response()->json([ 'error' => 'IP address is not valid.' ], 403);
        try {
            BlockIP::notDeleted()->whereIn('id', $listIds)->update([ 'is_activated' => false ]);
            Cache::forget('white-ips');
        } catch (\Exception $ex) {
            response()->json([ 'error' => $ex->getMessage() ], 403);
        }

        return response()->json(null, 204);
    }
}
