<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use App\Setting;
use App\Http\Resources\Admin\SettingResource;
use Illuminate\Support\Facades\Cache;
use Validator;

/**
 * Class SettingController
 *
 * @package App\Http\Controllers
 */
class SettingController extends Controller
{
    const ITEM_PER_PAGE = 100;

    public function index(Request $request)
    {
        $searchParams = $request->all();
        $group = Arr::get($searchParams, 'group', 'general');
        $list = Setting::notDeleted()->where('group', $group)->orderBy('group')->orderBy('id');

        return SettingResource::collection($list->get());
    }

    public function all()
    {
        $allSettings = Cache::remember('all_settings', 22*60, function() {
            return Setting::select('slug', 'content')->notDeleted()->isActivated()->isPublish()->pluck('content', 'slug');
        });

        return response()->json([ 'data' => $allSettings ], 200);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), ['settings' => ['required']]);
        if ($validator->fails()) return response()->json(['errors' => $validator->errors()], 403);
        $params = $request->all();

        if (!is_array($params['settings']) || count($params['settings']) <= 0) return response()->json(['errors' => 'Setting is not valid'], 403);
        foreach ($params['settings'] as $setting) {
            Setting::where('id', $setting['id'])->update(['content' => $setting['content']]);
        }
        Cache::forget('settings');
        Cache::forget('all_settings');
        Cache::forget('all_publish_settings');

        return response()->json(null, 204);
    }

    public function uploadImage(Request $request) {

    }
}
