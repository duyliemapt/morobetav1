<?php

namespace App\Http\Controllers;

use App\Events\NotifyEvent;
use App\Notify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Laravue\Models\Role;
use App\Laravue\Models\User;
use Carbon\Carbon;
use Validator;
use Mail;

class RegisterController extends Controller
{
    public function storeEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'         => [ 'required', 'string', 'email', 'min:6', 'max:30' ],
            'confirm_email' => [ 'same:email' ],
        ]);

        if ($validator->fails()) return response()->json([ 'errors' => $validator->errors() ], 403);
        $params = $request->all();
        $userEmail = $params['email'];

        $checkUser = User::where('is_deleted', 0)->where('is_temp', 0)->where('email', $userEmail)->count();
        if ($checkUser > 0) return response()->json([ 'data' => [ 'status' => 'error', 'message' => 'The email has already been taken.' ] ], 200);

        $code = hash('sha256', date('Ymd') . rand(1, 9999999999));
        $nameArr = explode("@", $userEmail);
        $name = $nameArr[0];
        $tel = $params['tel'];

        User::create([
            'name'                => $name,
            'username'            => $name,
            'slug'                => $name,
            'password'            => Hash::make($name),
            'email'               => $params['email'],
            'first_name'          => $name,
            'last_name'           => $name,
            'full_name'           => $name,
            'furigana_first_name' => $name,
            'furigana_last_name'  => $name,
            'furigana_name'       => $name,
            'phone_number'        => (!empty($tel)) ? $tel : null,
            'avatar'              => 'avatar.png',
            'reset_password_code' => $code,
            'reset_password_time' => date('Y-m-d H:i:s'),
            'is_temp'             => 1,
            'is_activated'        => 0,
            'is_deleted'          => 1,
        ]);

        $data = $params;
        $data['site'] = url("/");
        $data['code'] = $code;

        try {
            Mail::send('emails.register_email', $data, function ($message) use ($userEmail) {
                $message->from(config('mail.from.address'), config('mail.from.name'));
                $message->to($userEmail);
                $message->subject("【KIMONO】ご利用登録URLのご案内");
            });

            return response()->json([ 'data' => [ 'status' => 'success', 'message' => 'Send email successfully.' ] ], 200);
        } catch (\Exception $exception) {
            logger($exception->getMessage());
            return response()->json([ 'data' => [ 'status' => 'error', 'message' => 'Send email fail! Please contact supporter.' ] ], 200);
        }
    }

    public function checkCode($code)
    {
        $user = User::where('is_deleted', 1)
            ->where('is_activated', 0)
            ->where('is_temp', 1)
            ->where('reset_password_code', $code)
            ->orderBy('id', 'ASC')
            ->first();

        if (!$user) {
            return response()->json([ 'data' => [ 'status' => 'error', 'email' => '', 'message' => 'Invalid registration code, please try again.' ] ], 200);
        }

        $user->update([ 'email_verified_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s') ]);

        return response()->json([ 'data' => [ 'status' => 'success', 'email' => $user->email, 'message' => 'Code valid.' ] ], 200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code'             => [ 'required', 'string', 'max:255' ],
            'full_name'        => [ 'required', 'string', 'max:255' ],
            'furigana_name'    => [ 'required', 'string', 'max:255' ],
            'address'          => [ 'required', 'string', 'max:255' ],
            'phone_number'     => [ 'required', 'min:8', 'max:15' ],
            'mobile_tel'       => [ 'required', 'min:8', 'max:15' ],
            'emg_tel'          => [ 'required', 'min:8', 'max:15' ],
            'gender'           => [ 'required' ],
            'date_of_birth'    => [ 'required' ],
            'password'         => [ 'required', 'string', 'min:6', 'max:30' ],
            'confirm_password' => [ 'same:password' ],
            'email'            => [ 'required', 'string', 'email', 'min:6', 'max:30' ],
        ]);

        if ($validator->fails()) return response()->json([ 'errors' => $validator->errors() ], 403);
        $params = $request->all();

        $user = User::where('is_deleted', 1)
            ->where('is_activated', 0)
            ->where('is_temp', 1)
            ->where('reset_password_code', $params['code'])
            ->where('email', $params['email'])
            ->orderBy('id', 'ASC')
            ->first();

        if (!$user) {
            return response()->json([ 'data' => [ 'status' => 'error', 'email' => '', 'message' => 'Invalid registration code, please try again.' ] ], 200);
        }

        User::where('id', '!=', $user->id)->where('email', $params['email'])->delete();

        $user->update([
            'password'            => Hash::make($params['password']),
            'first_name'          => $params['full_name'],
            'last_name'           => $params['full_name'],
            'full_name'           => $params['full_name'],
            'furigana_first_name' => $params['furigana_name'],
            'furigana_last_name'  => $params['furigana_name'],
            'furigana_name'       => $params['furigana_name'],
            'city'                => $params['address'],
            'address'             => $params['address'],
            'phone_number'        => $params['phone_number'],
            'mobile_tel'          => (isset($params['mobile_tel']) && !empty($params['mobile_tel'])) ? $params['mobile_tel'] : null,
            'emg_tel'             => (isset($params['emg_tel']) && !empty($params['emg_tel'])) ? $params['emg_tel'] : null,
            'gender'              => (isset($params['gender']) && $params['gender'] == 0) ? 0 : 1,
            'date_of_birth'       => $params['date_of_birth'],
            'reset_password_code' => null,
            'reset_password_time' => date('Y-m-d H:i:s'),
            'is_temp'             => 0,
            'is_activated'        => 1,
            'is_deleted'          => 0,
            'updated_at'          => date('Y-m-d H:i:s')
        ]);

        $role = Role::findByName('user');
        $user->syncRoles($role);

        return response()->json([ 'data' => [ 'status' => 'success', 'message' => '' ] ], 200);

        /*$data = $params;
        $data['site'] = url("/");
        $userEmail = $params['email'];*/

        /*try {
            Notify::create([
                'content'      => "[" . $params['full_name'] . "] just registered an account.",
                'module'       => 'register',
                'item_content' => json_encode($data),
                'created_at'   => date('Y-m-d H:i:s'),
                'updated_at'   => date('Y-m-d H:i:s'),
            ]);
            event(new NotifyEvent());
        } catch (\Exception $exception) {
            logger($exception->getMessage());
        } finally {
            return response()->json([ 'data' => [ 'status' => 'success' ] ], 200);
        }*/

        //Send Email
        //try {
        //hunters2fujiyoshi@gmail.com
        /*Notify::create([
            'content'      => "[" . $params['full_name'] . "] just registered an account.",
            'module'       => 'contact',
            'item_content' => json_encode($data),
            'created_at'   => date('Y-m-d H:i:s'),
            'updated_at'   => date('Y-m-d H:i:s'),
        ]);
        event(new NotifyEvent()); */
        //broadcast(new NotifyEvent($notify));

        //admin
        /*Mail::send('emails.register_admin', $data, function ($message) {
            $message->from(config('mail.from.address'), config('mail.from.name'));
            $message->to(config('settings.admin_email'));
            $message->subject("【介護なんでも相談室】お客様から新規会員登録がありました。");
        });*/

        //user
        /*if (config('settings.admin_email') != $userEmail) {
            Mail::send('emails.register_user', $data, function ($message) use ($userEmail) {
                $message->from(config('mail.from.address'), config('mail.from.name'));
                $message->to($userEmail);
                $message->subject("【介護なんでも相談室】新規会員登録ありがとうございました。");
            });
            logger("[Email Registered] Done: " . $userEmail);
        }*/
        //} catch (\Exception $exception) {
        //logger($exception->getMessage());
        //} finally {
        //logger("[Email] Sent register email to: " . config('settings.admin_email'));

        //return response()->json(['data' => ['status' => 'success']], 200);
        //}
    }

    public function forgotPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => [ 'required', 'string', 'email', 'max:255' ],
        ]);
        if ($validator->fails()) return response()->json([ 'data' => [ 'status' => 'error', 'message' => $validator->errors() ] ], 200);

        $checkEmail = User::where('email', $request->get('email'))->first();
        if (!$checkEmail) return response()->json([ 'data' => [ 'status' => 'error', 'message' => '電子メールが存在しません。' ] ], 200);

        $code = md5(md5(rand(1, 9999) . date('YmdHis')));
        $link = url('/forgot-password/' . $code);
        $email = $request->get('email');
        $fullName = $checkEmail->full_name;
        if ($fullName == "" || $fullName == null) $fullName = $checkEmail->name;
        $expTime = Carbon::now()->addMinutes(15);

        $checkEmail->update([
            'reset_password_code' => $code,
            'reset_password_time' => $expTime
        ]);

        $data = [
            'full_name' => $fullName,
            'link'      => $link,
        ];

        Mail::send('emails.forgot_password_user', $data, function ($message) use ($email) {
            $message->from(config('mail.from.address'), config('mail.from.name'));
            $message->to($email);
            $message->subject("【介護なんでも相談室】パスワード再設定メール");
        });

        logger("[Email Forgot Password] Done: " . $email);

        return response()->json([ 'data' => [ 'status' => 'success' ] ], 200);
    }

    public function resetPassword(Request $request, $code = '')
    {
        $validator = Validator::make($request->all(), [
            'email'            => [ 'required', 'string', 'email', 'max:255' ],
            'password'         => [ 'required', 'string', 'min:6' ],
            'confirm_password' => [ 'same:password' ],
        ]);
        if ($validator->fails()) return response()->json([ 'data' => [ 'status' => 'error', 'message' => $validator->errors() ] ], 200);

        $params = $request->all();
        $checkEmail = User::where('email', $params['email'])->first();
        if (!$checkEmail) return response()->json([ 'data' => [ 'status' => 'error', 'message' => '電子メールが存在しません。' ] ], 200);

        $firstDate = Carbon::now();
        $secondDate = Carbon::parse($checkEmail->reset_password_time);

        if ($checkEmail->reset_password_code == null ||
            $checkEmail->reset_password_code == "" ||
            $checkEmail->reset_password_time == null ||
            $checkEmail->reset_password_time == "" ||
            $checkEmail->reset_password_code != $code || $firstDate->gt($secondDate)) {
            return response()->json([ 'data' => [ 'status' => 'error_exp', 'message' => 'パスワード リセット コードの有効期限が切れています。ここをクリックして、もう一度お試しください。' ] ], 200);
        }

        $checkEmail->update([
            'password'            => Hash::make($params['password']),
            'reset_password_code' => null,
            'reset_password_time' => null,
            'updated_at'          => date('Y-m-d H:i:s')
        ]);

        return response()->json([ 'data' => [ 'status' => 'success' ] ], 200);
    }
}
