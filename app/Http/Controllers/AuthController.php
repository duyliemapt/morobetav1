<?php
/**
 * File AuthController.php
 *
 * @author Tuan Duong <bacduong@gmail.com>
 * @package Laravue
 * @version 1.0
 */

namespace App\Http\Controllers;

use App\BlockIP;
use App\Laravue\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Cache;

/**
 * Class AuthController
 *
 * @package App\Http\Controllers
 */
class AuthController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        $ip = BlockIP::where('is_deleted', 0)->where('is_activated', 1)->where('ip', $request->ip())->first();
        if (!$ip) {
            return response()->json(new JsonResponse([], 'block_this_ip'), Response::HTTP_UNAUTHORIZED);
        }

        if (!Auth::attempt($credentials)) {
            $ip->incorrect += 1;
            $ip->save();

            if ($ip->incorrect >= 10) {
                $ip->update(['is_activated' => 0, 'updated_at' => date('Y-m-d H:i:s')]);
                Cache::forget('white-ips');

                return response()->json(new JsonResponse([], 'block_this_ip'), Response::HTTP_UNAUTHORIZED);
            }

            return response()->json(new JsonResponse([], 'Account and password are incorrect.'), Response::HTTP_UNAUTHORIZED);
        }

        $ip->update(['incorrect' => 0, 'updated_at' => date('Y-m-d H:i:s')]);

        $user = $request->user();
        $token = $user->createToken('laravue');

        return response()->json(new UserResource($user), Response::HTTP_OK)->header('Authorization', $token->plainTextToken);
    }

    public function loginUser(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if (!Auth::attempt($credentials)) {
            return response()->json(new JsonResponse([], 'Account or password are incorrect.'), Response::HTTP_UNAUTHORIZED);
        }

        $user = $request->user();
        $token = $user->createToken('laravue');

        return response()->json(new UserResource($user), Response::HTTP_OK)->header('Authorization', $token->plainTextToken);
    }

    public function logout(Request $request)
    {
        $request->user()->tokens()->delete();
        return response()->json((new JsonResponse())->success([]), Response::HTTP_OK);
    }

    public function user()
    {
        return new UserResource(Auth::user());
    }

    public function getUser(Request $request)
    {
        return $request->user();
    }
}
