<?php

namespace App\Http\Controllers;

use App\Events\SaveDataEvent;
use App\Helpers\Helper;
use App\Http\Resources\Admin\MediaResource;
use App\Http\Resources\Admin\SettingResource;
use App\Media;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Intervention\Image\Exception\NotReadableException;
use ImageCross;

class SettingController extends Controller
{
    const ITEM_PER_PAGE = 100;
    private $_filePath;

    public function __construct()
    {
        $this->_filePath = public_path('uploads/files/');
    }

    public function all()
    {
        $allSettings = Cache::remember('all_publish_settings', (7*24*60*60), function() {
            $allow = ['system_name', 'site_name', 'phone_number', 'copy_right', 'company_time', 'company_name', 'company_hot_line', 'company_fax', 'company_address'];

            $list = Setting::select('slug', 'content')
                ->notDeleted()
                ->isActivated()
                ->isPublish()
                ->whereIn('slug', $allow)
                ->pluck('content', 'slug');

            return ['items' => $list, 'lastModify' => (gmdate('D, d M Y H:i:s') . ' GMT')];
        });

        return response()->json(['data' => $allSettings['items']], 200)->header('Last-Modified', $allSettings['lastModify']);
    }

    public function uploadImage(Request $request) {
        $all = $request->all();
        $keys = array_keys($all);
        foreach ($keys as $k) {
            if ($request->hasFile($k)) {
                $file = $request->file($k);
                $baseName = $file->getClientOriginalName();
                $fileName = md5($baseName . rand(1111, 9999) . date('YmdHis')) . '.' .$file->getClientOriginalExtension();
                $file->move(public_path('uploads/image/setting'), $fileName);

                return response()->json(['status' => 'success', 'message' => 'Upload Success.', 'name' => $k, 'file_name' => $fileName], 200);
            }
        }

        return response()->json(['status' => 'error', 'message' => 'Upload Fail.', 'name' => '', 'file_name' => ''], 403);
    }

    public function uploadMedia(Request $request)
    {
        if (!$request->hasFile('file')) return response()->json(['status' => 'error', 'message' => 'Upload Fail.', 'file_name' => null], 403);
        $file = $request->file('file');
        $parent = trim($request->get('parent'));
        if (($parent == null || $parent == "" || $parent == "null") && $request->has("folder") && $request->get("folder") != null && $request->get("folder") != "" && $request->get("folder") != "null") {
            $folder = Media::where('is_deleted', false)
                ->where('type', 0)
                ->where('name', trim($request->get("folder")))
                ->where('parent_id', 0)
                ->first();
            if (!isset($folder) || $folder == null) {
                $folder = Media::create([
                    'name'       => trim($request->get("folder")),
                    'slug'       => Helper::slug($request->get("folder")),
                    'file'       => md5(rand(1000, 100000) . $request->get("folder") . date("YmdHis")),
                    'type'       => 0,
                    'parent_id'  => 0,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
            }
            $parent = $folder->id;
        }
        $parentId = ($parent != null && $parent != '' && $parent != 'null' && !empty($parent)) ? $parent : 0;
        $baseName = $file->getClientOriginalName();
        $imageMimeType = ['image/jpeg', 'image/png', 'image/jpg', 'image/gif', 'image/webp'];
        $data = ['status' => 'error', 'message' => 'Upload Fail.', 'file_name' => null, 'baseName' => $baseName, 'item' => null];
        $statusCode = 403;

        try {
            $extension = $file->getClientOriginalExtension();
            $fileType = $file->getMimeType();
            $fileSize = $this->formatBytes($file->getSize());

            do {
                $fileName = md5($baseName . rand(1111, 9999) . date('YmdHis')) . '.' . $extension;
                $checkFileName = Media::where('is_deleted', 0)->where('file', $fileName)->count();
            } while ($checkFileName > 0);

            $file->move($this->_filePath, $fileName);

            $extKey = "223344";
            $countBaseName = 0;
            $checkName = Media::where('is_deleted', 0)->where('parent_id', $parentId)->where('name', $baseName)->count();
            $firstBaseName = str_replace("." . $extension . $extKey, "", $baseName . $extKey);
            while ($checkName > 0) {
                $countBaseName += 1;
                $baseName = $firstBaseName . " (" . $countBaseName . ")." . $extension;
                $checkName = Media::where('is_deleted', 0)->where('parent_id', $parentId)->where('name', $baseName)->count();
            }

            $slug = str_replace(")." . $extension . $extKey, "", $baseName . $extKey);
            $slug = str_replace("." . $extension . $extKey, "", $slug);
            $slug = str_replace(".", "", $slug);
            $slug = str_replace("(", "-", $slug);
            $slug = str_replace(")", "", $slug);
            $slug = Helper::slug($slug);
            $slug = str_replace("--", "-", $slug);
            $slug = str_replace("_", "-", $slug);

            $media = Media::create([
                'name'       => $baseName,
                'slug'       => $slug,
                'file'       => $fileName,
                'file_type'  => $fileType,
                'extension'  => "." . $extension,
                'size'       => $fileSize,
                'type'       => 1,
                'parent_id'  => $parentId,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            if (in_array($fileType, $imageMimeType)) {
                try {
                    $imgCross = ImageCross::make($this->_filePath . $fileName)->orientate();
                    $media->update(['width' => $imgCross->width(), 'height' => $imgCross->height()]);
                    Helper::convertJpgToWebp($fileName);
                } catch (NotReadableException $exception) {
                    logger("[Error] Image Cross: " . $exception->getMessage());
                } catch (\Exception $exception) {
                    logger("[Error] Image Cross: " . $exception->getMessage());
                }
            }

            $item = new MediaResource($media);
            $data = ['status' => 'success', 'message' => 'Upload Success.', 'file_name' => $fileName, 'baseName' => $baseName, 'item' => $item];
            $statusCode = 200;
        } catch (\Exception $exception) {
            logger("[Error] Upload file: " . $exception->getMessage());
        } finally {
            return response()->json($data, $statusCode);
        }
    }

    private function formatBytes($bytes, $precision = 2)
    {
        $units = array('B', 'Kb', 'MB', 'GB', 'TB');
        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);
        $bytes /= pow(1024, $pow);
        //$bytes /= (1 << (10 * $pow));

        return round($bytes, $precision) . '' . $units[$pow];
    }
}
