<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ManufacturerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'              => $this->id,
            'name'            => $this->name,
            'slug'            => $this->slug,
            'image'           => ($this->image == null || $this->image == "") ? 'no-image.png' : $this->image,
            'description'     => $this->description,
            'category_detail' => $this->category_detail,
        ];
    }
}
