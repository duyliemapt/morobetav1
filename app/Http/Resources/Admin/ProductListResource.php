<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                        => $this->id,
            'category_id'               => $this->category_id,
            'category'                  => $this->category,
            'sku'                       => $this->sku,
            'name'                      => $this->name,
            'slug'                      => $this->slug,
            'image'                     => $this->image,
            'default_price'             => $this->default_price,
            'discount'                  => $this->discount,
            'view'                      => $this->view,
            'is_pricing_manual'         => $this->is_pricing_manual,
            'is_activated'              => $this->is_activated,
            'created_at'                => isset($this->created_at) ? date('Y-m-d H:i:s', strtotime($this->created_at)) : null,
        ];
    }
}
