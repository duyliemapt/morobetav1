<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResourceDetail extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                        => $this->id,
            'category_id'               => $this->category_id,
            'manufacturer_id'           => $this->manufacturer_id,
            'category'                  => $this->category,
            'sku'                       => $this->sku,
            'name'                      => $this->name,
            'slug'                      => $this->slug,
            'image'                     => $this->image,
            'description'               => $this->description,
            'price_from'                => $this->price_from,
            'price_to'                  => $this->price_to,
            'discount'                  => $this->discount,
            'discount_start_time'       => $this->discount_start_time,
            'discount_end_time'         => $this->discount_end_time,
            'regular_price_from'        => $this->regular_price_from,
            'regular_price_to'          => $this->regular_price_to,
            'default_price'             => $this->default_price,
            'standard_construction_fee' => $this->standard_construction_fee,
            'shipping_cost'             => $this->shipping_cost,
            'note'                      => $this->note,
            'policy'                    => $this->policy,
            'possible_delivery_time'    => $this->possible_delivery_time,
            'wish_list'                 => $this->wish_list,
            'view'                      => $this->view,
            'rank'                      => $this->rank,
            'is_available_in_stock'     => $this->is_available_in_stock,
            //'options'                   => $this->options,
            //'details'                   => $this->details,
            //'pricing_rules'             => $this->pricing_rules,
            'keyword_detail'            => $this->keyword_detail,
            'purchases'                 => $this->purchases,
            'images'                    => $this->images,
            'totalItem'                 => $this->totalItem,
            'is_pricing_manual'         => $this->is_pricing_manual,
            'is_activated'              => $this->is_activated,
            'is_deleted'                => $this->is_deleted,
            'created_at'                => isset($this->created_at) ? date('Y-m-d H:i:s', strtotime($this->created_at)) : null,
            'updated_at'                => isset($this->updated_at) ? date('Y-m-d H:i:s', strtotime($this->updated_at)) : null,
        ];
    }
}
