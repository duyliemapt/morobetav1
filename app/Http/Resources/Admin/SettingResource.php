<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class SettingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'         => $this->id,
            'group'      => $this->group,
            'tab'        => $this->tab,
            'slug'       => $this->slug,
            'name'       => $this->name,
            'content'    => $this->content,
            'example'    => $this->example,
            'rule'       => $this->rule,
            'type'       => $this->type,
            'is_publish' => $this->is_publish,
        ];
    }
}
