<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class MenuResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'name'         => $this->name,
            'slug'         => $this->slug,
            'type'         => $this->type,
            'link'         => $this->link,
            'category_id'  => $this->category_id,
            'parent_id'    => $this->parent_id,
            'position'     => $this->position,
            'is_activated' => $this->is_activated,
            'is_deleted'   => $this->is_deleted,
            'created_at'   => isset($this->created_at) ? $this->created_at : null,
            'updated_at'   => isset($this->updated_at) ? $this->updated_at : null,
        ];
    }
}
