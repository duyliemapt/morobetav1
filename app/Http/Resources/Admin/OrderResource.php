<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                      => $this->id,
            'code'                    => $this->code,
            'type'                    => $this->type,
            'user_id'                 => $this->user_id,
            'user'                    => $this->user,
            'total_price'             => $this->total_price,
            'estimated_delivery_date' => $this->estimated_delivery_date,
            'payment_method'          => $this->payment_method,
            'payment_status'          => $this->payment_status,
            'note'                    => $this->note,
            'status'                  => $this->status,
            'payment'                 => ($this->payment != null && count($this->payment) > 0) ? $this->payment[0] : null,
            'delivery'                => ($this->delivery != null && count($this->delivery) > 0) ? $this->delivery[0] : null,
            'order_detail'            => $this->order_detail,
            'is_activated'            => $this->is_activated,
            'created_at'              => isset($this->created_at) ? date('Y-m-d H:i:s', strtotime($this->created_at)) : null,
            'updated_at'              => isset($this->updated_at) ? date('Y-m-d H:i:s', strtotime($this->updated_at)) : null,
        ];
    }
}
