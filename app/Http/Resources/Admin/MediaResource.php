<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class MediaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'name'        => $this->name,
            'slug'        => $this->slug,
            'file'        => $this->file,
            'file_type'   => $this->file_type,
            'extension'   => $this->extension,
            'size'        => $this->size,
            'width'       => $this->width,
            'height'      => $this->height,
            'important'   => $this->important,
            'alt'         => $this->alt,
            'description' => $this->description,
            'type'        => $this->type,
            'parent_id'   => $this->parent_id,
            'parent'      => isset($this->parent) ? $this->parent : [],
            'is_deleted'  => $this->is_deleted,
            'created_at'  => isset($this->created_at) ? date('Y-m-d H:i:s', strtotime($this->created_at)) : null,
            'updated_at'  => isset($this->updated_at) ? date('Y-m-d H:i:s', strtotime($this->updated_at)) : null,
        ];
    }
}
