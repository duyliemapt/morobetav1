<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'product_id'    => $this->product_id,
            'sku'           => $this->sku,
            'slug'          => $this->slug,
            'code'          => $this->code,
            'price'         => $this->price,
            'option_detail' => $this->short_option_detail,
        ];
    }
}
