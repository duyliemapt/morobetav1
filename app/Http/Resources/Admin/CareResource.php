<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class CareResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'full_name'     => $this->full_name,
            'furigana_name' => $this->furigana_name,
            'name'          => $this->name,
            'slug'          => $this->slug,
            'career'        => $this->career,
            'qualification' => $this->qualification,
            'introduction'  => $this->introduction,
            'avatar'        => ($this->avatar != null && $this->avatar != "") ? $this->avatar : 'avatar.png',
            'gender'        => $this->gender,
            'address'       => $this->address,
            'address2'      => $this->address2,
            'email'         => $this->email,
            'phone_number'  => $this->phone_number,
            'counseling_id' => $this->counseling_id,
            'counseling'    => isset($this->counseling) ? $this->counseling : null,
        ];
    }
}
