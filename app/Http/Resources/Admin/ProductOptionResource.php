<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductOptionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'               => $this->id,
            'product_id'       => $this->product_id,
            'name'             => $this->name,
            'slug'             => $this->slug,
            'title'            => $this->title,
            'description'      => $this->description,
            'is_template'      => ($this->is_template == 1),
            'is_check_all'     => ($this->is_check_all == 1),
            'is_indeterminate' => ($this->is_indeterminate == 1),
            'is_show'          => true,
            'value'            => '',
            'details'          => $this->details,
        ];
    }
}
