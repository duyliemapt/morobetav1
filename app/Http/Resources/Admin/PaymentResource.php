<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class PaymentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                     => $this->id,
            'order_id'               => $this->order_id,
            'order'                  => $this->order,
            'code'                   => $this->code,
            'total'                  => $this->total,
            'amount_captured'        => $this->amount_captured,
            'amount_refunded'        => $this->amount_refunded,
            'application_fee'        => $this->application_fee,
            'application_fee_amount' => $this->application_fee_amount,
            'currency'               => $this->currency,
            'type'                   => $this->type,
            'provider'               => $this->provider,
            'customer'               => $this->customer,
            'payment_method'         => $this->payment_method,
            'brand'                  => $this->brand,
            'funding'                => $this->funding,
            'network'                => $this->network,
            'country'                => $this->country,
            'last4'                  => $this->last4,
            'exp_month'              => $this->exp_month,
            'exp_year'               => $this->exp_year,
            'failure_message'        => $this->failure_message,
            'failure_code'           => $this->failure_code,
            'error_content'          => $this->error_content,
            'finished_at'            => $this->finished_at,
            'status'                 => $this->status,
            'created_at'             => isset($this->created_at) ? date('Y-m-d H:i:s', strtotime($this->created_at)) : null,
            'updated_at'             => isset($this->updated_at) ? date('Y-m-d H:i:s', strtotime($this->updated_at)) : null,
        ];
    }
}
