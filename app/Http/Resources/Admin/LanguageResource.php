<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class LanguageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'  => $this->id,
            'key' => $this->key,
            'en'  => $this->en,
            'ja'  => $this->ja,
            'vi'  => $this->vi,
        ];
    }
}
