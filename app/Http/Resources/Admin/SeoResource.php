<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class SeoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'uri'         => $this->uri,
            'title'       => $this->title,
            'keywords'    => $this->keywords,
            'description' => $this->description,
            'image'       => $this->image,
            'note'        => $this->note,
            'type'        => $this->type,
            'is_amp'      => $this->is_amp,
            'created_at'  => isset($this->created_at) ? date('Y-m-d H:i:s', strtotime($this->created_at)) : null,
            'updated_at'  => isset($this->updated_at) ? date('Y-m-d H:i:s', strtotime($this->updated_at)) : null,
        ];
    }
}
