<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'name'         => $this->name,
            'slug'         => $this->slug,
            'image'        => $this->image,
            'parent_id'    => $this->parent_id,
            'parent'       => $this->parent,
            'sub_title'    => $this->sub_title,
            'group'        => $this->group . "",
            'description'  => $this->description,
            'rank'         => $this->rank,
            'manufacturer' => $this->manufacturer,
            'position'     => $this->position,
            'is_activated' => $this->is_activated,
            'is_deleted'   => $this->is_deleted,
            'created_at'   => isset($this->created_at) ? date('Y-m-d H:i:s', strtotime($this->created_at)) : null,
            'updated_at'   => isset($this->updated_at) ? date('Y-m-d H:i:s', strtotime($this->updated_at)) : null,
        ];
    }
}
