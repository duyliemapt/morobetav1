<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class ContactResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                                => $this->id,
            'type'                              => $this->type,
            'name'                              => $this->name,
            'company'                           => $this->company,
            'email'                             => $this->email,
            'phone_number'                      => $this->phone_number,
            'zip_code_1'                        => $this->zip_code_1,
            'zip_code_2'                        => $this->zip_code_2,
            'prefectures'                       => $this->prefectures,
            'address'                           => $this->address,
            'building_name'                     => $this->building_name,
            'content'                           => $this->content,
            'schedule_request_1_date'           => $this->schedule_request_1_date,
            'schedule_request_1_session'        => $this->schedule_request_1_session,
            'schedule_request_1_time'           => $this->schedule_request_1_time,
            'schedule_request_2_date'           => $this->schedule_request_2_date,
            'schedule_request_2_session'        => $this->schedule_request_2_session,
            'schedule_request_2_time'           => $this->schedule_request_2_time,
            'product_id'                        => $this->product_id,
            'product_code'                      => $this->product_code,
            'product_name'                      => $this->product_name,
            'product_image'                     => $this->product_image,
            'product_regular_price_from'        => $this->product_regular_price_from,
            'product_regular_price_to'          => $this->product_regular_price_to,
            'product_price_from'                => $this->product_price_from,
            'product_price_to'                  => $this->product_price_to,
            'product_discount'                  => $this->product_discount,
            'product_sale_cost'                 => $this->product_sale_cost,
            'product_price'                     => $this->product_price,
            'product_standard_construction_fee' => $this->product_standard_construction_fee,
            'product_tax'                       => $this->product_tax,
            'product_shipping_cost'             => $this->product_shipping_cost,
            'product_total_amount'              => $this->product_total_amount,
            'options'                           => $this->options,
            'other_address'                     => $this->other_address,
            'other_address2'                    => $this->other_address2,
            'other_address3'                    => $this->other_address3,
            'user_id'                           => $this->user_id,
            'is_other_address'                  => $this->is_other_address,
            'is_read'                           => $this->is_read,
            'is_activated'                      => $this->is_activated,
            'created_at'                        => isset($this->created_at) ? date('Y-m-d H:i:s', strtotime($this->created_at)) : null,
            'updated_at'                        => isset($this->updated_at) ? date('Y-m-d H:i:s', strtotime($this->updated_at)) : null,
        ];
    }
}
