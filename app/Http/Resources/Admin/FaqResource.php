<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class FaqResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'name'         => $this->name,
            'description'  => $this->description,
            'group'        => $this->group,
            'is_activated' => $this->is_activated,
            'is_deleted'   => $this->is_deleted,
            'created_at'   => isset($this->created_at) ? date('Y-m-d H:i:s', strtotime($this->created_at)) : null,
            'updated_at'   => isset($this->updated_at) ? date('Y-m-d H:i:s', strtotime($this->updated_at)) : null,
        ];
    }
}
