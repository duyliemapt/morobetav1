<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class JobResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'             => $this->id,
            'name'           => $this->name,
            'slug'           => $this->slug,
            'site'           => $this->site,
            'schedule'       => $this->schedule,
            'status'         => $this->status,
            'progressedLink' => $this->progressedLink,
            'totalLink'      => $this->totalLink,
            'created_at'     => isset($this->created_at) ? date('Y-m-d H:i:s', strtotime($this->created_at)) : null,
            'updated_at'     => isset($this->updated_at) ? date('Y-m-d H:i:s', strtotime($this->updated_at)) : null,
        ];
    }
}
