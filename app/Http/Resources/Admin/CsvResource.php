<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class CsvResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'    => $this->type,
            'status'  => $this->status,
            'data_1'  => $this->data_1,
            'data_2'  => $this->data_2,
            'data_3'  => $this->data_3,
            'data_4'  => $this->data_4,
            'data_5'  => $this->data_5,
            'data_6'  => $this->data_6,
            'data_7'  => $this->data_7,
            'data_8'  => $this->data_8,
            'data_9'  => $this->data_9,
            'data_10' => $this->data_10,
            'data_11' => $this->data_11,
            'data_12' => $this->data_12,
            'data_13' => $this->data_13,
            'data_14' => $this->data_14,
            'data_15' => $this->data_15,
            'data_16' => $this->data_16,
            'data_17' => $this->data_17,
            'data_18' => $this->data_18,
            'data_19' => $this->data_19,
            'data_20' => $this->data_20,
            'data_21' => $this->data_21,
            'data_22' => $this->data_22,
            'data_23' => $this->data_23,
            'data_24' => $this->data_24,
            'data_25' => $this->data_25,
        ];
    }
}
