<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class ArticleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'title'             => $this->title,
            'slug'              => $this->slug,
            'description'       => $this->description,
            'editor'            => $this->editor,
            'gu_content'        => $this->gu_content,
            'content'           => $this->content,
            'thumbnail'         => $this->thumbnail,
            'shop_id'           => $this->shop_id,
            'fortune_teller_id' => $this->fortune_teller_id,
            'keyword'           => $this->keyword,
            'keyword_detail'    => isset($this->keyword_detail) ? $this->keyword_detail : [],
            'tag'               => $this->tag,
            'publish_at'        => $this->publish_at,
            'preview'           => $this->preview,
            'important'         => $this->important,
            'teller'            => isset($this->teller) ? $this->teller : [],
            'user_id'           => $this->user_id,
            'user'              => isset($this->user) ? $this->user : [],
            'created_by'        => isset($this->created_by) ? $this->created_by : [],
            'author'            => isset($this->author) ? $this->author : [],
            'feature'           => isset($this->feature) ? $this->feature : [],
            'category'          => isset($this->category) ? $this->category : [],
            'type'              => $this->type,
            'question_status'   => $this->question_status,
            'is_published'      => $this->is_published,
            'is_activated'      => $this->is_activated,
            'is_deleted'        => $this->is_deleted,
            'created_at'        => isset($this->created_at) ? date('Y-m-d H:i:s', strtotime($this->created_at)) : null,
            'updated_at'        => isset($this->updated_at) ? date('Y-m-d H:i:s', strtotime($this->updated_at)) : null,
        ];
    }
}
