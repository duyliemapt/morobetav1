<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                     => $this->id,
            'sku'                    => $this->sku,
            'name'                   => $this->name,
            'slug'                   => $this->slug,
            'sub_title'              => $this->sub_title,
            'group'                  => $this->group,
            'image'                  => ($this->image == null || $this->image == "") ? 'no-image.png' : $this->image,
            'images'                 => $this->images,
            'description'            => $this->description,
            'default_price'          => $this->default_price,
            'discount'               => $this->discount,
            'shipping_cost'          => $this->shipping_cost,
            'note'                   => $this->note,
            'policy'                 => $this->policy,
            'possible_delivery_time' => $this->possible_delivery_time,
            'category_id'            => $this->category_id,
            'category'               => $this->category,
            'color'                  => $this->color,
            'material'               => $this->material,
            'dimension_cm'           => $this->dimension_cm,
            'dimension_scale'        => $this->dimension_scale,
            'height'                 => $this->height,
            'sleeve'                 => $this->sleeve,
            'sleeve_length'          => $this->sleeve_length,
            'primary_width'          => $this->primary_width,
            'second_width'           => $this->second_width,
            'explanation'            => $this->explanation,
            'kimimono'               => $this->kimimono,
            'is_activated'           => $this->is_activated,
            'created_at'             => $this->created_at,
        ];
    }
}
