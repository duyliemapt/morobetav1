<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ArticleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'              => $this->id,
            'title'           => $this->title,
            'slug'            => $this->slug,
            'description'     => $this->description,
            'content'         => $this->content,
            'thumbnail'       => $this->thumbnail,
            'category_detail' => isset($this->category_detail) ? $this->category_detail : [],
            'keyword_detail'  => isset($this->keyword_detail) ? $this->keyword_detail : [],
            'user'            => isset($this->user) ? $this->user : [],
            'question_status' => $this->question_status,
            'publish_at'      => $this->publish_at,
            'is_activated'    => $this->is_activated,
        ];
    }
}
