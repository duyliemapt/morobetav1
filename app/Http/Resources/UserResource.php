<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                  => $this->id,
            'name'                => $this->name,
            'first_name'          => $this->first_name,
            'last_name'           => $this->last_name,
            'full_name'           => $this->full_name,
            'furigana_first_name' => $this->furigana_first_name,
            'furigana_last_name'  => $this->furigana_last_name,
            'furigana_name'       => $this->furigana_name,
            'avatar'              => ($this->avatar != null && $this->avatar != "") ? $this->avatar : 'avatar.png',
            'post_code'           => $this->post_code,
            'city'                => $this->city,
            'address'             => $this->address,
            'address2'            => $this->address2,
            'phone_number'        => $this->phone_number,
            'fax_number'          => $this->fax_number,
            'mobile_tel'          => $this->mobile_tel,
            'emg_tel'             => $this->emg_tel,
            'email'               => $this->email,
            'email2'              => $this->email2,
            'gender'              => $this->gender,
            'career'              => $this->career,
            'date_of_birth'       => $this->date_of_birth,
            'reminder_question'   => $this->reminder_question,
            'reminder_answer'     => $this->reminder_answer,
            'email_subscribe'     => $this->email_subscribe,
            'email_format'        => $this->email_format,
            'point'               => $this->point,
            'point_rate'          => $this->point_rate,
            'language'            => '',
            'created_by'          => $this->created_by,
            'roles'               => array_map(
                function ($role) {
                    return $role['name'];
                },
                $this->roles->toArray()
            ),
            'permissions'         => array_map(
                function ($permission) {
                    return $permission['name'];
                },
                $this->getAllPermissions()->toArray()
            ),
        ];
    }
}
