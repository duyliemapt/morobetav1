<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'              => $this->id,
            'code'            => $this->id,
            'sku'             => $this->sku,
            'name'            => $this->name,
            'slug'            => $this->slug,
            'image'           => ($this->image == null || $this->image == "") ? 'no-image.png' : $this->image,
            'description'     => $this->description,
            'note'            => $this->note,
            'images'          => $this->images,
            'default_price'   => $this->default_price,
            'category_id'     => $this->category_id,
            'category'        => $this->category,
            'scene_id'        => $this->scene_id,
            'scene'           => $this->scene,
            'color'           => $this->color,
            'material'        => $this->material,
            'dimension_cm'    => $this->dimension_cm,
            'dimension_scale' => $this->dimension_scale,
            'height'          => $this->height,
            'sleeve'          => $this->sleeve,
            'sleeve_length'   => $this->sleeve_length,
            'primary_width'   => $this->primary_width,
            'second_width'    => $this->second_width,
            'explanation'     => $this->explanation,
            'kimimono'        => $this->kimimono,
        ];
    }
}
