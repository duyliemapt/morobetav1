<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'name'         => $this->name,
            'slug'         => $this->slug,
            'sub_title'    => $this->sub_title,
            'group'        => $this->group,
            'image'        => ($this->image == null || $this->image == "") ? 'no-image.png' : $this->image,
            'description'  => $this->description,
            'item'         => $this->item,
            'children'     => $this->children,
            'manufacturer' => $this->manufacturer,
        ];
    }
}
