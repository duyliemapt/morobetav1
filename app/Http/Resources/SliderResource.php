<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SliderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'             => $this->id,
            'name'           => $this->name,
            'position'       => $this->position,
            'autoplay'       => $this->autoplay,
            'autoplaySpeed'  => $this->autoplaySpeed,
            'speed'          => $this->speed,
            'slidesToShow'   => $this->slidesToShow,
            'slidesToScroll' => $this->slidesToScroll,
            'infinite'       => $this->infinite,
            'fade'           => $this->fade,
            'dots'           => $this->dots,
            'arrows'         => $this->arrows,
            'pauseOnHover'   => $this->pauseOnHover,
            'pauseOnFocus'   => $this->pauseOnFocus,
            'imageCount'     => $this->imageCount,
            'details'        => $this->details,
        ];
    }
}
