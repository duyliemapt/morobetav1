<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CareResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'slug'          => $this->slug,
            'full_name'     => $this->full_name,
            'furigana_name' => $this->furigana_name,
            'career'        => $this->career,
            'qualification' => $this->qualification,
            'introduction'  => $this->introduction,
            'avatar'        => $this->avatar,
            'gender'        => $this->gender,
            'counseling_id' => $this->counseling_id,
            'counseling'    => isset($this->counseling) ? $this->counseling : null,
        ];
    }
}
