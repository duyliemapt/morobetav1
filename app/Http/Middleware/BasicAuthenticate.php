<?php

namespace App\Http\Middleware;

use Closure;

class BasicAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (config('basic.active') == true && config("basic.username") != null && config("basic.username") != "" && config("basic.password") != null && config("basic.password") != "") {
            $user = trim($request->getUser());
            $password = trim($request->getPassword());
            if ($user == config("basic.username") && $password == config("basic.password")) {
                return $next($request);
            }

            return response('', 401, ['WWW-Authenticate' => 'Basic']);
        } else {
            return $next($request);
        }
    }
}
