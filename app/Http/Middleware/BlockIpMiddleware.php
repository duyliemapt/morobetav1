<?php

namespace App\Http\Middleware;

use App\BlockIP;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class BlockIpMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        /*$whiteIPs = Cache::remember('white-ips', (7*24*60*60), function () {
            return BlockIP::select('ip')
                ->isPublished()
                ->orderBy('id')
                ->pluck('ip')
                ->toArray();
        });

        if (!in_array($request->ip(), $whiteIPs)) {
            abort(403, "You are restricted to access the site.");
        }*/

        return $next($request);
    }
}
