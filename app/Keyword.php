<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keyword extends Model
{
    protected $table = 'keywords';

    protected $appends = ['keywordCount'];

    protected $fillable = ['id', 'name', 'description', 'position', 'is_note', 'is_related', 'is_activated', 'is_deleted', 'created_at', 'updated_at'];

    public function scopeIsDeleted($query)
    {
        return $query->where('is_deleted', true);
    }

    public function scopeNotDeleted($query)
    {
        return $query->where('is_deleted', false);
    }

    public function scopeActivated($query)
    {
        return $query->where('is_activated', true);
    }

    public function scopeNotActivated($query)
    {
        return $query->where('is_activated', false);
    }

    public function scopePublished($query)
    {
        return $query->where('is_deleted', false)->where('is_activated', true);
    }

    public function detail()
    {
        return $this->hasMany(KeywordDetail::class, 'keyword_id', 'id');
    }

    public function getKeywordCountAttribute() {
        return $this->detail()->count();
    }
}
