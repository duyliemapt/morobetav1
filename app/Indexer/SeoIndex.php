<?php

namespace App\Indexer;

use Fobia\Database\SphinxConnection\Eloquent\Model;

class SeoIndex extends Model
{
    protected $table = 'seoes_index';

    protected $casts = [
        'id'      => 'integer',
        'post_id' => 'integer',
        'is_amp'  => 'integer',
    ];
}
