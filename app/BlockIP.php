<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BlockIP extends BaseModel
{
    use HasFactory;

    protected $table = 'block_ips';

    protected $fillable = ['id', 'ip', 'description', 'incorrect', 'during_time', 'is_activated', 'is_deleted', 'created_at', 'updated_at'];
}
