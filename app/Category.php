<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends BaseModel
{
    protected $table = 'categories';

    protected $fillable = ['id', 'name', 'slug', 'image', 'parent_id', 'sub_title', 'group', 'description', 'rank', 'position', 'is_activated', 'is_deleted', 'created_at', 'updated_at'];

    public function parent() {
        return $this->belongsTo(Category::class, 'parent_id', 'id')
            ->with('parent')
            ->select('id', 'name', 'slug', 'parent_id')
            ->where('is_deleted', false)
            ->where('is_activated', true)
            ->orderBy('position')
            ->orderBy('id', 'ASC');
    }

    public function children() {
        return $this->hasMany(Category::class, 'parent_id', 'id')
            ->with('children')
            ->select('id', 'name', 'slug', 'parent_id')
            ->where('is_deleted', false)
            ->where('is_activated', true)
            ->orderBy('position')
            ->orderBy('id', 'ASC');
    }

    public function item() {
        return $this->hasMany(Category::class, 'parent_id', 'id')
            ->select('id', 'name', 'slug', 'parent_id')
            ->where('is_deleted', false)
            ->where('is_activated', true)
            ->orderBy('position')
            ->orderBy('id', 'ASC');
    }

    public function manufacturer() {
        return $this->hasMany(CategoryManufacturer::class, 'category_id', 'id')->with('manufacturer_detail');
    }
}
