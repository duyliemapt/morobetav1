<?php

namespace App;

class Setting extends BaseModel
{
    protected $table = 'settings';

    protected $fillable = ['id', 'group', 'tab', 'slug', 'name', 'content', 'example', 'rule', 'type', 'is_publish', 'is_activated', 'is_deleted', 'created_at', 'updated_at'];

    public function scopeIsPublish($query)
    {
        return $query->where('is_publish', true);
    }
}
