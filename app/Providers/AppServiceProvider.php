<?php

namespace App\Providers;

use App\FilterWord;
use App\Setting;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Cache::remember('settings', 22*60, function() {
            $settings = Setting::notDeleted()->isActivated()->pluck('content', 'slug');
            config()->set(['settings' => $settings]);
        });
    }
}
