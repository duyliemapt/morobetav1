<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KeywordDetail extends BaseModel
{
    protected $table = 'keyword_details';

    protected $fillable = ['id', 'keyword_id', 'ref_id', 'type', 'created_at', 'updated_at'];

    public function keyword() {
        return $this->belongsTo(Keyword::class, 'keyword_id', 'id')->select('id', 'name');
    }
}
