<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPurchaseMethod extends Model
{
    protected $table = 'product_purchase_methods';

    protected $fillable = ['id', 'product_id', 'name', 'slug', 'description', 'created_at', 'updated_at'];
}
