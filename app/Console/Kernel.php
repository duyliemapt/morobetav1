<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        //$schedule->command('cron:magicwands')->everyMinute(); //->withoutOverlapping();

        /*$jobs = Job::where('status', 1)->whereNotNull('command')->where('schedule', 'everyMinute')->get();
        foreach ($jobs as $job) {
            $schedule->command($job->command)->everyMinute()->withoutOverlapping();
        }*/
        //crontab -e
        //* * * * * cd /var/www/html/linkdata && php artisan schedule:run >> /dev/null 2>&1
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
