<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ReloadIndexer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'indexer:reload';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto reload all indexing of data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $basePath = base_path();
        exec("cd " . $basePath . "/sphinx-3.3.1/bin && ./indexer --all --rotate >> " . $basePath . "/storage/logs/laravel.log 2>&1");
        logger("[Indexer] Updated data.\r\n");
        //print "Updated Indexing.\r\n";
    }
}
