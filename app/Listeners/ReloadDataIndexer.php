<?php

namespace App\Listeners;

use App\Events\SaveDataEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Artisan;

class ReloadDataIndexer
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SaveDataEvent  $event
     * @return void
     */
    public function handle(SaveDataEvent $event)
    {
        //Artisan::call('indexer:reload');
        //Artisan::call('cache:clear');
    }
}
