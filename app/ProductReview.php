<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductReview extends BaseModel
{
    protected $table = 'product_reviews';

    protected $fillable = ['id', 'product_id', 'user_id', 'rate', 'comment', 'is_activated', 'is_deleted', 'created_at', 'updated_at'];
}
