<?php

namespace App\Helpers;
use ImageCross;
use Intervention\Image\Exception\NotReadableException;

class Helper
{
    public static function slug($string) {
        $table = array(
            'Š'=>'S', 'š'=>'s', 'Đ'=>'D', 'đ'=>'d', 'Ž'=>'Z', 'ž'=>'z',
            'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c', 'À'=>'A', 'Á'=>'A',
            'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C',
            'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I',
            'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
            'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U',
            'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a',
            'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
            'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i',
            'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
            'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u',
            'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', 'Ŕ'=>'R',
            'ŕ'=>'r', '/'=>'-', ' '=>'-', ':'=> '', ',' =>'', '('=> '',
            ')' =>'', '.' => '', '-–'=>'-', '-–-'=>'-', '’'=>'', 'ầ' => 'a',
            'ấ' => 'a', 'ư' => 'u', 'ơ' => 'o', 'ă' => 'a', 'ứ' => 'u',
            'ạ' => 'a' ,'ế' =>'e','ề'=>'e','ệ' =>'e','ổ'=>'o','ẻ'=>'e',
            'ủ' => 'u' ,'ị' =>'i','ử'=>'u','đ' =>'d','ỏ' =>'o','ắ' =>'a',
            'ẫ'=>'a','ố'=>'o','ự'=>'u','ớ'=>'o','ồ'=>'o','ợ'=>'o', "'" => '',
            'ả' => 'a', 'ẩ' => 'a', '%' => '', 'ậ' => 'a', 'ể' => 'e', 'ặ' => 'a',
        );
        $string = str_replace("--","-",$string);
        $string = str_replace(" ","",$string);
        $string = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $string);

        return strtolower(strtr($string, $table));
    }

    public static function createThumb($file, $filePath, $width = 135, $height = 150, $renewName = false)
    {
        try {
            $fileNewName = basename($file);
            if ($renewName) $fileNewName = md5(date('YmdHis') . rand(1, 9999)) . '.jpg';
            $imgCross = ImageCross::make($file)->orientate(); //->save($filePath . '/' . $fileNewName);
            $imgCross->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save($filePath . "thumb_{$width}x{$height}/" . $fileNewName);
            return $fileNewName;
        } catch (\Exception $exception) {
            return null;
        }
    }

    public static function getImageFile($file, $filePath, $renewName = false)
    {
        if (!isset($file) || $file == null || $file == "") return null;

        try {
            $fileNewName = basename($file);
            if ($renewName) $fileNewName = md5(date('YmdHis') . rand(1, 9999)) . '.jpg';
            ImageCross::make($file)->orientate()->save($filePath . $fileNewName);
            return $fileNewName;
        } catch (\Exception $exception) {
            return null;
        }
    }

    public static function getStoreImage($fileUrl, $filePath)
    {
        if (!isset($fileUrl) || $fileUrl == null || $fileUrl == "") return "no-image.png";

        try {
            $fileNewName = md5(date('YmdHis') . rand(1, 9999)) . '.jpg';
            $imgCross = ImageCross::make($fileUrl)->orientate()->save($filePath . $fileNewName);
            $imgCross->fit(760, 550, function ($constraint) {
                $constraint->upsize();
            })->save($filePath . "thumb_760x550/" . $fileNewName);
            $imgCross->fit(60, 60, function ($constraint) {
                $constraint->upsize();
            })->save($filePath . "thumb_60x60/" . $fileNewName);
            return $fileNewName;
        } catch (\Exception $exception) {
            return "no-image.png";
        }
    }    

    public static function createThumbFit($file, $filePath, $width = 460, $height = 360, $renewName = false)
    {
        try {
            $fileNewName = basename($file);
            if ($renewName) $fileNewName = md5(date('YmdHis') . rand(1, 9999)) . '.jpg';
            $imgCross = ImageCross::make($file)->orientate();
            $imgCross->fit($width, $height, function ($constraint) {
                $constraint->upsize();
            })->save($filePath . "thumb_{$width}x{$height}/" . $fileNewName);
            return $fileNewName;
        } catch (\Exception $exception) {
            return null;
        }
    }

    public static function copyThumbFit($file, $filePath, $width = 460, $height = 360)
    {
        $fileNewName = basename($file);
        try {
            copy($file, $filePath . "thumb_{$width}x{$height}/" . $fileNewName);
        } catch (\Exception $exception) {
            logger("[Error] Image method copying error: " . $fileNewName);
        }
    }

    public static function createThumbDetectSize($file, $filePath, $width = 460, $height = 360, $renewName = false)
    {
        try {
            $fileNewName = basename($file);
            if ($renewName) $fileNewName = md5(date('YmdHis') . rand(1, 9999)) . '.jpg';
            $img = ImageCross::make($file)->orientate();
            $imgWidth = $img->width();
            $imgHeight = $img->height();

            if ($imgHeight > $imgWidth) {
                $img->resize(null, 360, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })->save($filePath . "thumb_{$width}x{$height}/" . $fileNewName);
            } else {
                $img->resize(460, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })->save($filePath . "thumb_{$width}x{$height}/" . $fileNewName);
            }

            return $fileNewName;
        } catch (\Exception $exception) {
            return null;
        }
    }

    public static function randColor() {
        return '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
    }

    public static function generateRandomString($length = 8) {
        return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
    }

    public static function convertJpgToWebp($name) {
        $result = false;
        try {
            $filePath = public_path('uploads/files/');
            if (!file_exists($filePath . $name)) return false;
            $key = "87252hahagawbj";
            $webpFile = str_replace(".jpg" . $key, "", $name . $key);
            $webpFile = str_replace(".png" . $key, "", $webpFile);
            $webpFile = str_replace(".jpeg" . $key, "", $webpFile);
            $webpFile = str_replace(".PNG" . $key, "", $webpFile);
            $webpFile = str_replace(".JPG" . $key, "", $webpFile);
            $webpFile = str_replace(".JPEG" . $key, "", $webpFile);
            $webpFile .= ".webp";
            ImageCross::make($filePath . $name)->encode('webp', 90)->save($filePath . $webpFile);
            $result = true;
        } catch (NotReadableException $exception) {
            logger("[Error] Image Cross: " . $exception->getMessage());
        } catch (\Exception $exception) {
            logger("[Error] Image Cross: " . $exception->getMessage());
        } finally {
            return $result;
        }
    }
}
