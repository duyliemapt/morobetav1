<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seo extends Model
{
    protected $table = 'seo_metas';

    protected $fillable = ['id', 'uri', 'title', 'keywords', 'description', 'image', 'type', 'post_id', 'created_at', 'updated_at'];
}
