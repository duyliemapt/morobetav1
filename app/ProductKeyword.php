<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductKeyword extends Model
{
    protected $table = 'product_keywords';

    protected $fillable = ['id', 'product_id', 'keyword_id', 'created_at', 'updated_at'];

    public function keyword() {
        return $this->belongsTo(Keyword::class, 'keyword_id', 'id')->select('id', 'name', 'description');
    }
}
