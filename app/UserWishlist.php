<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserWishlist extends Model
{
    protected $table = 'user_wishlists';

    protected $fillable = ['id', 'user_id', 'product_id', 'created_at', 'updated_at'];
}
