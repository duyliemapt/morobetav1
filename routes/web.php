<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home')->middleware('basic');

/*Route::get('pdf/estimate/{purchase}/{code}', 'HomeController@productExportPdf')->name('product.export.pdf');*/

/*Route::get('stripe', 'StripePaymentController@stripe')->name('stripe.payment');
Route::post('stripe', 'StripePaymentController@stripePost')->name('stripe.payment.store');
Route::get('stripe/payment/webhook', 'HomeController@stripePaymentWebhook')->name('stripe.payment.webhook');*/

Route::group(['prefix' => 'laraberg'], function() { //'middleware' => ['web', 'auth']
    Route::get('oembed', '\VanOns\Laraberg\Controllers\OEmbedController@show');
    Route::get('blocks', 'GutenburgController@show'); //'\VanOns\Laraberg\Controllers\BlockRendererController@show'
    Route::get('block-renderer', '\VanOns\Laraberg\Controllers\BlockRendererController@show');
    /*Route::apiResource('blocks', '\VanOns\Laraberg\Http\Controllers\BlockController');
    Route::get('oembed', '\VanOns\Laraberg\Http\Controllers\OEmbedController@__invoke');*/
});

Route::get('files/thumb_{width}x{height}/{file}', 'MediaController@fileThumb')->name('mediaThumb');
Route::get('files/{file}', 'MediaController@file')->name('media');
Route::get('download/{file}', 'MediaController@download')->name('mediaDownload');
/*Route::post('login', [ 'as' => 'login', 'uses' => 'HomeController@index']);*/

Route::get('bbs/feed', 'RssFeedController@bbsFeed')->name('feed.column'); //->middleware('etag');
Route::get('column/feed', 'RssFeedController@columnFeed')->name('feed.bbs'); //->middleware('etag');

Route::get('sitemap.xml', 'SiteMapController@index')->name('sitemap');
Route::get('sitemap-page.xml', 'SiteMapController@page')->name('sitemap.page');
Route::get('sitemap-bbs.xml', 'SiteMapController@bbs')->name('sitemap.bbs');
Route::get('sitemap-column.xml', 'SiteMapController@column')->name('sitemap.column');
Route::get('sitemap-care.xml', 'SiteMapController@care')->name('sitemap.care');

Route::group(['middleware' => 'web', 'prefix' => 'admin'], function() {
    Route::middleware(['blockIP'])->group(function () {
        Route::get('/',      [ 'as' => 'admin.dashboard',       'uses' => 'Admin\AdminController@index' ]); //->middleware('basic');
        Route::get('/login', [ 'as' => 'admin.login',           'uses' => 'Admin\AdminController@index' ]); //->middleware('basic');
        Route::get('/{any}', [ 'as' => 'admin.any',             'uses' => 'Admin\AdminController@index' ])->where('any', '.*');
    });
});

Route::get('/{any}', 'HomeController@index')->where('any', '.*')->name('home.any');
