<?php

/*use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Laravue\Faker;
use \App\Laravue\JsonResponse;*/

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get(  'menu',                                [ 'as' => 'menu.front.index',                 'uses' => 'MenuController@index' ]);
Route::get(  'menu/header',                         [ 'as' => 'menu.front.header',                'uses' => 'MenuController@header' ])->middleware('etag');
Route::get(  'menu/footer',                         [ 'as' => 'menu.front.footer',                'uses' => 'MenuController@footer' ])->middleware('etag');

Route::get(  'category',                            [ 'as' => 'category.front.index',             'uses' => 'CategoryController@index' ]);
Route::get(  'category/top',                        [ 'as' => 'category.front.top',               'uses' => 'CategoryController@top' ]);
Route::get(  'category/{id}',                       [ 'as' => 'category.front.show',              'uses' => 'CategoryController@show' ]);
Route::get(  'category/slug/{slug}',                [ 'as' => 'category.front.slug',              'uses' => 'CategoryController@slug' ]);
Route::get(  'category/parent/list',                [ 'as' => 'category.front.parent',            'uses' => 'CategoryController@onlyParent' ]);
Route::get(  'category/children/list',              [ 'as' => 'category.front.children',          'uses' => 'CategoryController@onlyChildren' ]);
Route::post( 'category/image/progress',             [ 'as' => 'category.front.image',             'uses' => 'CategoryController@progressImage' ]);
Route::get(  'category/image/check/{id}',           [ 'as' => 'category.front.image.check',       'uses' => 'CategoryController@checkProgressImage' ]);

Route::get(  'scene',                            [ 'as' => 'scene.front.index',             'uses' => 'SceneController@index' ]);
Route::get(  'scene/top',                        [ 'as' => 'scene.front.top',               'uses' => 'SceneController@top' ]);
Route::get(  'scene/{id}',                       [ 'as' => 'scene.front.show',              'uses' => 'SceneController@show' ]);
Route::get(  'scene/slug/{slug}',                [ 'as' => 'scene.front.slug',              'uses' => 'SceneController@slug' ]);
Route::get(  'scene/parent/list',                [ 'as' => 'scene.front.parent',            'uses' => 'SceneController@onlyParent' ]);
Route::get(  'scene/children/list',              [ 'as' => 'scene.front.children',          'uses' => 'SceneController@onlyChildren' ]);
Route::post( 'scene/image/progress',             [ 'as' => 'scene.front.image',             'uses' => 'SceneController@progressImage' ]);
Route::get(  'scene/image/check/{id}',           [ 'as' => 'scene.front.image.check',       'uses' => 'SceneController@checkProgressImage' ]);

Route::get(  'condition',                            [ 'as' => 'condition.front.index',             'uses' => 'ConditionController@index' ]);
Route::get(  'condition/top',                        [ 'as' => 'condition.front.top',               'uses' => 'ConditionController@top' ]);
Route::get(  'condition/{id}',                       [ 'as' => 'condition.front.show',              'uses' => 'ConditionController@show' ]);
Route::get(  'condition/slug/{slug}',                [ 'as' => 'condition.front.slug',              'uses' => 'ConditionController@slug' ]);
Route::get(  'condition/parent/list',                [ 'as' => 'condition.front.parent',            'uses' => 'ConditionController@onlyParent' ]);
Route::get(  'condition/children/list',              [ 'as' => 'condition.front.children',          'uses' => 'ConditionController@onlyChildren' ]);
Route::post( 'condition/image/progress',             [ 'as' => 'condition.front.image',             'uses' => 'ConditionController@progressImage' ]);
Route::get(  'condition/image/check/{id}',           [ 'as' => 'condition.front.image.check',       'uses' => 'ConditionController@checkProgressImage' ]);

Route::get(  'product',                            [ 'as' => 'product.front.index',                 'uses' => 'ProductController@index' ]);
Route::get(  'product/mypage',                     [ 'as' => 'product.front.mypage',                'uses' => 'ProductController@mypage' ]);
Route::get(  'product/top',                        [ 'as' => 'product.front.top',                   'uses' => 'ProductController@top' ]);
Route::get(  'product/ranking',                    [ 'as' => 'product.front.ranking',               'uses' => 'ProductController@ranking' ]);
Route::get(  'product/ranking/category/{id}',      [ 'as' => 'product.front.ranking.category',      'uses' => 'ProductController@rankingCategory' ]);
Route::get(  'product/ranking/manufacturer/{id}',  [ 'as' => 'product.front.ranking.manufacturer',  'uses' => 'ProductController@rankingManufacturer' ]);
Route::get(  'product/{id}',                       [ 'as' => 'product.front.show',                  'uses' => 'ProductController@show' ]);
Route::post( 'product/estimate/{id}',              [ 'as' => 'product.front.estimate.check',        'uses' => 'ProductController@estimateCheck' ]);
Route::get(  'product/estimate/{id}/{code}',       [ 'as' => 'product.front.estimate.show',         'uses' => 'ProductController@estimateShow' ]);

Route::get(  'manufacturer',                        [ 'as' => 'manufacturer.front.index',         'uses' => 'ManufacturerController@index' ]);
Route::get(  'manufacturer/top',                    [ 'as' => 'manufacturer.front.top',           'uses' => 'ManufacturerController@top' ]);
Route::get(  'manufacturer/{id}',                   [ 'as' => 'manufacturer.front.show',          'uses' => 'ManufacturerController@show' ]);

Route::get(     'cart',                             [ 'as' => 'cart.front.index',                 'uses' => 'CartController@index' ]);
Route::get(     'cart/{id}',                        [ 'as' => 'cart.front.show',                  'uses' => 'CartController@show' ]);
Route::get(     'cart/statistic/all',               [ 'as' => 'cart.front.statistic',             'uses' => 'CartController@statistic' ]);
Route::post(    'cart',                             [ 'as' => 'cart.front.store',                 'uses' => 'CartController@store' ]);
Route::post(    'cart/order',                       [ 'as' => 'cart.front.order',                 'uses' => 'CartController@order' ]);
Route::put(     'cart/{id}',                        [ 'as' => 'cart.front.update',                'uses' => 'CartController@update' ]);
Route::delete(  'cart/{id}',                        [ 'as' => 'cart.front.destroy',               'uses' => 'CartController@destroy' ]);

Route::get(  'keyword',                             [ 'as' => 'keyword.front.index',              'uses' => 'KeywordController@index' ]);
Route::get(  'keyword/top',                         [ 'as' => 'keyword.front.top',                'uses' => 'KeywordController@topKeyword' ])->middleware('etag');
Route::get(  'keyword/{id}',                        [ 'as' => 'keyword.front.show.id',            'uses' => 'KeywordController@show' ]);

Route::get(  'faq',                                 [ 'as' => 'faq.front.index',                  'uses' => 'FaqController@index' ]);
Route::post( 'seo/uri',                             [ 'as' => 'seo.front.uri',                    'uses' => 'SeoController@index' ])->middleware('etag');

Route::post( 'header/uri',                          [ 'as' => 'header.front.uri',                 'uses' => 'HeaderController@index' ]);

Route::get(  'slider',                              [ 'as' => 'slider.front.index',               'uses' => 'SliderController@index' ]);
Route::get(  'slider/top/page',                     [ 'as' => 'slider.front.topPage',             'uses' => 'SliderController@topPage' ]);
Route::get(  'slider/position/{position}',          [ 'as' => 'slider.front.position',            'uses' => 'SliderController@position' ]);
Route::get(  'slider/show/position/{position}',     [ 'as' => 'slider.front.show.position',       'uses' => 'SliderController@showPosition' ])->middleware('etag');

Route::get(  'article',                             [ 'as' => 'article.front.index',              'uses' => 'ArticleController@index' ]);
Route::get(  'article/top/hot',                     [ 'as' => 'article.front.hot',                'uses' => 'ArticleController@hot' ]);
Route::get(  'article/top/recent',                  [ 'as' => 'article.front.recent',             'uses' => 'ArticleController@recent' ]);
Route::get(  'article/top/ranking',                 [ 'as' => 'article.front.ranking',            'uses' => 'ArticleController@ranking' ]);
Route::get(  'article/ranking/list',                [ 'as' => 'article.front.list',               'uses' => 'ArticleController@rankingList' ]);
Route::get(  'article/question/post',               [ 'as' => 'article.front.question',           'uses' => 'ArticleController@questionPost' ]);
Route::get(  'article/{id}',                        [ 'as' => 'article.front.show.id',            'uses' => 'ArticleController@show' ]);
Route::get(  'article/slug/{slug}',                 [ 'as' => 'article.front.show.slug',          'uses' => 'ArticleController@showBySlug' ]);
Route::post( 'article',                             [ 'as' => 'article.front.store',              'uses' => 'ArticleController@store' ]);

Route::post( 'register',                            [ 'as' => 'register.front.store',             'uses' => 'RegisterController@store' ]);
Route::post( 'register/email',                      [ 'as' => 'register.front.store.email',       'uses' => 'RegisterController@storeEmail' ]);
Route::post( 'register/check/{code}',               [ 'as' => 'register.front.check.code',        'uses' => 'RegisterController@checkCode' ]);

Route::post( 'users/forgot-password',               [ 'as' => 'forgot.front.store',               'uses' => 'RegisterController@forgotPassword' ]);
Route::post( 'users/reset-password/{code}',         [ 'as' => 'reset.front.store',                'uses' => 'RegisterController@resetPassword' ]);

Route::post( 'contact',                             [ 'as' => 'contact.front.store',              'uses' => 'ContactController@store' ]);
Route::post( 'contact/survey/{code}',               [ 'as' => 'contact.front.store.survey',       'uses' => 'ContactController@storeSurvey' ]);
Route::get(  'setting/all',                         [ 'as' => 'setting.front.all',                'uses' => 'SettingController@all' ])->middleware('etag');
Route::post( 'setting/upload/image',                [ 'as' => 'setting.front.upload',             'uses' => 'SettingController@uploadImage' ]);
Route::post( 'media/upload/file',                   [ 'as' => 'setting.front.upload.image',       'uses' => 'SettingController@uploadMedia' ]);

Route::post( 'product/register/rental',             [ 'as' => 'product.front.register.rental',    'uses' => 'ProductController@register' ]);
Route::post( 'product/try/faceswap',                [ 'as' => 'product.front.try.faceswap',       'uses' => 'ProductController@faceswap' ]);
Route::get(  'product/faceswap/{code}',             [ 'as' => 'product.front.check.faceswap',     'uses' => 'ProductController@faceswapChecking' ]);

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::post( 'auth/login',      'AuthController@login');
Route::post( 'auth/login/user', 'AuthController@loginUser');

Route::group(['middleware' => 'auth:api'], function () {
    Route::get(         'user',                     'AuthController@getUser');
    Route::get(         'auth/user',                'AuthController@user');
    Route::post(        'auth/logout',              'AuthController@logout');

    Route::get(         'users',                    'UserController@index')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_USER_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_USER_ONLY_MANAGE);
    Route::get(         'users/assigned/list',      'UserController@assigned')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_USER_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_USER_ONLY_MANAGE);
    Route::get(         'users/{user}',             'UserController@show')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_USER_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_USER_EDIT_MANAGE);
    Route::post(        'users',                    'UserController@store');
    Route::put(         'users/profile/{user}',     'UserController@updateProfile');
    Route::put(         'users/{user}',             'UserController@update')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_USER_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_USER_EDIT_MANAGE);
    Route::delete(      'users/{user}',             'UserController@destroy')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_USER_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_USER_DELETE_MANAGE);
    Route::get(         'users/{user}/permissions', 'UserController@permissions')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_USER_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_USER_EDIT_MANAGE);
    Route::put(         'users/{user}/permissions', 'UserController@updatePermissions')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_USER_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_USER_EDIT_MANAGE);

    Route::apiResource( 'roles',                    'RoleController')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PERMISSION_MANAGE);
    Route::get(         'roles/list/only',          'RoleController@roleList')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PERMISSION_MANAGE);
    Route::get(         'roles/permission/all',     'RoleController@permissionsAll')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PERMISSION_MANAGE);
    Route::get(         'roles/{role}/permissions', 'RoleController@permissions')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PERMISSION_MANAGE);
    Route::apiResource( 'permissions',              'PermissionController')->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PERMISSION_MANAGE);


    Route::group(['prefix' => 'admin'], function() {
        Route::get(         'dashboard/statistic',          [ 'as' => 'dashboard.statistic',            'uses' => 'Admin\DashboardController@statistic' ]);

        Route::get(         'keyword',                     [ 'as' => 'keyword.index',                 'uses' => 'Admin\KeywordController@index' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_KEYWORD_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_KEYWORD_ONLY_MANAGE);
        Route::get(         'keyword/all',                 [ 'as' => 'keyword.all',                   'uses' => 'Admin\KeywordController@all' ]);
        Route::get(         'keyword/tags',                [ 'as' => 'keyword.tags',                  'uses' => 'Admin\KeywordController@tags' ]);
        Route::get(         'keyword/{id}',                [ 'as' => 'keyword.show',                  'uses' => 'Admin\KeywordController@show' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_KEYWORD_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_KEYWORD_EDIT_MANAGE);
        Route::post(        'keyword',                     [ 'as' => 'keyword.store',                 'uses' => 'Admin\KeywordController@store' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_KEYWORD_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_KEYWORD_CREATE_MANAGE);
        Route::post(        'keyword/position',            [ 'as' => 'keyword.position',              'uses' => 'Admin\KeywordController@position' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_KEYWORD_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_KEYWORD_CREATE_MANAGE);
        Route::put(         'keyword/{id}',                [ 'as' => 'keyword.update',                'uses' => 'Admin\KeywordController@update' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_KEYWORD_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_KEYWORD_EDIT_MANAGE);
        Route::delete(      'keyword/{id}',                [ 'as' => 'keyword.destroy',               'uses' => 'Admin\KeywordController@destroy' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_KEYWORD_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_KEYWORD_DELETE_MANAGE);
        Route::delete(      'keyword/multiple/destroy',    [ 'as' => 'keyword.multiple.destroy',      'uses' => 'Admin\KeywordController@destroyMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_KEYWORD_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_KEYWORD_DELETE_MANAGE);
        Route::post(        'keyword/multiple/activate',   [ 'as' => 'keyword.multiple.activate',     'uses' => 'Admin\KeywordController@activateMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_KEYWORD_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_KEYWORD_EDIT_MANAGE);
        Route::post(        'keyword/multiple/deactivate', [ 'as' => 'keyword.multiple.deactivate',   'uses' => 'Admin\KeywordController@deactivateMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_KEYWORD_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_KEYWORD_EDIT_MANAGE);

        Route::get(         'faq',                     [ 'as' => 'faq.index',                 'uses' => 'Admin\FaqController@index' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_FAQ_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_FAQ_ONLY_MANAGE);
        Route::get(         'faq/all',                 [ 'as' => 'faq.all',                   'uses' => 'Admin\FaqController@all' ]);
        Route::get(         'faq/tags',                [ 'as' => 'faq.tags',                  'uses' => 'Admin\FaqController@tags' ]);
        Route::get(         'faq/{id}',                [ 'as' => 'faq.show',                  'uses' => 'Admin\FaqController@show' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_FAQ_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_FAQ_EDIT_MANAGE);
        Route::post(        'faq',                     [ 'as' => 'faq.store',                 'uses' => 'Admin\FaqController@store' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_FAQ_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_FAQ_CREATE_MANAGE);
        Route::put(         'faq/{id}',                [ 'as' => 'faq.update',                'uses' => 'Admin\FaqController@update' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_FAQ_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_FAQ_EDIT_MANAGE);
        Route::delete(      'faq/{id}',                [ 'as' => 'faq.destroy',               'uses' => 'Admin\FaqController@destroy' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_FAQ_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_FAQ_DELETE_MANAGE);
        Route::delete(      'faq/multiple/destroy',    [ 'as' => 'faq.multiple.destroy',      'uses' => 'Admin\FaqController@destroyMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_FAQ_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_FAQ_DELETE_MANAGE);
        Route::post(        'faq/multiple/activate',   [ 'as' => 'faq.multiple.activate',     'uses' => 'Admin\FaqController@activateMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_FAQ_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_FAQ_EDIT_MANAGE);
        Route::post(        'faq/multiple/deactivate', [ 'as' => 'faq.multiple.deactivate',   'uses' => 'Admin\FaqController@deactivateMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_FAQ_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_FAQ_EDIT_MANAGE);

        Route::get(         'filter',                     [ 'as' => 'filter.index',                 'uses' => 'Admin\FilterController@index' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_FILTER_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_FILTER_ONLY_MANAGE);
        Route::get(         'filter/all',                 [ 'as' => 'filter.all',                   'uses' => 'Admin\FilterController@all' ]);
        Route::get(         'filter/{id}',                [ 'as' => 'filter.show',                  'uses' => 'Admin\FilterController@show' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_FILTER_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_FILTER_EDIT_MANAGE);
        Route::post(        'filter',                     [ 'as' => 'filter.store',                 'uses' => 'Admin\FilterController@store' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_FILTER_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_FILTER_CREATE_MANAGE);
        Route::put(         'filter/{id}',                [ 'as' => 'filter.update',                'uses' => 'Admin\FilterController@update' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_FILTER_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_FILTER_EDIT_MANAGE);
        Route::delete(      'filter/{id}',                [ 'as' => 'filter.destroy',               'uses' => 'Admin\FilterController@destroy' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_FILTER_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_FILTER_DELETE_MANAGE);
        Route::delete(      'filter/multiple/destroy',    [ 'as' => 'filter.multiple.destroy',      'uses' => 'Admin\FilterController@destroyMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_FILTER_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_FILTER_DELETE_MANAGE);
        Route::post(        'filter/multiple/activate',   [ 'as' => 'filter.multiple.activate',     'uses' => 'Admin\FilterController@activateMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_FILTER_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_FILTER_EDIT_MANAGE);
        Route::post(        'filter/multiple/deactivate', [ 'as' => 'filter.multiple.deactivate',   'uses' => 'Admin\FilterController@deactivateMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_FILTER_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_FILTER_EDIT_MANAGE);

        Route::get(         'menu',                     [ 'as' => 'menu.index',                 'uses' => 'Admin\MenuController@index' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MENU_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_MENU_ONLY_MANAGE);
        Route::get(         'menu/all',                 [ 'as' => 'menu.all',                   'uses' => 'Admin\MenuController@all' ]);
        Route::get(         'menu/all/nested',          [ 'as' => 'menu.all.nested',            'uses' => 'Admin\MenuController@allNested' ]);
        Route::get(         'menu/{id}',                [ 'as' => 'menu.show',                  'uses' => 'Admin\MenuController@show' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MENU_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_MENU_EDIT_MANAGE);
        Route::post(        'menu',                     [ 'as' => 'menu.store',                 'uses' => 'Admin\MenuController@store' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MENU_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_MENU_CREATE_MANAGE);
        Route::post(        'menu/position',            [ 'as' => 'menu.position',              'uses' => 'Admin\MenuController@position' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MENU_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_MENU_EDIT_MANAGE);
        Route::put(         'menu/{id}',                [ 'as' => 'menu.update',                'uses' => 'Admin\MenuController@update' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MENU_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_MENU_EDIT_MANAGE);
        Route::delete(      'menu/{id}',                [ 'as' => 'menu.destroy',               'uses' => 'Admin\MenuController@destroy' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MENU_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_MENU_DELETE_MANAGE);
        Route::delete(      'menu/multiple/destroy',    [ 'as' => 'menu.multiple.destroy',      'uses' => 'Admin\MenuController@destroyMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MENU_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_MENU_DELETE_MANAGE);
        Route::post(        'menu/multiple/activate',   [ 'as' => 'menu.multiple.activate',     'uses' => 'Admin\MenuController@activateMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MENU_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_MENU_EDIT_MANAGE);
        Route::post(        'menu/multiple/deactivate', [ 'as' => 'menu.multiple.deactivate',   'uses' => 'Admin\MenuController@deactivateMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_MENU_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_MENU_EDIT_MANAGE);

		Route::get(         'slider',                     [ 'as' => 'slider.index',                 'uses' => 'Admin\SliderController@index' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_SLIDER_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_SLIDER_ONLY_MANAGE);
        Route::get(         'slider/all',                 [ 'as' => 'slider.all',                   'uses' => 'Admin\SliderController@all' ]);
        Route::get(         'slider/{id}',                [ 'as' => 'slider.show',                  'uses' => 'Admin\SliderController@show' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_SLIDER_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_SLIDER_EDIT_MANAGE);
        Route::post(        'slider',                     [ 'as' => 'slider.store',                 'uses' => 'Admin\SliderController@store' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_SLIDER_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_SLIDER_CREATE_MANAGE);
        Route::post(        'slider/position',            [ 'as' => 'slider.position',              'uses' => 'Admin\SliderController@position' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_SLIDER_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_SLIDER_EDIT_MANAGE);
        Route::put(         'slider/{id}',                [ 'as' => 'slider.update',                'uses' => 'Admin\SliderController@update' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_SLIDER_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_SLIDER_EDIT_MANAGE);
        Route::delete(      'slider/{id}',                [ 'as' => 'slider.destroy',               'uses' => 'Admin\SliderController@destroy' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_SLIDER_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_SLIDER_DELETE_MANAGE);
        Route::delete(      'slider/multiple/destroy',    [ 'as' => 'slider.multiple.destroy',      'uses' => 'Admin\SliderController@destroyMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_SLIDER_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_SLIDER_DELETE_MANAGE);
        Route::post(        'slider/multiple/activate',   [ 'as' => 'slider.multiple.activate',     'uses' => 'Admin\SliderController@activateMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_SLIDER_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_SLIDER_EDIT_MANAGE);
        Route::post(        'slider/multiple/deactivate', [ 'as' => 'slider.multiple.deactivate',   'uses' => 'Admin\SliderController@deactivateMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_SLIDER_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_SLIDER_EDIT_MANAGE);
        Route::post(        'slider/upload/image',        [ 'as' => 'slider.upload.image',          'uses' => 'Admin\SliderController@uploadImage' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_SLIDER_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_SLIDER_CREATE_MANAGE);

		Route::get(         'article',                     [ 'as' => 'article.index',                 'uses' => 'Admin\ArticleController@index' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_ARTICLE_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_ARTICLE_ONLY_MANAGE);
        Route::get(         'article/all',                 [ 'as' => 'article.all',                   'uses' => 'Admin\ArticleController@all' ]);
        Route::get(         'article/{id}',                [ 'as' => 'article.show',                  'uses' => 'Admin\ArticleController@show' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_ARTICLE_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_ARTICLE_EDIT_MANAGE);
        Route::post(        'article',                     [ 'as' => 'article.store',                 'uses' => 'Admin\ArticleController@store' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_ARTICLE_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_ARTICLE_CREATE_MANAGE);
        Route::put(         'article/{id}',                [ 'as' => 'article.update',                'uses' => 'Admin\ArticleController@update' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_ARTICLE_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_ARTICLE_EDIT_MANAGE);
        Route::delete(      'article/{id}',                [ 'as' => 'article.destroy',               'uses' => 'Admin\ArticleController@destroy' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_ARTICLE_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_ARTICLE_DELETE_MANAGE);
        Route::delete(      'article/multiple/destroy',    [ 'as' => 'article.multiple.destroy',      'uses' => 'Admin\ArticleController@destroyMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_ARTICLE_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_ARTICLE_DELETE_MANAGE);
        Route::post(        'article/multiple/activate',   [ 'as' => 'article.multiple.activate',     'uses' => 'Admin\ArticleController@activateMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_ARTICLE_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_ARTICLE_EDIT_MANAGE);
        Route::post(        'article/multiple/deactivate', [ 'as' => 'article.multiple.deactivate',   'uses' => 'Admin\ArticleController@deactivateMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_ARTICLE_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_ARTICLE_EDIT_MANAGE);
        Route::post(        'article/upload/image',        [ 'as' => 'article.upload.image',          'uses' => 'Admin\ArticleController@uploadImage' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_ARTICLE_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_ARTICLE_CREATE_MANAGE);
        Route::post(        'article/upload/images',       [ 'as' => 'article.upload.images',         'uses' => 'Admin\ArticleController@uploadImages' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_ARTICLE_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_ARTICLE_CREATE_MANAGE);

        Route::get(         'question',                     [ 'as' => 'question.index',                 'uses' => 'Admin\QuestionController@index' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_QUESTION_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_QUESTION_ONLY_MANAGE);
        Route::get(         'question/all',                 [ 'as' => 'question.all',                   'uses' => 'Admin\QuestionController@all' ]);
        Route::get(         'question/{id}',                [ 'as' => 'question.show',                  'uses' => 'Admin\QuestionController@show' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_QUESTION_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_QUESTION_EDIT_MANAGE);
        Route::post(        'question',                     [ 'as' => 'question.store',                 'uses' => 'Admin\QuestionController@store' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_QUESTION_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_QUESTION_CREATE_MANAGE);
        Route::put(         'question/{id}',                [ 'as' => 'question.update',                'uses' => 'Admin\QuestionController@update' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_QUESTION_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_QUESTION_EDIT_MANAGE);
        Route::delete(      'question/{id}',                [ 'as' => 'question.destroy',               'uses' => 'Admin\QuestionController@destroy' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_QUESTION_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_QUESTION_DELETE_MANAGE);
        Route::delete(      'question/multiple/destroy',    [ 'as' => 'question.multiple.destroy',      'uses' => 'Admin\QuestionController@destroyMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_QUESTION_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_QUESTION_DELETE_MANAGE);
        Route::post(        'question/multiple/activate',   [ 'as' => 'question.multiple.activate',     'uses' => 'Admin\QuestionController@activateMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_QUESTION_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_QUESTION_EDIT_MANAGE);
        Route::post(        'question/multiple/deactivate', [ 'as' => 'question.multiple.deactivate',   'uses' => 'Admin\QuestionController@deactivateMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_QUESTION_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_QUESTION_EDIT_MANAGE);
        Route::post(        'question/upload/image',        [ 'as' => 'question.upload.image',          'uses' => 'Admin\QuestionController@uploadImage' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_QUESTION_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_QUESTION_CREATE_MANAGE);
        Route::post(        'question/upload/images',       [ 'as' => 'question.upload.images',         'uses' => 'Admin\QuestionController@uploadImages' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_QUESTION_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_QUESTION_CREATE_MANAGE);

        Route::get(         'category',                     [ 'as' => 'category.list',                  'uses' => 'Admin\CategoryController@index' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CATEGORY_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_CATEGORY_ONLY_MANAGE);
        Route::get(         'category/all',                 [ 'as' => 'category.all',                   'uses' => 'Admin\CategoryController@all' ]);
        Route::get(         'category/all/nested',          [ 'as' => 'category.all.nested',            'uses' => 'Admin\CategoryController@allNested' ]);
        Route::get(         'category/available',           [ 'as' => 'category.available',             'uses' => 'Admin\CategoryController@available' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CATEGORY_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_CATEGORY_EDIT_MANAGE);
        Route::get(         'category/{id}',                [ 'as' => 'category.show',                  'uses' => 'Admin\CategoryController@show' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CATEGORY_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_CATEGORY_EDIT_MANAGE);
        Route::post(        'category',                     [ 'as' => 'category.store',                 'uses' => 'Admin\CategoryController@store' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CATEGORY_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_CATEGORY_CREATE_MANAGE);
        Route::post(        'category/position',            [ 'as' => 'category.position',              'uses' => 'Admin\CategoryController@position' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CATEGORY_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_CATEGORY_EDIT_MANAGE);
        Route::put(         'category/{id}',                [ 'as' => 'category.update',                'uses' => 'Admin\CategoryController@update' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CATEGORY_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_CATEGORY_EDIT_MANAGE);
        Route::delete(      'category/{id}',                [ 'as' => 'category.destroy',               'uses' => 'Admin\CategoryController@destroy' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CATEGORY_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_CATEGORY_DELETE_MANAGE);
        Route::delete(      'category/multiple/destroy',    [ 'as' => 'category.multiple.destroy',      'uses' => 'Admin\CategoryController@destroyMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CATEGORY_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_CATEGORY_DELETE_MANAGE);
        Route::post(        'category/multiple/activate',   [ 'as' => 'category.multiple.activate',     'uses' => 'Admin\CategoryController@activateMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CATEGORY_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_CATEGORY_EDIT_MANAGE);
        Route::post(        'category/multiple/deactivate', [ 'as' => 'category.multiple.deactivate',   'uses' => 'Admin\CategoryController@deactivateMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CATEGORY_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_CATEGORY_EDIT_MANAGE);

        //Scene
        Route::get(         'scene',                     [ 'as' => 'scene.list',                  'uses' => 'Admin\SceneController@index' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_SCENE_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_SCENE_ONLY_MANAGE);
        Route::get(         'scene/all',                 [ 'as' => 'scene.all',                   'uses' => 'Admin\SceneController@all' ]);
        Route::get(         'scene/all/nested',          [ 'as' => 'scene.all.nested',            'uses' => 'Admin\SceneController@allNested' ]);
        Route::get(         'scene/available',           [ 'as' => 'scene.available',             'uses' => 'Admin\SceneController@available' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_SCENE_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_SCENE_EDIT_MANAGE);
        Route::get(         'scene/{id}',                [ 'as' => 'scene.show',                  'uses' => 'Admin\SceneController@show' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_SCENE_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_SCENE_EDIT_MANAGE);
        Route::post(        'scene',                     [ 'as' => 'scene.store',                 'uses' => 'Admin\SceneController@store' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_SCENE_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_SCENE_CREATE_MANAGE);
        Route::post(        'scene/position',            [ 'as' => 'scene.position',              'uses' => 'Admin\SceneController@position' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_SCENE_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_SCENE_EDIT_MANAGE);
        Route::put(         'scene/{id}',                [ 'as' => 'scene.update',                'uses' => 'Admin\SceneController@update' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_SCENE_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_SCENE_EDIT_MANAGE);
        Route::delete(      'scene/{id}',                [ 'as' => 'scene.destroy',               'uses' => 'Admin\SceneController@destroy' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_SCENE_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_SCENE_DELETE_MANAGE);
        Route::delete(      'scene/multiple/destroy',    [ 'as' => 'scene.multiple.destroy',      'uses' => 'Admin\SceneController@destroyMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_SCENE_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_SCENE_DELETE_MANAGE);
        Route::post(        'scene/multiple/activate',   [ 'as' => 'scene.multiple.activate',     'uses' => 'Admin\SceneController@activateMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_SCENE_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_SCENE_EDIT_MANAGE);
        Route::post(        'scene/multiple/deactivate', [ 'as' => 'scene.multiple.deactivate',   'uses' => 'Admin\SceneController@deactivateMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_SCENE_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_SCENE_EDIT_MANAGE);

        //Condition
        Route::get(         'condition',                     [ 'as' => 'condition.list',                  'uses' => 'Admin\ConditionController@index' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONDITION_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_CONDITION_ONLY_MANAGE);
        Route::get(         'condition/all',                 [ 'as' => 'condition.all',                   'uses' => 'Admin\ConditionController@all' ]);
        Route::get(         'condition/all/nested',          [ 'as' => 'condition.all.nested',            'uses' => 'Admin\ConditionController@allNested' ]);
        Route::get(         'condition/available',           [ 'as' => 'condition.available',             'uses' => 'Admin\ConditionController@available' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONDITION_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_CONDITION_EDIT_MANAGE);
        Route::get(         'condition/{id}',                [ 'as' => 'condition.show',                  'uses' => 'Admin\ConditionController@show' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONDITION_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_CONDITION_EDIT_MANAGE);
        Route::post(        'condition',                     [ 'as' => 'condition.store',                 'uses' => 'Admin\ConditionController@store' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONDITION_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_CONDITION_CREATE_MANAGE);
        Route::post(        'condition/position',            [ 'as' => 'condition.position',              'uses' => 'Admin\ConditionController@position' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONDITION_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_CONDITION_EDIT_MANAGE);
        Route::put(         'condition/{id}',                [ 'as' => 'condition.update',                'uses' => 'Admin\ConditionController@update' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONDITION_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_CONDITION_EDIT_MANAGE);
        Route::delete(      'condition/{id}',                [ 'as' => 'condition.destroy',               'uses' => 'Admin\ConditionController@destroy' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONDITION_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_CONDITION_DELETE_MANAGE);
        Route::delete(      'condition/multiple/destroy',    [ 'as' => 'condition.multiple.destroy',      'uses' => 'Admin\ConditionController@destroyMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONDITION_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_CONDITION_DELETE_MANAGE);
        Route::post(        'condition/multiple/activate',   [ 'as' => 'condition.multiple.activate',     'uses' => 'Admin\ConditionController@activateMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONDITION_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_CONDITION_EDIT_MANAGE);
        Route::post(        'condition/multiple/deactivate', [ 'as' => 'condition.multiple.deactivate',   'uses' => 'Admin\ConditionController@deactivateMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONDITION_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_CONDITION_EDIT_MANAGE);

        //Payment
        Route::get(         'payment',                        [ 'as' => 'payment.list',                     'uses' => 'Admin\PaymentController@index' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PAYMENT_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_PAYMENT_ONLY_MANAGE);
        Route::get(         'payment/all',                    [ 'as' => 'payment.all',                      'uses' => 'Admin\PaymentController@all' ]);
        Route::get(         'payment/available',              [ 'as' => 'payment.available',                'uses' => 'Admin\PaymentController@available' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PAYMENT_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_PAYMENT_EDIT_MANAGE);
        Route::get(         'payment/{id}',                   [ 'as' => 'payment.show',                     'uses' => 'Admin\PaymentController@show' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PAYMENT_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_PAYMENT_EDIT_MANAGE);
        Route::post(        'payment',                        [ 'as' => 'payment.store',                    'uses' => 'Admin\PaymentController@store' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PAYMENT_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_PAYMENT_CREATE_MANAGE);
        Route::put(         'payment/{id}',                   [ 'as' => 'payment.update',                   'uses' => 'Admin\PaymentController@update' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PAYMENT_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_PAYMENT_EDIT_MANAGE);
        Route::delete(      'payment/{id}',                   [ 'as' => 'payment.destroy',                  'uses' => 'Admin\PaymentController@destroy' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PAYMENT_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_PAYMENT_DELETE_MANAGE);
        Route::delete(      'payment/multiple/destroy',       [ 'as' => 'payment.multiple.destroy',         'uses' => 'Admin\PaymentController@destroyMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PAYMENT_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_PAYMENT_DELETE_MANAGE);
        Route::post(        'payment/multiple/activate',      [ 'as' => 'payment.multiple.activate',        'uses' => 'Admin\PaymentController@activateMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PAYMENT_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_PAYMENT_EDIT_MANAGE);
        Route::post(        'payment/multiple/deactivate',    [ 'as' => 'payment.multiple.deactivate',      'uses' => 'Admin\PaymentController@deactivateMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PAYMENT_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_PAYMENT_EDIT_MANAGE);

        //Order
        Route::get(         'order',                        [ 'as' => 'order.list',                     'uses' => 'Admin\OrderController@index' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_ORDER_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_ORDER_ONLY_MANAGE);
        Route::get(         'order/all',                    [ 'as' => 'order.all',                      'uses' => 'Admin\OrderController@all' ]);
        Route::get(         'order/available',              [ 'as' => 'order.available',                'uses' => 'Admin\OrderController@available' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_ORDER_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_ORDER_EDIT_MANAGE);
        Route::get(         'order/{id}',                   [ 'as' => 'order.show',                     'uses' => 'Admin\OrderController@show' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_ORDER_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_ORDER_EDIT_MANAGE);
        Route::post(        'order',                        [ 'as' => 'order.store',                    'uses' => 'Admin\OrderController@store' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_ORDER_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_ORDER_CREATE_MANAGE);
        Route::put(         'order/{id}',                   [ 'as' => 'order.update',                   'uses' => 'Admin\OrderController@update' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_ORDER_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_ORDER_EDIT_MANAGE);
        Route::delete(      'order/{id}',                   [ 'as' => 'order.destroy',                  'uses' => 'Admin\OrderController@destroy' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_ORDER_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_ORDER_DELETE_MANAGE);
        Route::delete(      'order/multiple/destroy',       [ 'as' => 'order.multiple.destroy',         'uses' => 'Admin\OrderController@destroyMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_ORDER_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_ORDER_DELETE_MANAGE);
        Route::post(        'order/multiple/activate',      [ 'as' => 'order.multiple.activate',        'uses' => 'Admin\OrderController@activateMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_ORDER_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_ORDER_EDIT_MANAGE);
        Route::post(        'order/multiple/deactivate',    [ 'as' => 'order.multiple.deactivate',      'uses' => 'Admin\OrderController@deactivateMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_ORDER_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_ORDER_EDIT_MANAGE);

        //Product
        Route::get(         'product',                     [ 'as' => 'product.list',                  'uses' => 'Admin\ProductController@index' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PRODUCT_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_PRODUCT_ONLY_MANAGE);
        Route::get(         'product/all',                 [ 'as' => 'product.all',                   'uses' => 'Admin\ProductController@all' ]);
        Route::get(         'product/available',           [ 'as' => 'product.available',             'uses' => 'Admin\ProductController@available' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PRODUCT_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_PRODUCT_EDIT_MANAGE);
        Route::get(         'product/{id}',                [ 'as' => 'product.show',                  'uses' => 'Admin\ProductController@show' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PRODUCT_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_PRODUCT_EDIT_MANAGE);
        Route::get(         'product/detail/{id}',         [ 'as' => 'product.show.detail',           'uses' => 'Admin\ProductController@showDetail' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PRODUCT_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_PRODUCT_EDIT_MANAGE);
        Route::get(         'product/rule/{id}',           [ 'as' => 'product.show.rule',             'uses' => 'Admin\ProductController@showRule' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PRODUCT_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_PRODUCT_EDIT_MANAGE);
        Route::get(         'product/option/{id}',         [ 'as' => 'product.show.option',           'uses' => 'Admin\ProductController@showOption' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PRODUCT_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_PRODUCT_EDIT_MANAGE);
        Route::get(         'product/options/template',    [ 'as' => 'product.options.template',      'uses' => 'Admin\ProductController@optionsTemplate' ]);
        Route::post(        'product',                     [ 'as' => 'product.store',                 'uses' => 'Admin\ProductController@store' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PRODUCT_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_PRODUCT_CREATE_MANAGE);
        Route::post(        'product/rule/store',          [ 'as' => 'product.store.rule',            'uses' => 'Admin\ProductController@storeRule' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PRODUCT_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_PRODUCT_CREATE_MANAGE);
        Route::put(         'product/{id}',                [ 'as' => 'product.update',                'uses' => 'Admin\ProductController@update' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PRODUCT_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_PRODUCT_EDIT_MANAGE);
        Route::delete(      'product/{id}',                [ 'as' => 'product.destroy',               'uses' => 'Admin\ProductController@destroy' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PRODUCT_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_PRODUCT_DELETE_MANAGE);
        Route::delete(      'product/multiple/destroy',    [ 'as' => 'product.multiple.destroy',      'uses' => 'Admin\ProductController@destroyMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PRODUCT_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_PRODUCT_DELETE_MANAGE);
        Route::post(        'product/sku/{sku}',           [ 'as' => 'product.check.sku',             'uses' => 'Admin\ProductController@checkSku' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PRODUCT_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_PRODUCT_CREATE_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_PRODUCT_EDIT_MANAGE);
        Route::post(        'product/multiple/activate',   [ 'as' => 'product.multiple.activate',     'uses' => 'Admin\ProductController@activateMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PRODUCT_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_PRODUCT_EDIT_MANAGE);
        Route::post(        'product/multiple/deactivate', [ 'as' => 'product.multiple.deactivate',   'uses' => 'Admin\ProductController@deactivateMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PRODUCT_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_PRODUCT_EDIT_MANAGE);
        Route::delete(      'product/options/template',    [ 'as' => 'product.options.delete',        'uses' => 'Admin\ProductController@optionsTemplateDestroy' ]);

        //Block IP Address
        Route::get(         'block-ip',                     [ 'as' => 'block.ip.list',                  'uses' => 'Admin\BlockIpController@index' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PERMISSION_MANAGE);
        Route::get(         'block-ip/available',           [ 'as' => 'block.ip.available',             'uses' => 'Admin\BlockIpController@available' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PERMISSION_MANAGE);
        Route::get(         'block-ip/{id}',                [ 'as' => 'block.ip.show',                  'uses' => 'Admin\BlockIpController@show' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PERMISSION_MANAGE);
        Route::post(        'block-ip',                     [ 'as' => 'block.ip.store',                 'uses' => 'Admin\BlockIpController@store' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PERMISSION_MANAGE);
        Route::put(         'block-ip/{id}',                [ 'as' => 'block.ip.update',                'uses' => 'Admin\BlockIpController@update' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PERMISSION_MANAGE);
        Route::delete(      'block-ip/{id}',                [ 'as' => 'block.ip.destroy',               'uses' => 'Admin\BlockIpController@destroy' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PERMISSION_MANAGE);
        Route::delete(      'block-ip/multiple/destroy',    [ 'as' => 'block.ip.multiple.destroy',      'uses' => 'Admin\BlockIpController@destroyMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PERMISSION_MANAGE);
        Route::post(        'block-ip/multiple/activate',   [ 'as' => 'block.ip.multiple.activate',     'uses' => 'Admin\BlockIpController@activateMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PERMISSION_MANAGE);
        Route::post(        'block-ip/multiple/deactivate', [ 'as' => 'block.ip.multiple.deactivate',   'uses' => 'Admin\BlockIpController@deactivateMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_PERMISSION_MANAGE);

        //Contact
		Route::get(         'contact',                     [ 'as' => 'contact.index',                 'uses' => 'Admin\ContactController@index' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONTACT_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_CONTACT_ONLY_MANAGE);
        Route::get(         'contact/all',                 [ 'as' => 'contact.all',                   'uses' => 'Admin\ContactController@all' ]);
        Route::get(         'contact/{id}',                [ 'as' => 'contact.show',                  'uses' => 'Admin\ContactController@show' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONTACT_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_CONTACT_EDIT_MANAGE);
        Route::get(         'contact/read/{id}',           [ 'as' => 'contact.read',                  'uses' => 'Admin\ContactController@read' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONTACT_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_CONTACT_EDIT_MANAGE);
        Route::post(        'contact',                     [ 'as' => 'contact.store',                 'uses' => 'Admin\ContactController@store' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONTACT_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_CONTACT_CREATE_MANAGE);
        Route::put(         'contact/{id}',                [ 'as' => 'contact.update',                'uses' => 'Admin\ContactController@update' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONTACT_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_CONTACT_EDIT_MANAGE);
        Route::delete(      'contact/{id}',                [ 'as' => 'contact.destroy',               'uses' => 'Admin\ContactController@destroy' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONTACT_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_CONTACT_DELETE_MANAGE);
        Route::delete(      'contact/multiple/destroy',    [ 'as' => 'contact.multiple.destroy',      'uses' => 'Admin\ContactController@destroyMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONTACT_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_CONTACT_DELETE_MANAGE);
        Route::post(        'contact/multiple/activate',   [ 'as' => 'contact.multiple.activate',     'uses' => 'Admin\ContactController@activateMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONTACT_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_CONTACT_EDIT_MANAGE);
        Route::post(        'contact/multiple/deactivate', [ 'as' => 'contact.multiple.deactivate',   'uses' => 'Admin\ContactController@deactivateMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_CONTACT_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_CONTACT_EDIT_MANAGE);

		Route::get(         'seo',                     [ 'as' => 'seo.index',                 'uses' => 'Admin\SeoController@index' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_SEO_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_SEO_ONLY_MANAGE);
        Route::get(         'seo/all',                 [ 'as' => 'seo.all',                   'uses' => 'Admin\SeoController@all' ]);
        Route::get(         'seo/format',              [ 'as' => 'seo.format',                'uses' => 'Admin\SeoController@seoFormat' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_SEO_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_SEO_EDIT_MANAGE);
        Route::get(         'seo/check',               [ 'as' => 'seo.check',                 'uses' => 'Admin\SeoController@seoCheck' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_SEO_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_SEO_EDIT_MANAGE);
        Route::get(         'seo/{id}',                [ 'as' => 'seo.show',                  'uses' => 'Admin\SeoController@show' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_SEO_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_SEO_EDIT_MANAGE);
        Route::post(        'seo',                     [ 'as' => 'seo.store',                 'uses' => 'Admin\SeoController@store' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_SEO_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_SEO_CREATE_MANAGE);
        Route::put(         'seo/{id}',                [ 'as' => 'seo.update',                'uses' => 'Admin\SeoController@update' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_SEO_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_SEO_EDIT_MANAGE);
        Route::delete(      'seo/{id}',                [ 'as' => 'seo.destroy',               'uses' => 'Admin\SeoController@destroy' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_SEO_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_SEO_DELETE_MANAGE);
        Route::delete(      'seo/multiple/destroy',    [ 'as' => 'seo.multiple.destroy',      'uses' => 'Admin\SeoController@destroyMultiple' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_SEO_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_SEO_DELETE_MANAGE);

        Route::get(         'media',                     [ 'as' => 'media.index',                 'uses' => 'Admin\MediaController@index' ]);
        Route::get(         'media/{id}',                [ 'as' => 'media.show',                  'uses' => 'Admin\MediaController@show' ]);
        Route::post(        'media',                     [ 'as' => 'media.store',                 'uses' => 'Admin\MediaController@store' ]);
        Route::put(         'media/{id}',                [ 'as' => 'media.update',                'uses' => 'Admin\MediaController@update' ]);
        Route::delete(      'media/{id}',                [ 'as' => 'media.destroy',               'uses' => 'Admin\MediaController@destroy' ]);
        Route::delete(      'media/multiple/destroy',    [ 'as' => 'media.multiple.destroy',      'uses' => 'Admin\MediaController@destroyMultiple' ]);
        Route::post(        'media/upload/file',         [ 'as' => 'media.upload.file',           'uses' => 'Admin\MediaController@uploadFile' ]);

        Route::get(         'notify/all',               [ 'as' => 'notify.all',                    'uses' => 'Admin\NotifyController@all' ]);
        Route::post(        'notify',                   [ 'as' => 'notify.update',                 'uses' => 'Admin\NotifyController@update' ]);
        Route::delete(      'notify',                   [ 'as' => 'notify.destroy',                'uses' => 'Admin\NotifyController@destroy' ]);
        Route::put(         'notify/{id}',              [ 'as' => 'notify.mark',                   'uses' => 'Admin\NotifyController@mark' ]);

        Route::post(        'gutenberg',                [ 'as' => 'gutenberg.convert',             'uses' => 'Admin\GutenbergController@rawContent' ]);

        Route::get(         'setting',                  [ 'as' => 'setting.index',                 'uses' => 'Admin\SettingController@index' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_SETTING_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_SETTING_ONLY_MANAGE);
        Route::get(         'setting/all',              [ 'as' => 'setting.all',                   'uses' => 'Admin\SettingController@all' ]);
        Route::post(        'setting/multiple/update',  [ 'as' => 'setting.multiple.update',       'uses' => 'Admin\SettingController@update' ])->middleware('permission:' . \App\Laravue\Acl::PERMISSION_SETTING_MANAGE . '|' . \App\Laravue\Acl::PERMISSION_SETTING_EDIT_MANAGE);
    });
});
