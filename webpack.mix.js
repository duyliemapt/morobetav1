let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

if (process.env.section && ['site', 'admin'].includes(process.env.section)) {
    require(`${__dirname}/webpack.${process.env.section}.mix.js`);
} else {
    console.log('\x1b[41m%s\x1b[0m', 'Provide correct --section argument to build command: site or admin');
    throw new Error('Provide correct --section argument to build command!');
}
