<?php

namespace Database\Seeders;

use App\Laravue\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Laravue\Models\Role;
use App\Laravue\Models\Permission;
use App\Fortuneteller;
use App\RuleCustom;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /*$admin = User::create([
            'name' => 'Admin',
            'email' => 'admin@laravue.dev',
            'password' => Hash::make('laravue'),
        ]);
        $manager = User::create([
            'name' => 'Manager',
            'email' => 'manager@laravue.dev',
            'password' => Hash::make('laravue'),
        ]);
        $editor = User::create([
            'name' => 'Editor',
            'email' => 'editor@laravue.dev',
            'password' => Hash::make('laravue'),
        ]);
        $user = User::create([
            'name' => 'User',
            'email' => 'user@laravue.dev',
            'password' => Hash::make('laravue'),
        ]);
        $visitor = User::create([
            'name' => 'Visitor',
            'email' => 'visitor@laravue.dev',
            'password' => Hash::make('laravue'),
        ]);

        $adminRole = Role::findByName(\App\Laravue\Acl::ROLE_ADMIN);
        $managerRole = Role::findByName(\App\Laravue\Acl::ROLE_MANAGER);
        $editorRole = Role::findByName(\App\Laravue\Acl::ROLE_EDITOR);
        $userRole = Role::findByName(\App\Laravue\Acl::ROLE_USER);
        $visitorRole = Role::findByName(\App\Laravue\Acl::ROLE_VISITOR);
        $admin->syncRoles($adminRole);
        $manager->syncRoles($managerRole);
        $editor->syncRoles($editorRole);
        $user->syncRoles($userRole);
        $visitor->syncRoles($visitorRole);
        $this->call(UsersTableSeeder::class);*/

        //$this->call(CreateFakeItem::class);
        //$this->call(ImportRefName::class);
        //$this->call(GenerateFortuneThumb::class);
        //$this->call(UpdateLeftMenu::class);
        //$this->call(ImportArea::class);
        //$this->call(ImportSEO::class);
        //$this->call(ImportTellerSEO::class);
        //$this->call(GenFortuneThumb::class);
        /*$this->call(UpdateAreaSlug::class);
        $this->call(UpdateMethodSlug::class);
        $this->call(UpdateTellerSlug::class);*/
        /*$this->call(UpdateSeoFormat::class);*/

        /*$all = Fortuneteller::all();
        foreach ($all as $item) {
            $item->update(['is_deleted' => 1]);
        }*/

        /*$permissions = Permission::where('name', 'like', '%-shop%')->get();
        $list = ['store1', 'store2', 'store3', 'store4', 'store5'];
        foreach ($list as $item) {
            $role = Role::create(['name' => $item, 'guard_name' => 'api', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
            foreach ($permissions as $p) {
                DB::table('role_has_permissions')->insert(['permission_id' => $p->id, 'role_id' => $role->id]);
                if ($p->name == "manage-shop-custom") {
                    RuleCustom::create([
                        'role_id' => $role->id,
                        'permission_id' => $p->id,
                        'condition_name' => 'id',
                        'condition_compare' => 'is',
                        'condition_value' => '30',
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
                }
            }
        }*/

        //$this->call(CreateFeatureItem::class);
        //$this->call(ChangeArticleImage::class);
        //$this->call(UpdateSeoFormat::class);

        //$this->call(ChangeCategorySeeder::class);

        //$this->call(SceneImport::class);
        //$this->call(ConditionImport::class);
    }
}
