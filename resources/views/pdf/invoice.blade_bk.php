<!DOCTYPE html>
<html lang="jp">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="author" content="Hunters co.">
    <meta name="keywords" content="SBD">
    <title>Product PDF</title>

    <style>
        *{
            box-sizing: border-box;
        }

        html, body {
            margin: 0;
            padding: 0;
        }

        body {
            font-family: 'ipaexg';
        }

        img {
            width: auto;
            height: auto;
            max-width: 100%;
            max-height: 100%;
        }

        .page-break {
            page-break-after: always;
        }

        .clear {
            clear: both;
        }
    </style>
</head>
<body>
<div class="wrapper">
    <div class="page-item">
        <div class="table-pdf">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td rowspan="3">SK-II</td>
                    <td rowspan="3">GMS</td>
                    <td rowspan="3">---</td>
                    <td class="sub-title">店コード</td>
                    <td rowspan="2" class="b-title">イオン防府店 ／ AEON Hofu</td>
                </tr>
                <tr>
                    <td class="top-border medium-text">2000260952</td>
                </tr>
                <tr>
                    <td class="top-border" colspan="2">MAMJ22（3.20〜）<span>GMS</span></td>
                </tr>
            </table>
        </div>
        <div class="product-item">
            <h4 class="title"><span class="dot" >&nbsp;</span>コルトン展開優先順位</h4>
            <div class="p-content">
                <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td>
                            <div class="item-boxes">
                                <h5>No.1</h5>
                                <div class="img-wrap">
                                    <img src="{{ $img1 }}">
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="item-boxes">
                                <h5>No.2</h5>
                                <div class="img-wrap">
                                    <img src="{{ $img2 }}">
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="item-boxes">
                                <h5>No.3</h5>
                                <div class="img-wrap">
                                    <img src="{{ $img3 }}">
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="item-boxes">
                                <h5>No.4</h5>
                                <div class="img-wrap">
                                    <img src="{{ $img4 }}">
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="product-table">
            <div class="table-sub">
                <p class="att">※No.1より優先順位の高い場所に設置してください。</p>
                <table cellpadding="0" cellspacing="0" >
                    <tr>
                        <th>コルトン枚数</th>
                        <td class="lbl-top">No.1</td>
                        <td class="lbl-top">No.2</td>
                        <td class="lbl-top">No.3</td>
                        <td class="lbl-top">No.4</td>
                    </tr>
                    <tr>
                        <th><span class="lbl-num">4</span>枚以上</th>
                        <td>◯</td>
                        <td>◯</td>
                        <td>◯</td>
                        <td>◯</td>
                    </tr>
                    <tr>
                        <th><span class="lbl-num">3</span>枚</th>
                        <td>◯</td>
                        <td>◯</td>
                        <td>◯</td>
                        <td class="table-null"></td>
                    </tr>
                    <tr>
                        <th><span class="lbl-num">2</span>枚</th>
                        <td>◯</td>
                        <td>◯</td>
                        <td class="table-null"></td>
                        <td class="table-null"></td>
                    </tr>
                    <tr>
                        <th><span class="lbl-num">1</span>枚</th>
                        <td>◯</td>
                        <td class="table-null"></td>
                        <td class="table-null"></td>
                        <td class="table-null"></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="page-break"></div>

<div class="wrapper">
    <div class="page-item">
        <div class="table-pdf">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td rowspan="3">SK-II</td>
                    <td rowspan="3">GMS</td>
                    <td rowspan="3">---</td>
                    <td class="sub-title">店コード</td>
                    <td rowspan="2" class="b-title">イオン防府店 ／ AEON Hofu</td>
                </tr>
                <tr>
                    <td class="top-border medium-text">2000260952</td>
                </tr>
                <tr>
                    <td class="top-border" colspan="2">MAMJ22（3.20〜）<span>GMS</span></td>
                </tr>
            </table>
        </div>
        <div class="product-detail">
            <table>
                <tr>
                    <td class="left">
                        <div class="title-box">売場平面図</div>
                        <div class="image">
                            <img src="{{ $img5 }}" alt="">
                        </div>
                    </td>
                    <td class="right">
                        <table>
                            <tr>
                                <td>
                                    <h4 class="title"><span class="dot" >&nbsp;</span>セフォラ ゴンドラ（表面1800mm）</h4>
                                    <div class="img">
                                        <img src="{{ $img6 }}" alt="">
                                    </div>
                                </td>
                                <td>
                                    <h4 class="title"><span class="dot" >&nbsp;</span>セフォラ ゴンドラ
                                        （表面1800mm）</h4>
                                    <div class="img">
                                        <img src="{{ $img7 }}" alt="">
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table>
                            <tr>
                                <td>
                                    <h4 class="title"><span class="dot" >&nbsp;</span>ゴンドラ ディスプレイセット（表面展開 1800mm用）</h4>
                                    <div class="img">
                                        <img src="{{ $img8 }}" alt="">
                                    </div>
                                    <div class="item">
                                        <h4 class="title"><span class="dot" >&nbsp;</span>ゴンドラ ベストセラー・トライアルセット（1800mm）
                                        </h4>
                                        <div class="img">
                                            <img src="{{ $img10 }}" alt="">
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <h4 class="title"><span class="dot" >&nbsp;</span>エンド ディスプレイユニット</h4>
                                    <div class="img">
                                        <img src="{{ $img9 }}" alt="">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<div class="page-break"></div>
<div class="wrapper">
    <div class="page-item">
        <div class="table-pdf">
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td rowspan="3">SK-II</td>
                    <td rowspan="3">GMS</td>
                    <td rowspan="3">---</td>
                    <td class="sub-title">店コード</td>
                    <td rowspan="2" class="b-title">イオン防府店 ／ AEON Hofu</td>
                </tr>
                <tr>
                    <td class="top-border medium-text">2000260952</td>
                </tr>
                <tr>
                    <td class="top-border" colspan="2">MAMJ22（3.20〜）<span>GMS</span></td>
                </tr>
            </table>
        </div>
        <div class="product-item_detail">
            <table>
                <tr>
                    <td class="left">
                        <h4 class="title"><span class="dot" >&nbsp;</span>プロモーション ディスプレイセット<br/>（フルセット）</h4>
                        <div class="image">
                            <img src="{{ $img11 }}" alt="">
                        </div>
                    </td>
                    <td class="right">
                        <table>
                            <tr>
                                <td>
                                    <h4 class="title"><span class="dot" >&nbsp;</span>エクストラ スペース セット（W900mm用）</h4>
                                    <div class="image">
                                        <img src="{{ $img12 }}" alt="">
                                    </div>
                                </td>
                                <td>
                                    <div class="item">
                                        <h4 class="title"><span class="dot" >&nbsp;</span>+1什器</h4>
                                        <div class="image">
                                            <img src="{{ $img13 }}" alt="">
                                        </div>
                                    </div>
                                    <div class="item">
                                        <h4 class="title"><span class="dot" >&nbsp;</span>バックシェルフ ディスプレイ</h4>
                                        <div class="image">
                                            <img src="{{ $img14 }}" alt="">
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td>
                        <h4 class="title"><span class="dot" >&nbsp;</span>プロモーション ディスプレイセット（フルセット）</h4>
                        <div class="image">
                            <img src="{{ $img15 }}" alt="">
                        </div>
                    </td>
                    <td>
                        <p class="text">
                            FTE…フェイシャル トリートメント エッセンス PES…ピテラ エッセンス セット PAK…ピテラ オーラ キット PPK…ピテラ パワー キット ACC…アトモスフィア CC クリーム
                            ALUV50…アトモスフィア エアリー ライト UV クリーム GUE…ジェノプティクス ウルトオーラ エッセンス GAE…ジェノプティクス オーラ エッセンス GSE…ジェノプティクス スポット
                            エッセンス SPA…スキンパワー エアリー SPC…スキンパワー クリーム SPE…スキンパワー エッセンス SPEC…スキンパワー アイ クリーム GUEC…ジェノプティクス アンダー アイ
                            サークル FEK…ピテラ ファースト エクスペリエンス キット
                        </p>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
</body>
</html>
