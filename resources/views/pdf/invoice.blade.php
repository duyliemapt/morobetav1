<!DOCTYPE html>
<html lang="jp">
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="author" content="{{ config('app.name') }}">
    <meta name="keywords" content="{{ $product->name }}">
    @if($purchase == 2)
        <title>[工事まで希望]{{ $currentDate }}の製品{{ $product->name }}の注文書</title>
    @else
        <title>[製品のみ購入]{{ $currentDate }}の製品{{ $product->name }}の注文書</title>
    @endif
    <style>
        * {
            box-sizing: border-box;
        }

        html, body {
            margin: 0;
            padding: 0;
        }

        body {
            font-family: 'ipaexg';
            color: #3c3b3b;
        }

        img {
            width: auto;
            height: auto;
            max-width: 100%;
            max-height: 100%;
        }

        .page-break {
            page-break-after: always;
        }

        .clear {
            clear: both;
        }

        .wrap {
            display: block;
            width: 85%;
            margin: auto;
            height: auto;
            padding-top: 20px;
        }

        .wrap .header {
            height: 46px;
            border-bottom: solid 2px #000;
        }

        .wrap .header span {
            float: left;
            line-height: 45px;
            font-size: 38px;
        }

        .wrap .header span.h_date {
            float: right;
            font-size: 15px;
            line-height: 1.2;
            padding-top: 27px;
        }

        .wrap .top {
            display: block;
            width: 100%;
            padding-top: 6px;
        }

        .wrap .top .b_right,
        .wrap .top .b_left {
            display: inline-block;
            width: 48%;
            float: left;
        }

        .wrap .top .b_left div.img {
            display: block;
            margin: 0 auto 10px auto;
            padding: 0;
            text-align: center;
            overflow-x: hidden;
        }

        .wrap .top .b_left div.img img {
            width: auto;
            height: auto;
            max-height: 220px;
            max-width: 100%;
        }

        .wrap .top .b_right {
            float: right;
        }

        .tbl_item {
            width: 100%;
            border: solid 1px #000;
            border-bottom: solid 0px #fff;
            margin-bottom: 8px;
            border-collapse: collapse;
        }

        .tbl_item tr td,
        .tbl_item tr th {
            font-weight: normal;
            text-align: center;
            border: 1px dotted #000;
            vertical-align: middle;
            padding: 1px 10px;
            font-size: 10px;
        }

        .tbl_item tr th {
            width: 45%;
            background-color: #eeeeee;
        }

        .tbl_item tr td {
            text-align: left;
        }

        .tbl_bottom {
            border: solid 1px #FFF;
            border-bottom: solid 1px #000;
            border-spacing: 0;
        }

        .tbl_bottom tr th {
            width: 20%;
            background-color: #fff;
            padding-left: 0;
            text-align: left;
            vertical-align: top;
        }

        .tbl_bottom tr th.tbl_header {
            font-size: 14px;
            font-weight: bold;
            color: #000;
        }

        .tbl_bottom tr th,
        .tbl_bottom tr td {
            border: none;
            border-left: solid 0 #fff;
            border-right: solid 0 #fff;
            border-top: dotted 1px #000;
        }

        .tbl_bottom tr td {
            padding-bottom: 15px;
        }

        .tbl_price {
            border: solid 0 #fff;
            border-bottom: solid 1px #000;
        }

        .tbl_price tr th,
        .tbl_price tr td {
            border: solid 0 #fff;
            background-color: #fff;
            border-top: dotted 1px #cfcfcf;
            padding: 5px 0 5px 5px;
            text-align: left;
        }

        .tbl_price tr td {
            text-align: right;
            padding-right: 20px;
        }

        .tbl_price tr th.tbl_header {
            font-size: 13px;
            border-top: solid 0 #fff;
            background-color: #000;
            color: #fff;
            font-weight: bold;
        }

        .tbl_price tr.no_border th,
        .tbl_price tr.no_border td {
            border-top: solid 0 #fff;
        }

        .tbl_price tr.with_color th,
        .tbl_price tr.with_color td {
            color: red;
        }

        .tbl_price tr.total th,
        .tbl_price tr.total td {
            border-top: solid 1px #000;
            background-color: #dcdcdc;
            font-weight: bold;
            color: red;
        }

        .info {
            display: block;
            width: 100%;
            max-width: 700px;
            margin-bottom: 5px;
            overflow-x: hidden;
        }

        .info h3 {
            font-weight: bold;
            color: red;
            font-size: 10px;
            line-height: 1.2;
            margin: 0;
            padding: 0;
            border-bottom: solid 1px #000;
        }

        .info p {
            margin: 0;
            padding: 0;
            font-size: 10px;
            font-weight: normal;
        }

        .info p.bottom_line {
            line-height: 15px;
            margin-bottom: 2px;
            border-bottom: dotted 2px #000;
        }

        .footer {
            display: block;
            width: 100%;
        }

        .footer table {
            width: 100%;
            border: solid 1px #000;
            border-collapse: collapse;
        }

        .footer table tr td {
            font-size: 10px;
            border-bottom: dotted 1px #959595;
            border-right: dotted 1px #959595;
            padding: 5px;
            vertical-align: middle;
            text-align: center;
        }

        .footer table tr.tbl_header td {
            border-bottom: solid 1px #000;
            border-right: dotted 1px #fff;
            text-align: left;
            vertical-align: middle;
            padding: 0 2px;
        }

        .footer table tr.tbl_header td.row_control {
            text-align: right;
            padding-top: 2px;
            padding-bottom: 0;
        }

        .footer table tr.tbl_header td.row_control label,
        .footer table tr.tbl_header td.row_control input {
            display: inline-block;
            vertical-align: middle;
        }

        .footer table tr.tbl_header td.row_control input {
            margin-left: 25px;
        }

        .footer table tr.tbl_header td.row_control input {
            margin: 0 0 0 25px;
            padding: 0;
            line-height: 1.2;
        }

        .footer table tr.tbl_header td.row_control label {
            padding-top: 3px;
        }

        .footer table tr td.border_left {
            border-left: solid 1px #000;
        }

        .footer table tr td.first_col {
            height: 20px;
        }

        .footer table tr td.cell_top,
        .footer table tr td.cell_fade {
            font-size: 9px;
        }

        .footer table tr td.cell_fade {
            vertical-align: bottom;
            text-align: left;
            color: #9d9d9d;
        }

        .footer table tr td.cell_top {
            vertical-align: top;
            text-align: left;
        }

        .footer table tr td p {
            margin: 0;
            text-align: left;
        }

        .footer table tr td p span {
            margin-right: 30%;
        }

        .footer table tr td p span:first-child {
            margin-right: 2px;
        }

        .footer table tr td.left_text {
            text-align: left;
        }

        .footer table tr td p input {
            margin: 0;
            padding: 0;
            line-height: 1.2;
        }

        .footer table tr td p input,
        .footer table tr td p label {
            vertical-align: middle;
        }
    </style>
</head>
<body>
<div class="wrap">
    <div class="header">
        <span class="logo">発注書</span>
        <span class="h_date">DATE：{{ $currentDate }}</span>
    </div>
    <div class="clear"></div>
    <div class="top">
        <div class="b_left">
            <div class="img">
                <img src="{{ $imgPath }}/{{ $product->image }}" alt="{{ $product->name }}"/>
            </div>
            <table class="tbl_item tbl_price">
                <tr>
                    <th class="tbl_header" colspan="2">金額明細</th>
                </tr>
                <tr class="no_border">
                    <th>カタログ価格</th>
                    <td>{{ $originalPrice }}円</td>
                </tr>
                <tr class="with_color">
                    <th>オンライン値引</th>
                    <td>-{{ $discount }}円</td>
                </tr>
                <tr>
                    <th>製品販売価格</th>
                    <td>{{ $price }}円</td>
                </tr>
                @if($purchase == 2)
                <tr>
                    <th>標準施工費</th>
                    <td>{{ $constructionFee }}円</td>
                </tr>
                @endif
                <tr>
                    <th>消費税</th>
                    <td>{{ $tax }}円</td>
                </tr>
                <tr class="with_color">
                    <th>送料(税込)</th>
                    <td>{{ $shippingCost }}円</td>
                </tr>
                <tr class="total">
                    <th>合計</th>
                    <td>{{ $totalAmount }}円</td>
                </tr>
            </table>
            <div class="clear"></div>
        </div>
        <div class="b_right">
            <table class="tbl_item">
                <tr>
                    <th>メーカー</th>
                    <td>LIXIL</td>
                </tr>
                <tr>
                    <th>製品名</th>
                    <td>{{ $product->name }}</td>
                </tr>
                <tr>
                    <th>&nbsp;</th>
                    <td>{{ $product->note }}</td>
                </tr>
                @foreach($options as $item)
                <tr>
                    <th>{{ $item->detail->option->name }}</th>
                    <td>{{ $item->detail->name }}</td>
                </tr>
                @endforeach
            </table>
            <table class="tbl_item tbl_bottom" cellspacing="0" cellpadding="0">
                <tr>
                    <th class="tbl_header" colspan="2">配送について</th>
                </tr>
                <tr>
                    <th>配送エリア</th>
                    <td>
                        茨城県 栃木県 群馬県 埼玉県 千葉
                        県 東京都 神奈川県 京都府 大阪
                        府 奈良県
                        <br>
                        <br>
                        ※一部配送できない地域がございますので予めご
                        了承ください。配送できない場合は、担当からご
                        連絡させて頂きます。
                    </td>
                </tr>
                <tr>
                    <th>最短納期の目安</th>
                    <td>
                        本日ご注文した場合、{{ $possibleDeliveryTime }} ごろ
                        お届けできます。
                    </td>
                </tr>
            </table>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    <div class="info">
        <h3>INFORMATION ご購入の際のご注意</h3>
        <p>
            ①受注生産品のため、ご注文後の返品・変更・キャンセルはお受けできません。ご注文確定後の返品・キャンセル・変更を行う場合は製品販売価格
            と消費税、別途処分費を請求させて頂きます。
        </p>
        <p>
            ②納品後10日以内に検品するようにお願いします。製品にへこみや傷がある場合は、お手数ですがメールにて写真をお送りください。無料にて代替
            品をお送りさせて頂きます。10日以上経過後に製品の不具合や部品の欠品についてご連絡いただいた場合は有償による対応とさせていただきます。
        </p>
        <p>
            ③表示されている最短の納期は目安となります。メーカーの生産状況や物流状況等、諸事情によりお時間を頂く場合がございますので予めご了承く
            ださい。
        </p>
        <p>
            ⑤誤発注がご不安な場合は、決済後の『ご注文の完了』時に『お電話にて最終確認を希望する』または『メールにて最終確認を希望する』をチェッ
            クしてください。
        </p>
        <p>
            ⑥弊社取扱いの製品は、実際に建築現場で扱われているプロ仕様の製品です。組立や設置には専用の工具や知識が必要な場合がございますのでご注
            意ください。
        </p>
        <p>
            ⑦誤発注、納期遅延、施工ができなかったことによる返品・変更はお受けできませんのでご注意ください。
        </p>
        <p class="bottom_line">&nbsp;</p>
    </div>
    <div class="clear"></div>
    <div class="footer">
        <table class="tbl_footer">
            <tr class="tbl_header">
                <td colspan="4">FAXでのお問合せ：048-556-7961</td>
                <td class="row_control" width="42%">
                    <input type="checkbox" id="chb1" name="chb1"/>
                    <label for="chb1">お問い合わせ</label>
                    <input type="checkbox" id="chb2" name="chb2"/>
                    <label for="chb2">購入</label>
                    <input type="checkbox" id="chb3" name="chb3"/>
                    <label for="chb3">その他</label>
                </td>
            </tr>
            <tr>
                <td class="first_col" width="15%">お名前</td>
                <td class="cell_fade">(姓)</td>
                <td class="cell_fade">(名)</td>
                <td class="border_left" rowspan="2" width="15%">お届け先</td>
                <td class="cell_top">〒</td>
            </tr>
            <tr>
                <td class="first_col">ふりがな</td>
                <td class="cell_fade">(姓)</td>
                <td class="cell_fade">(名)</td>
                <td class="cell_top">〒</td>
            </tr>
            <tr>
                <td>会社名</td>
                <td colspan="2">&nbsp;</td>
                <td rowspan="3" class="border_left">希望納期</td>
                <td>
                    <p>
                        <span>①</span>
                        <span>平成</span>
                        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;年</span>
                    </p>
                    <p>
                        <span>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <span>月</span>
                        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;日</span>
                    </p>
                    <p>
                        <span>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <span>午前・午後</span>
                        <span>時頃</span>
                    </p>
                </td>
            </tr>
            <tr>
                <td>TEL</td>
                <td colspan="2">&nbsp;</td>
                <td>
                    <p>
                        <span>②</span>
                        <span>平成</span>
                        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;年</span>
                    </p>
                    <p>
                        <span>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <span>月</span>
                        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;日</span>
                    </p>
                    <p>
                        <span>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <span>午前・午後</span>
                        <span>時頃</span>
                    </p>
                </td>
            </tr>
            <tr>
                <td>FAX</td>
                <td colspan="2">&nbsp;</td>
                <td class="left_text">
                    ※配送方法で『配送日の指定無し』の場合は弊社から候
                    補日をご連絡させて頂きます。
                </td>
            </tr>
            <tr>
                <td>MAIL</td>
                <td colspan="2">&nbsp;</td>
                <td rowspan="3" class="border_left">決済方法</td>
                <td class="left_text" rowspan="2">
                    <p>
                        <input type="checkbox" id="chb4" name="chb4"/>
                        <label for="chb4">前払い:入金確認後に発送準備</label>
                    </p>
                    <p>※オプション品は別途御見積させて頂きます。</p>
                </td>
            </tr>
            <tr>
                <td rowspan="2">住所</td>
                <td colspan="2" class="cell_top">〒</td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
                <td class="left_text">
                    <p>【お振込先】</p>
                    <p>三井住友銀行　熊谷支店　普通　4294366</p>
                </td>
            </tr>
            <tr>
                <td class="first_col">質問事項等</td>
                <td colspan="4">&nbsp;</td>
            </tr>
        </table>
    </div>
    <div class="clear"></div>
</div>
{{--<div class="page-break"></div>--}}
</body>
</html>
