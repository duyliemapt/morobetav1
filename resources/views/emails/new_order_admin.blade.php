<h3>※このメールはシステムからの自動返信です</h3><br/>
[JISHODO]の新しい注文があります。<br>
<br>
今回、新規登録されたお客様から掲示板に質問投稿がありましたら、相談員の方はご回答をお願いします。<br>
<br/>
新規ご注文内容
<br/>
<br/>
<table style="border-collapse: collapse; width: 100%;">
    <tbody>
        <tr style="background-color: #797979;">
            <td colspan="2" style="text-align: left; color: #fff;">
                <p style="padding-left: 20px;">注文</p>
            </td>
        </tr>
        <tr style="background-color: #f2f2f2;">
            <th style="border: 1px solid #ddd; padding: 8px; width: 30%;">オーダーコード：</th>
            <td style="border: 1px solid #ddd; padding: 8px;">
                <a href="{{ $site }}/admin/order?c={{ $code }}" target="_blank"><b>#{{ $code }}</b></a>
            </td>
        </tr>
        <tr>
            <th style="border: 1px solid #ddd; padding: 8px; width: 30%;">合計金額：</th>
            <td style="border: 1px solid #ddd; padding: 8px;">{{ $total_amount }}</td>
        </tr>
        <tr style="background-color: #f2f2f2;">
            <th style="border: 1px solid #ddd; padding: 8px; width: 30%;">支払方法：</th>
            <td style="border: 1px solid #ddd; padding: 8px;">{{ $payment_method }}</td>
        </tr>
        <tr>
            <th style="border: 1px solid #ddd; padding: 8px; width: 30%;">支払い状況：</th>
            <td style="border: 1px solid #ddd; padding: 8px;">{{ $payment_status }}</td>
        </tr>
        <tr style="background-color: #f2f2f2;">
            <th style="border: 1px solid #ddd; padding: 8px; width: 30%;">ノート：</th>
            <td style="border: 1px solid #ddd; padding: 8px;">{{ $note }}</td>
        </tr>
        <tr style="background-color: #797979;">
            <td colspan="2" style="text-align: left; color: #fff;">
                <p style="padding-left: 20px;">配送先住所</p>
            </td>
        </tr>
        <tr>
            <th style="border: 1px solid #ddd; padding: 8px; width: 30%;">お名前：</th>
            <td style="border: 1px solid #ddd; padding: 8px;">{{ $name }}</td>
        </tr>
        <tr style="background-color: #f2f2f2;">
            <th style="border: 1px solid #ddd; padding: 8px; width: 30%;">フリガナ：</th>
            <td style="border: 1px solid #ddd; padding: 8px;">{{ $furigana }}</td>
        </tr>
        <tr>
            <th style="border: 1px solid #ddd; padding: 8px; width: 30%;">住所：</th>
            <td style="border: 1px solid #ddd; padding: 8px;">{{ $address }}</td>
        </tr>
        <tr style="background-color: #f2f2f2;">
            <th style="border: 1px solid #ddd; padding: 8px; width: 30%;">電話番号：</th>
            <td style="border: 1px solid #ddd; padding: 8px;">{{ $phone_number }}</td>
        </tr>
        <tr>
            <th style="border: 1px solid #ddd; padding: 8px; width: 30%;">メールアドレス：</th>
            <td style="border: 1px solid #ddd; padding: 8px;">{{ $email }}</td>
        </tr>
    </tbody>
</table>
