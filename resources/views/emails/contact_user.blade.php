<h3>※このメールはシステムからの自動返信です</h3><br/>
{{ $full_name }}様<br/>
<br/>
メールフォームでのお問い合わせありがとうございます。<br/>
<br/>
後日、当社の相談員が、お客様のメールアドレスにご相談の返答を送信します。<br/>
今しばらくお待ちください。<br/>
<br/>
━━━━━━□■□　お問い合わせ内容　□■□━━━━━━<br/>
<table style="border-collapse: collapse; width: 100%;">
    <tbody>
        <tr style="background-color: #f2f2f2;">
            <th style="border: 1px solid #ddd; padding: 8px; width: 30%;">お名前：</th>
            <td style="border: 1px solid #ddd; padding: 8px;">{{ $full_name }}</td>
        </tr>
        <tr>
            <th style="border: 1px solid #ddd; padding: 8px; width: 30%;">メールアドレス：</th>
            <td style="border: 1px solid #ddd; padding: 8px;">{{ $email }}</td>
        </tr>
        <tr style="background-color: #f2f2f2;">
            <th style="border: 1px solid #ddd; padding: 8px; width: 30%;">電話番号：</th>
            <td style="border: 1px solid #ddd; padding: 8px;">{{ $phone_number }}</td>
        </tr>
        <tr>
            <th style="border: 1px solid #ddd; padding: 8px; width: 30%;">お問い合わせ内容：</th>
            <td style="border: 1px solid #ddd; padding: 8px;">{{ $content }}</td>
        </tr>
    </tbody>
</table>
<br>
<br>
----------------------------------------------------<br>
JISHODO<br>
<a href="{{ $site }}" target="_blank">{{ $site }}/</a><br>
【運営会社】株式会社 自笑堂<br>
〒530-0003<br>
埼玉県行田市持田2-1-30<br>
TEL.0944-88-9107<br>
----------------------------------------------------<br>
※このメールは送信専用アドレスです。<br>
ご返信いただいても配信元へは届きませんのでご了承ください。<br>
お問い合せは、以下お問い合わせフォームよりお願いいたします。<br>
お問い合わせメールフォーム<br>
<a href="{{ $site }}/contact/index" target="_blank">{{ $site }}/contact/index</a><br>
----------------------------------------------------<br>
<br>
