<h3>※このメールはシステムからの自動返信です</h3><br/>
{{ $full_name }}様<br/>
<br/>
よりメールフォームにお問い合わせがありました。<br/>
以下の内容でお問い合わせが来ていますので、介護相談のご担当者様はお客様のメールアドレスにご連絡をお願いします。<br/>
<br/>
━━━━━━□■□　お問い合わせ内容　□■□━━━━━━<br/>
<table style="border-collapse: collapse; width: 100%;">
    <tbody>
        <tr style="background-color: #f2f2f2;">
            <th style="border: 1px solid #ddd; padding: 8px; width: 30%;">お名前：</th>
            <td style="border: 1px solid #ddd; padding: 8px;">{{ $full_name }}</td>
        </tr>
        <tr>
            <th style="border: 1px solid #ddd; padding: 8px; width: 30%;">メールアドレス：</th>
            <td style="border: 1px solid #ddd; padding: 8px;">{{ $email }}</td>
        </tr>
        <tr style="background-color: #f2f2f2;">
            <th style="border: 1px solid #ddd; padding: 8px; width: 30%;">電話番号：</th>
            <td style="border: 1px solid #ddd; padding: 8px;">{{ $phone_number }}</td>
        </tr>
        <tr>
            <th style="border: 1px solid #ddd; padding: 8px; width: 30%;">お問い合わせ内容：</th>
            <td style="border: 1px solid #ddd; padding: 8px;">{{ $content }}</td>
        </tr>
    </tbody>
</table>
