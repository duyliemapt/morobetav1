<h3>この度は「KIMONO」を、ご利用いただきまして、誠にありがとうございます。</h3><br/>
<br/>
下記URLよりご利用登録を行なってください。<br/>
<a href="{{ $site }}/register/{{ $code }}" target="_blank" >{{ $site }}/register/{{ $code }}</a><br/>
※メール・SMS（ショートメッセージ）に記載されているURLの有効時間は100分となっております。ご注意ください。<br/>
<br/>
今後ともどうぞ、「KIMONO」をよろしくお願い申し上げます。<br/>
<br/>
────────────────────────────<br/>
　KIMONO<br/>
　<a href="{{ $site }}/" target="_blank" >{{ $site }}/</a><br/>
<br/>
────────────────────────────<br/>
　TEL : 0944-88-9107　　E-Mail : support@kirei-kimono.jp<br/>
────────────────────────────<br/>
　KIMONOは着物のレンタル・通販ショッピングモールサイトです。<br/>
────────────────────────────<br/>
<br/>
<br/>
