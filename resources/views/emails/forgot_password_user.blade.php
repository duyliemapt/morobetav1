<br>
<br>
{{ $full_name }}様<br/>
<br/>
介護なんでも相談室のパスワード再設定メールです。<br/>
下記の認証URLをクリックして、パスワードの変更を完了してください。<br/>
<br/>
<a href="{{ $link }}" target="_blank"><strong>{{ $link }}</strong></a><br/>
<br/>
クリックしてもページに移動しない場合は、認証URLをブラウザの入力欄にコピーしてページ移動してください。<br/>
認証URLの有効時間は15分となっています。<br/>
今後とも、介護なんでも相談室をよろしくお願いいたします。<br/>
<br/>
介護なんでも相談室<br/>
<a href="https://www.kaigo-soudan21.jp/" target="_blank"><strong>https://www.kaigo-soudan21.jp/</strong></a><br/>
<br/>
<br/>
