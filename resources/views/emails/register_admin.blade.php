<h3>※このメールはシステムからの自動返信です</h3><br/>
【介護なんでも相談室】の新規会員登録フォームから、新規会員登録がありました。<br>
<br>
今回、新規登録されたお客様から掲示板に質問投稿がありましたら、相談員の方はご回答をお願いします。<br>
<br/>
新規会員登録内容<br/>
<table style="border-collapse: collapse; width: 100%;">
    <tbody>
    <tr style="background-color: #f2f2f2;">
        <th style="border: 1px solid #ddd; padding: 8px;">お名前：</th>
        <td style="border: 1px solid #ddd; padding: 8px;">{{ $full_name }}</td>
    </tr>
    <tr>
        <th style="border: 1px solid #ddd; padding: 8px; width: 35%;">フリガナ：</th>
        <td style="border: 1px solid #ddd; padding: 8px;">{{ $furigana_name }}</td>
    </tr>
    <tr style="background-color: #f2f2f2;">
        <th style="border: 1px solid #ddd; padding: 8px;">ニックネーム：</th>
        <td style="border: 1px solid #ddd; padding: 8px;">{{ $name }}</td>
    </tr>
    <tr>
        <th style="border: 1px solid #ddd; padding: 8px; width: 35%;">性別：</th>
        <td style="border: 1px solid #ddd; padding: 8px;">{{ $gender }}</td>
    </tr>
    <tr style="background-color: #f2f2f2;">
        <th style="border: 1px solid #ddd; padding: 8px;">年齢：</th>
        <td style="border: 1px solid #ddd; padding: 8px;">{{ $age }}</td>
    </tr>
    <tr>
        <th style="border: 1px solid #ddd; padding: 8px; width: 35%;">住所：</th>
        <td style="border: 1px solid #ddd; padding: 8px;">{{ $post_code }} ー {{ $address }} ー {{ $address2 }}</td>
    </tr>
    <tr style="background-color: #f2f2f2;">
        <th style="border: 1px solid #ddd; padding: 8px;">電話番号：</th>
        <td style="border: 1px solid #ddd; padding: 8px;">{{ $phone_number }}</td>
    </tr>
    <tr>
        <th style="border: 1px solid #ddd; padding: 8px;">ラインID：</th>
        <td style="border: 1px solid #ddd; padding: 8px;">{{ $line_id }}</td>
    </tr>
    <tr style="background-color: #f2f2f2;">
        <th style="border: 1px solid #ddd; padding: 8px; width: 35%;">メールアドレス：</th>
        <td style="border: 1px solid #ddd; padding: 8px;">{{ $email }}</td>
    </tr>
    <tr>
        <th style="border: 1px solid #ddd; padding: 8px;">連絡方法：</th>
        <td style="border: 1px solid #ddd; padding: 8px;">{{ $contact_method }}</td>
    </tr>
    </tbody>
</table>
