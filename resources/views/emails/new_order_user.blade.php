<h3>※このメールはシステムからの自動返信です</h3><br/>
【JISHODO】にご注文いただきありがとうございます。<br>
<br>
ご注文を処理しています。<br>
<br/>
以下、注文情報です:
<br/>
<br/>
<table style="border-collapse: collapse; width: 100%;">
    <tbody>
        <tr style="background-color: #797979;">
            <td colspan="2" style="text-align: left; color: #fff;">
                <p style="padding-left: 20px;">注文</p>
            </td>
        </tr>
        <tr style="background-color: #f2f2f2;">
            <th style="border: 1px solid #ddd; padding: 8px; width: 30%;">オーダーコード：</th>
            <td style="border: 1px solid #ddd; padding: 8px;"><b>#{{ $code }}</b></td>
        </tr>
        <tr>
            <th style="border: 1px solid #ddd; padding: 8px; width: 30%;">合計金額：</th>
            <td style="border: 1px solid #ddd; padding: 8px;">{{ $total_amount }}</td>
        </tr>
        <tr style="background-color: #f2f2f2;">
            <th style="border: 1px solid #ddd; padding: 8px; width: 30%;">支払方法：</th>
            <td style="border: 1px solid #ddd; padding: 8px;">{{ $payment_method }}</td>
        </tr>
        <tr>
            <th style="border: 1px solid #ddd; padding: 8px; width: 30%;">支払い状況：</th>
            <td style="border: 1px solid #ddd; padding: 8px;">{{ $payment_status }}</td>
        </tr>
        <tr style="background-color: #f2f2f2;">
            <th style="border: 1px solid #ddd; padding: 8px; width: 30%;">ノート：</th>
            <td style="border: 1px solid #ddd; padding: 8px;">{{ $note }}</td>
        </tr>
        <tr style="background-color: #797979;">
            <td colspan="2" style="text-align: left; color: #fff;">
                <p style="padding-left: 20px;">配送先住所</p>
            </td>
        </tr>
        <tr>
            <th style="border: 1px solid #ddd; padding: 8px; width: 30%;">お名前：</th>
            <td style="border: 1px solid #ddd; padding: 8px;">{{ $name }}</td>
        </tr>
        <tr style="background-color: #f2f2f2;">
            <th style="border: 1px solid #ddd; padding: 8px; width: 30%;">フリガナ：</th>
            <td style="border: 1px solid #ddd; padding: 8px;">{{ $furigana }}</td>
        </tr>
        <tr>
            <th style="border: 1px solid #ddd; padding: 8px; width: 30%;">住所：</th>
            <td style="border: 1px solid #ddd; padding: 8px;">{{ $address }}</td>
        </tr>
        <tr style="background-color: #f2f2f2;">
            <th style="border: 1px solid #ddd; padding: 8px; width: 30%;">電話番号：</th>
            <td style="border: 1px solid #ddd; padding: 8px;">{{ $phone_number }}</td>
        </tr>
        <tr>
            <th style="border: 1px solid #ddd; padding: 8px; width: 30%;">メールアドレス：</th>
            <td style="border: 1px solid #ddd; padding: 8px;">{{ $email }}</td>
        </tr>
    </tbody>
</table>
