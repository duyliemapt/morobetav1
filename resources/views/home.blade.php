<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" id="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta id="format-detection" name="format-detection" content="telephone=no">
    <meta id="site-name" name="site-name" content="{{ config('settings.site_name') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="Content-Language" content="ja">
    <meta name="google-site-verification" content="" />
    <meta name="google" content="notranslate">

    <title>{{ $title }}</title>

    <meta data-vue-meta="1" data-vmid="keywords" id="keywords" name="keywords" content="{{ $keywords }}">
    <meta data-vue-meta="2" data-vmid="description" id="description" name="description" content="{{ $description }}">
    <meta data-vue-meta="3" data-vmid="image" id="image" name="image" content="{{ $image }}">

    <meta property="og:site_name" content="{{ config('settings.site_name') }}">
    <meta property="og:type" content="website">
    <meta data-vue-meta="4" data-vmid="og_url" id="og_url" property="og:url" content="{{ url()->current() }}">
    <meta data-vue-meta="5" data-vmid="og_title" id="og_title" property="og:title" content="{{ $title }}">
    <meta data-vue-meta="6" data-vmid="og_description" id="og_description" property="og:description" content="{{ $description }}">
    <meta data-vue-meta="7" data-vmid="og_image" id="og_image" property="og:image" content="{{ $image }}">
    <meta data-vue-meta="8" data-vmid="og_fb_app_id" id="og_fb_app_id" property="fb:app_id" content="">

    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="{{ config('settings.site_name') }}">
    <meta name="twitter:creator" content="Hunters">
    <meta data-vue-meta="9" data-vmid="twitter_title" id="twitter_title" name="twitter:title" content="{{ $title }}">
    <meta data-vue-meta="10" data-vmid="twitter_description" id="twitter_description" name="twitter:description" content="{{ $description }}">
    <meta data-vue-meta="11" data-vmid="twitter_image" id="twitter_image" name="twitter:image" content="{{ $image }}">
    <meta data-vue-meta="12" data-vmid="twitter_image_src" id="twitter_image_src" name="twitter:image:src" content="{{ $image }}">
    <meta data-vue-meta="13" data-vmid="twitter_url" id="twitter_url" name="twitter:url" content="{{ url()->current() }}">
    <meta http-equiv="origin-trial" content="AwnOWg2dzaxHPelVjqOT/Y02cSxnG2FkjXO7DlX9VZF0eyD0In8IIJ9fbDFZGXvxNvn6HaF51qFHycDGLOkj1AUAAACAeyJvcmlnaW4iOiJodHRwczovL2NyaXRlby5jb206NDQzIiwiZmVhdHVyZSI6IlByaXZhY3lTYW5kYm94QWRzQVBJcyIsImV4cGlyeSI6MTY5NTE2Nzk5OSwiaXNTdWJkb21haW4iOnRydWUsImlzVGhpcmRQYXJ0eSI6dHJ1ZX0=">

    <!-- icon -->
    <link rel="dns-prefetch" href="//s.w.org">
    <link rel="shortcut icon" href="{{ url('/') }}/favicon.ico">
    <link rel="apple-touch-icon" href="{{ url('/') }}/apple-touch-icon.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">

    <script>
        (function (doc) {
            var ua = navigator.userAgent.toLowerCase();
            if (/ipad/.test(ua) || /android/.test(ua) && !/mobile/.test(ua)) {
                doc.getElementById('viewport').setAttribute('content', 'width=1200');
            } else {
                doc.getElementById('viewport').setAttribute('content', 'width=device-width, initial-scale=1');
            }
        }(document));
    </script>

    <style>
        #loading_first {
            display: block;
            position: relative;
            width: 80px;
            height: 80px;
            margin: 150px auto 50px;
        }
    </style>

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Zen+Old+Mincho:wght@400;500;600;700;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Italianno&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha512-SfTiTlX6kk+qitfevl/7LibUOeJWlt9rbyDn92a1DqWOw9vWG2MFoays0sgObmWazO5BQPiFucnnEAjpAB+/Sw==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
</head>
<body id="body" class="home page-template page-template-page_index page-template-page_index-php page page-id-9325">
    <div id="loading_first">Loading...</div>
    <div id="app">
        <app></app>
    </div>

    <script async src="{{ mix('js/manifest.js') }}" defer></script>
    <script async src="{{ mix('js/vendor.js') }}" defer></script>
    <script async src="{{ mix('js/app.js') }}" defer></script>

    <noscript id="deferred-styles">
        <link rel="stylesheet" href="{{ mix('css/style.css') }}" type="text/css">
    </noscript>

    <script>
        var loadDeferredStyles = function() {
            var addStylesNode = document.getElementById("deferred-styles");
            var replacement = document.createElement("div");
            replacement.innerHTML = addStylesNode.textContent;
            document.body.appendChild(replacement)
            addStylesNode.parentElement.removeChild(addStylesNode);
        };
        var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
            window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
        if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
        else window.addEventListener('load', loadDeferredStyles);
    </script>

    {{--<script src="{{ url('/') }}/js/jquery-3.3.1.min.js"></script>--}}
    {{--<script src="{{ url('/') }}/js/slick.js"></script>--}}
    {{--<script src="{{ url('/') }}/js/script.js"></script>--}}
    {{--<script src="{{ url('/') }}/js/bootstrap.min.js"></script>--}}

    {!! config('settings.google_analytics') !!}
</body>
</html>
