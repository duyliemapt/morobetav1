<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('settings.site_name') }}</title>
    <link rel="shortcut icon" href="{{ url('/') }}/favicon.ico">
    <link rel="apple-touch-icon" href="{{ url('/') }}/favicon.ico">
    <link rel="manifest" href="{{ url('/') }}/backend/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ url('/') }}/backend/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+JP&display=swap" rel="stylesheet">

    {{--<script src="https://unpkg.com/react@16.8.6/umd/react.production.min.js"></script>
    <script src="https://unpkg.com/react-dom@16.8.6/umd/react-dom.production.min.js"></script>
    <script src="{{ asset('vendor/laraberg/js/laraberg.js') }}"></script>
    <link href="{{ asset('vendor/laraberg/css/laraberg.css') }}" type="text/css" rel="stylesheet" >--}}

    <link href="{{ asset('backend/css/admin.css') }}" type="text/css" rel="stylesheet" />
</head>
<body>
    <div id="app">
        <app></app>
    </div>
    <script src="{{ asset('backend/static/tinymce4.7.5/tinymce.min.js') }}"></script>
    <script src="{{ asset('backend/js/manifest.js') }}"></script>
    <script src="{{ asset('backend/js/vendor.js') }}"></script>
    <script src="{{ asset('backend/js/admin.js') }}"></script>
    {{--<script type="text/javascript" >
        window.addEventListener('DOMContentLoaded', () => {
            setTimeout(function () {
                if (document.getElementById('textAreaArticleCreate') !== null) Laraberg.init('textAreaArticleCreate', { sidebar: true });
            }, 2000);
        })
    </script>--}}
</body>
</html>
