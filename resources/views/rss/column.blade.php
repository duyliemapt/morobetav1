<?= '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL ?>
<rss version="2.0"
     xmlns:content="http://purl.org/rss/1.0/modules/content/"
     xmlns:wfw="http://wellformedweb.org/CommentAPI/"
     xmlns:dc="http://purl.org/dc/elements/1.1/"
     xmlns:atom="http://www.w3.org/2005/Atom"
     xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
     xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
     xmlns:georss="http://www.georss.org/georss"
     xmlns:geo="http://www.w3.org/2003/01/geo/wgs84_pos#"
>
    <channel>
        <title><![CDATA[ {{ $configs['site_name'] }} ]]></title>
        <atom:link href="{{ $configs['feed_url'] }}" rel="self" type="application/rss+xml" />
        <link><![CDATA[ {{ $configs['site_url'] }} ]]></link>
        <description><![CDATA[ {{ $configs['site_name'] }} ]]></description>
        <lastBuildDate>Mon, 08 Dec 2022 08:44:03 +0000</lastBuildDate>
        <language>ja</language>
        <pubDate>{{ now() }}</pubDate>
        <sy:updatePeriod>hourly</sy:updatePeriod>
        <sy:updateFrequency>1</sy:updateFrequency>
        <generator>{{ $configs['site_url'] }}</generator>

        <image>
            <url>{{ $configs['site_url'] }}/favicon/favicon-32x32.png</url>
            <title>{{ $configs['site_name'] }}</title>
            <link>{{ $configs['site_url'] }}</link>
            <width>32</width>
            <height>32</height>
        </image>

        <atom:link rel="hub" href="https://pubsubhubbub.appspot.com"/>
        <atom:link rel="hub" href="https://pubsubhubbub.superfeedr.com"/>
        <site xmlns="com-wordpress:feed-additions:1">108122022</site>

        @foreach($posts as $post)
            <item>
                <title><![CDATA[{{ $post->title }}]]></title>
                <link>{{ $configs['site_url'] }}/column/detail/{{ $post->id }}/{{ $post->slug }}</link>
                <pubDate>{{ $post->created_at->toRssString() }}</pubDate>
                <category><![CDATA[コラム]]></category>
                <guid>{{ $post->id }}</guid>
                <description><![CDATA[{!! $post->description !!}]]></description>
                <author><![CDATA[{{ $configs['site_name'] }}]]></author>
                <post-id xmlns="com-wordpress:feed-additions:1">{{ $post->id }}</post-id>
                <dc:creator><![CDATA[{{ $configs['site_name'] }}]]></dc:creator>
            </item>
        @endforeach
    </channel>
</rss>
