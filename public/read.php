<?php

function writeLog($id) {
    $logHandle = fopen("./images.log", "a+");
    fwrite($logHandle, $id . "\r\n");
    fclose($logHandle);
}

function submitData($id, $image, $token) {
    if (function_exists('curl_file_create')) {
        $fileContent = curl_file_create("./examples/results/" . $image, 'image/png');
    } else {
        $fileContent = '@' . realpath("./examples/results/" . $image);
    }

    $postData = ['id' => $id, 'image' => $image, 'fileParam' => $fileContent, '_token' => $token];
    $curl = curl_init("http://kimono.gxo.co.jp/verify.php");
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
    $result = curl_exec($curl);
    curl_close($curl);
}

function readingData() {
    $content = file_get_contents("http://kimono.gxo.co.jp/data.json");
    if (!empty($content)) {
        $data = json_decode($content, true);
        if ($data != null && count($data) > 0) {
            $logHandle = fopen("./images.log", "r+");
            $logs = [];
            while (!feof($logHandle)) {
                $row = fgets($logHandle);
                $row = str_replace("\r\n", "", $row);
                $row = str_replace("\r", "", $row);
                $row = str_replace("\n", "", $row);
                $row = trim($row);
                if (!empty($row)) $logs[] = $row;
            }
            fclose($logHandle);

            $imagesList = [];
            $token = '';

            foreach ($data as $item) {
                if (in_array($item['id'], $logs)) continue;
                $imgInputContent = file_get_contents("http://kimono.gxo.co.jp/uploads/files/" . $item['source']);
                file_put_contents("./examples/images/" . $item['source'], $imgInputContent);
                $imgTargetContent = file_get_contents("http://kimono.gxo.co.jp/uploads/files/" . $item['target']);
                file_put_contents("./examples/images/" . $item['target'], $imgTargetContent);
                //file_put_contents("./examples/results/" . $item['output'], $imgTargetContent);

                shell_exec("python /content/sber-swap/app.py --input_file '" . $item['source'] . "' --target_file '" . $item['target'] . "' --output_file '" . $item['output'] . "'");
                $imagesList[] = ['id' => $item['id'], 'name' => $item['output']];
                $token = $item['_token'];
                print "Progress: " . $item['id'] . " -- input: " . $item['source'] . " -- target: " . $item['target'] . " -- output: " . $item['output'] . "\r\n";
                writeLog($item['id']);
            }

            foreach ($imagesList as $item) {
                submitData($item['id'], $item['name'], $token);
            }
        }
    }
}

while (true) {
    readingData();
    sleep(4);
}
