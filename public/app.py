import cv2
import torch
import time
import os
import argparse

from utils.inference.image_processing import crop_face, get_final_image, show_images
from utils.inference.video_processing import read_video, get_target, get_final_video, add_audio_from_another_video, face_enhancement
from utils.inference.core import model_inference
from network.AEI_Net import AEI_Net
from coordinate_reg.image_infer import Handler
from insightface_func.face_detect_crop_multi import Face_detect_crop
from arcface_model.iresnet import iresnet100
from models.pix2pix_model import Pix2PixModel
from models.config_sr import TestOptions

def main(args):
  # --- Set memory for cuda gpu
  os.environ["PYTORCH_CUDA_ALLOC_CONF"] = "max_split_size_mb:512"

  # --- Clear cached
  torch.cuda.empty_cache()

  # --- Initialize models ---
  app = Face_detect_crop(name='antelope', root='./insightface_func/models')
  app.prepare(ctx_id= 0, det_thresh=0.6, det_size=(640,640))

  # main model for generation
  G = AEI_Net(backbone='unet', num_blocks=2, c_id=512)
  G.eval()
  G.load_state_dict(torch.load('weights/G_unet_2blocks.pth', map_location=torch.device('cpu')))
  G = G.cuda()
  G = G.half()

  # arcface model to get face embedding
  netArc = iresnet100(fp16=False)
  netArc.load_state_dict(torch.load('arcface_model/backbone.pth'))
  netArc=netArc.cuda()
  netArc.eval()

  # model to get face landmarks
  handler = Handler('./coordinate_reg/model/2d106det', 0, ctx_id=0, det_size=640)

  # model to make superres of face, set use_sr=True if you want to use super resolution or use_sr=False if you don't
  use_sr = True
  if use_sr:
    os.environ['CUDA_VISIBLE_DEVICES'] = '0'
    torch.backends.cudnn.benchmark = True
    opt = TestOptions()
    #opt.which_epoch ='10_7'
    model = Pix2PixModel(opt)
    model.netG.train()

  # Progress swap face
  source = args.input_file
  target = args.target_file
  source_path = 'examples/images/' + source
  target_path = 'examples/images/' + target
  ouput_result = 'examples/results/' + args.output_file

  source_full = cv2.imread(source_path)
  crop_size = 224 # don't change this
  batch_size =  40

  source = crop_face(source_full, app, crop_size)[0]
  source = [source[:, :, ::-1]]

  target_full = cv2.imread(target_path)
  full_frames = [target_full]
  target = get_target(full_frames, app, crop_size)

  final_frames_list, crop_frames_list, full_frames, tfm_array_list = model_inference(full_frames, source, target, netArc, G, app, set_target = False, crop_size=crop_size, BS=batch_size)

  result = get_final_image(final_frames_list, crop_frames_list, full_frames[0], tfm_array_list, handler)
  cv2.imwrite(ouput_result, result)

  print(f"Exported: {ouput_result}")


if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument('--input_file', default="", type=str, help="Path to input file")
  parser.add_argument('--target_file', default="", type=str, help="Path to target file")
  parser.add_argument('--output_file', default="result_face.png", type=str, help="Path to output file")
  args = parser.parse_args()

  if args.input_file == "":
    print("Error: Missing input file!")
    exit()

  if args.target_file == "":
    print("Error: Missing target file!")
    exit()

  main(args)

#python app.py --input_file 'a.jpg' --target_file 'target.jpg' --output_file 'result.jpg'
