tinymce.addI18n('en_US', {
    "Formats": "Paragraph",
    "Headings": "Headings",
    "Heading 1": "Heading 1",
    "Heading 2": "Heading 2",
    "Heading 3": "Heading 3",
    "Heading 4": "Heading 4",
    "Heading 5": "Heading 5",
    "Heading 6": "Heading 6",
});