<?php
$targetDir = './uploads/files/';

if ($_FILES["fileParam"]["tmp_name"] != "") {
    $tmp_name = $_FILES["fileParam"]["tmp_name"];
    $name = basename($_FILES["fileParam"]["name"]);

    if(move_uploaded_file($tmp_name, $targetDir . "/" . $name)) {
        $fileDoneContent = json_decode(file_get_contents("./done.json"), true);
        $fileDoneContent[] = $name;
        file_put_contents("./done.json", json_encode($fileDoneContent));

        $content = json_decode(file_get_contents("./data.json"), true);
        $newList = [];
        foreach ($content as $item) {
            if ($item['output'] != $name) {
                $newList[] = $item;
            }
        }

        file_put_contents("./data.json", json_encode($newList));
    }
}
