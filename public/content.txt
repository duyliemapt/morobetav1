レンタル・下見について
++++++++++++
===========
何を着たら良いかわかりません。
_______

<p class="h01-faq_content_txt">注文方法がわからない。この着物は着て行っても大丈夫？私の体型に合うものはどれ？<br>
  などなど。 ご不安なことは何でもご相談、お問い合わせください。きもの３６５のスタッフがお答えさせていただきます。
  <a href="https://www.kimono-365.jp/contact/" class="c-link c-link-primary h01-h01-faq_content_link">お問い合わせページへ</a>
</p>
<p class="h01-faq_content_txt">シチュエーションやお立場から着物を選ぶことが可能です。
  <a href="https://www.kimono-365.jp/rental/" class="c-link c-link-primary h01-h01-faq_content_link">着物レンタルへ</a>
</p>

===========
レンタルの延長はできるの？
_______

<p class="h01-faq_content_txt">延長1日につき各種割引適用前のレンタル料金の10％を延滞料としてご請求させて頂いております。
  <a href="/guide/rental/#section04" class="c-link c-link-primary h01-h01-faq_content_link">ご利用ガイド（レンタル期間）へ</a>
</p>

===========
他には何も用意しなくてよいの？
_______

<p class="h01-faq_content_txt">セットの中には着付けに必要な小物や草履バッグまで全て含まれておりますので、ご安心ください。<br>
  体型の補整にタオルが必要ですので、4～5枚ご用意ください。<br>
  その他、こはぜ無し足袋（無料）とテトロン足袋（4枚こはぜ）をカート内でお選び頂く事ができます。
  <a href="/guide/rental/#section01" class="c-link c-link-primary h01-h01-faq_content_link">ご利用ガイド（セット内容）へ</a>
</p>

===========
着物を汚してしまった場合はどうなるの？
_______

<p class="h01-faq_content_txt">商品を汚してしまった場合でも、絶対にご自分で洗濯や補修をなさらないでください。何による汚れかをご返却シートに書き記していただき、そのままご返却ください。<br>
  著しく汚損、破損された場合は、クリーニング代、修理費等の実費をご請求させていただく場合がありますので、あらかじめご了承ください。安心パックへの加入をお勧めしています。また、修復不可能な汚損、破損があった場合、又は、紛失された場合は安心パックの適用外となりお買取りとなります。その際は当店の定める金額をご請求させて頂きます。
</p>

===========
帯は硬いタイプ・柔らかいタイプどちらが多いの？
_______

<p class="h01-faq_content_txt">基本的に現代風の柔らかいタイプの帯がほとんどですが、帯そのものの厚さや生地、縫い方によっても硬さ、柔らかさが異なってきます。また振袖の帯は近年、柔らかい芯無しの帯の方が多様な変わり結びができるので、人気が高くなっています。帯それぞれの風合いをお楽しみください。
</p>

===========
着物と草履バッグの組み合わせについて
_______

<p class="h01-faq_content_txt">草履バッグがセット内容に含まれる場合は着物のプロが着物や帯の雰囲気から選んでお送りしております。<br>
  ただし、複数の着物をレンタルされる際、同じデザインの草履バッグが届く場合がございます。<br>
  異なるデザインの草履バッグをご希望される場合、下記から選択頂けますので、ぜひご確認ください。<br>
  <a href="https://www.kimono-365.jp/rental/option/?option[]=108" class="c-link c-link-primary h01-h01-faq_content_link">レンタル高級草履バッグ</a>
  <a href="https://www.kimono-365.jp/sell/option/list/?option_sell[]=271" class="c-link c-link-primary h01-h01-faq_content_link">通販草履バッグ</a>
</p>

===========
事前に着物を見ることはできる？
_______

<p class="h01-faq_content_txt">実際にご着用になる前にお着物・帯・（訪問着、振袖のみ、帯〆・帯揚げ・重ね衿が含まれます。）をお客様のご自宅（もしくはご希望の場所に）お送りさせて頂く、「下見サービス」がございます。下見をご希望される商品のページからお申し込みください。
  品質やお顔映り等をご確認いただきまして翌日にはお返しください。
  <a href="/guide/rental/#section04" class="c-link c-link-primary h01-h01-faq_content_link">ご利用ガイド（レンタル期間）へ</a>
</p>

===========
下見って何？
_______

<p class="h01-faq_content_txt">実際の商品をお手元でご覧頂くサービスです。<br>
  下見では「着物と帯」の2点のみご覧頂けます。（訪問着、振袖のみ、帯〆・帯揚げ・重ね衿が含まれます。）</p>
<div class="c-note">
  <ul class="c-note_list">
    <li class="c-note_listItem">下見のレンタル期間は1泊2日となっております。</li>
  </ul>
</div>
<p class="h01-faq_content_txt">
  <a href="/guide/rental/#section05" class="c-link c-link-primary h01-h01-faq_content_link">下見についての詳細はこちら</a>
  <a href="/flow/" class="c-link c-link-primary h01-h01-faq_content_link">下見の流れはこちら（下見の場合を選択ください）</a>
  <a href="/guide/rental/#section07" class="c-link c-link-primary h01-h01-faq_content_link">下見のキャンセルについてはこちら</a>
</p>

===========
下見は無料ですか？
_______

<p class="h01-faq_content_txt">1枚に付き2,200円（税抜2,000円）の下見料金がかかってまいります。
<a href="https://www.kimono-365.jp/kimono_try_lp/" class="c-link c-link-primary h01-h01-faq_content_link">詳細はこちら</a>
</p>

===========
下見をしたのに借りられないということはない？
_______

<p class="h01-faq_content_txt">そのような場合もございます。<br>そのため下見の際に、実際にお召しになる日のご予約をおすすめしています。</p>
<p class="h01-faq_content_txt"><strong>■レンタルをご検討のお客様</strong><br>
下見後、本番の着用をキャンセルされる場合はお電話かメールにてお知らせください。<br>
着用日12日前まではキャンセル料はかかりません。<br>
レンタルお申し込みの際、その他お問い合わせ欄に「○月○日下見」と記載いただきましたら、下見翌日までレンタル注文を仮予約とさせて頂きます。<br>
下見翌日中にご連絡がない場合はレンタル注文を確定とさせて頂きます。</p>

<p class="h01-faq_content_txt"><strong>■購入をご検討のお客様</strong><br>
下見後、キャンセルされる場合は、お電話かメールにてお知らせください。<br>
その際、キャンセル料はかかりません。</p>


===========
本番着用日でのレンタル申し込みや通販申し込みは必ずしなければいけないの？
_______

<p class="h01-faq_content_txt">必ずしなければいけないものではございませんが、人気の商品はいつ他のお客様がレンタルなさるかわからないため、当社では本番着用日でのレンタル申し込みや通販申し込みをおすすめさせて頂いております。</p>

===========
下見の際に本番着用日でのレンタル申し込みした商品の「キャンセル」もしくは「予約決定」の場合はどうすればいいの？
_______

<p class="h01-faq_content_txt">
  キャンセルの場合のみ、お電話かメールでご連絡ください。<br>
  レンタルの場合ご着用日12日前まではキャンセル料はかかりません。<br>
  レンタルお申し込みの際、その他お問い合わせ欄に「○月○日下見」と記載いただきましたら、下見翌日までレンタル注文を仮予約とさせて頂きます。
</p>
<div class="c-note">
  <ul class="c-note_list">
    <li class="c-note_listItem">下見翌日中にご連絡がない場合はレンタルご注文を確定とさせて頂きます。</li>
  </ul>
</div>

------------------------------------
安心パック・足袋について
++++++++++++
===========
安心パックってどんなもの？
_______

<p class="h01-faq_content_txt">安心パックとは予測不能な事態により生じた汚れ破損などに対するレンタル保障となっております。<br>ですので、希望日時にお着物をきちんとお届けをするといったことや、お選びいただいたサイズがきちんと着用できるように小物などが大きいサイズで届くことをお約束する物ではございません。（こちらは安心パック内には含まれておりませんが、当社のサービスとして、きちんとしたサイズのものをお送りさせて頂いておりますので、ご安心くださいませ。）
  <a href="/guide/rental/#section03" class="c-link c-link-primary h01-h01-faq_content_link">ご利用ガイド（安心パックについて）へ</a>
</p>

===========
レンタル時に＋660円（税抜600円）で選択できるテトロン足袋（4枚こはぜ）の詳細を教えて？
_______

<p class="h01-faq_content_txt">レンタル時に＋660円（税抜600円）で選択できるテトロン足袋（4枚こはぜ）の詳細</p>

<table class="userguide_content_section_table">
  <tbody><tr>
    <th class="userguide_content_section_table_head">サイズ</th>
    <th class="userguide_content_section_table_head">親指</th>
    <th class="userguide_content_section_table_head">足幅</th>
    <th class="userguide_content_section_table_head">足首回り</th>
  </tr>
  <tr>
    <td class="userguide_content_section_table_data">22.0cm</td>
    <td class="userguide_content_section_table_data">4.0cm</td>
    <td class="userguide_content_section_table_data">7.5cm</td>
    <td class="userguide_content_section_table_data">20.0cm</td>
  </tr>

  <tr>
    <td class="userguide_content_section_table_data">22.5cm</td>
    <td class="userguide_content_section_table_data">4.0cm</td>
    <td class="userguide_content_section_table_data">7.5cm</td>
    <td class="userguide_content_section_table_data">20.0cm</td>
  </tr>

  <tr>
    <td class="userguide_content_section_table_data">23.0cm</td>
    <td class="userguide_content_section_table_data">4.0cm</td>
    <td class="userguide_content_section_table_data">7.5cm</td>
    <td class="userguide_content_section_table_data">21.0cm</td>
  </tr>
  <tr>
    <td class="userguide_content_section_table_data">23.5cm</td>
    <td class="userguide_content_section_table_data">4.0cm</td>
    <td class="userguide_content_section_table_data">7.5cm</td>
    <td class="userguide_content_section_table_data">21.0cm</td>
  </tr>
  <tr>
    <td class="userguide_content_section_table_data">24.0cm</td>
    <td class="userguide_content_section_table_data">4.0cm</td>
    <td class="userguide_content_section_table_data">7.5cm</td>
    <td class="userguide_content_section_table_data">21.0cm</td>
  </tr>

  <tr>
    <td class="userguide_content_section_table_data">24.5cm</td>
    <td class="userguide_content_section_table_data">4.0cm</td>
    <td class="userguide_content_section_table_data">8.0cm</td>
    <td class="userguide_content_section_table_data">21.0cm</td>
  </tr>

  <tr>
    <td class="userguide_content_section_table_data">25.0cm</td>
    <td class="userguide_content_section_table_data">4.0cm</td>
    <td class="userguide_content_section_table_data">8.5cm</td>
    <td class="userguide_content_section_table_data">21.0cm</td>
  </tr>
  <tr>
    <td class="userguide_content_section_table_data">25.5cm</td>
    <td class="userguide_content_section_table_data">4.0cm</td>
    <td class="userguide_content_section_table_data">8.5cm</td>
    <td class="userguide_content_section_table_data">22.0cm</td>
  </tr>

  <tr>
    <td class="userguide_content_section_table_data">26.0cm</td>
    <td class="userguide_content_section_table_data">4.5cm</td>
    <td class="userguide_content_section_table_data">8.5cm</td>
    <td class="userguide_content_section_table_data">22.5cm</td>
  </tr>


  <tr>
    <td class="userguide_content_section_table_data">26.5cm</td>
    <td class="userguide_content_section_table_data">4.5cm</td>
    <td class="userguide_content_section_table_data">9.0cm</td>
    <td class="userguide_content_section_table_data">22.5cm</td>
  </tr>

  <tr>
    <td class="userguide_content_section_table_data">27.0cm</td>
    <td class="userguide_content_section_table_data">4.5cm</td>
    <td class="userguide_content_section_table_data">9.0cm</td>
    <td class="userguide_content_section_table_data">23.0cm</td>
  </tr>

  <tr>
    <td class="userguide_content_section_table_data">28.0cm</td>
    <td class="userguide_content_section_table_data">5.0cm</td>
    <td class="userguide_content_section_table_data">9.5cm</td>
    <td class="userguide_content_section_table_data">23.5cm</td>
  </tr>

  <tr>
    <td class="userguide_content_section_table_data">29.0cm</td>
    <td class="userguide_content_section_table_data">5.0cm</td>
    <td class="userguide_content_section_table_data">10.0cm</td>
    <td class="userguide_content_section_table_data">24.5cm</td>
  </tr>

  <tr>
    <td class="userguide_content_section_table_data">30.0cm</td>
    <td class="userguide_content_section_table_data">5.0cm</td>
    <td class="userguide_content_section_table_data">10.0cm</td>
    <td class="userguide_content_section_table_data">25.0cm</td>
  </tr>

</tbody></table>

<p class="h01-faq_content_txt">さらりとした肌触りで、型崩れのしにくい生地です。</p>


------------------------------------
会員登録について
++++++++++++
===========
SMSって何?
_______

<p class="h01-faq_content_txt">SMSとは電話番号を宛先にしてメッセージのやり取りが可能なサービスです。<br>
  メールでは迷惑メール設定がされていてメールが受信が難しい方はSMS認証をお試しください。<br>
  なお、返信の届くアプリが通常のメールと異なる場合がありますのでご注意ください。
</p>

===========
どうして認証が必要なの?
_______

<p class="h01-faq_content_txt">当サイトではサービスをご利用いただくにあたりメールアドレスまたはSMSにて認証確認をおこなっております。<br>
  緊急時、確実にお客様と連絡の取れるようご登録いただくアドレスや番号にご連絡が取れることを確認するためのものです。<br>
  お手数ではございますが、お客様へ確実に商品をお届けさせていただくためにご理解とご協力をお願いいたします。
</p>

===========
メールやSMSが届かない場合は?
_______

<p class="h01-faq_content_txt">メールアドレスや電話番号の送信をいただいても返信が一向に届かない場合、迷惑メールの受信拒否設定などが考えられます。<br>
  詳しくはご利用ガイドの<a href="/guide/procedure/#section07">メールアドレス・SMS認証</a>をご確認ください。
</p>

------------------------------------
ご注文について
++++++++++++
===========
帯や小物は選べるの？
_______

<p class="h01-faq_content_txt">基本は写真の通りのコーディネートになります。<br>コーディネートの変更をご希望の場合は有料オプションよりお選びくださいませ。
  <a href="/rental/option/?mode=search&amp;sort=&amp;disp=3&amp;limit=&amp;rnd=neg&amp;keyword=&amp;keyword=&amp;option%5B%5D=121&amp;option%5B%5D=104&amp;option%5B%5D=105&amp;option%5B%5D=106&amp;option%5B%5D=122&amp;kimono%5B%5D=2&amp;kimono%5B%5D=3&amp;kimono%5B%5D=4&amp;kimono%5B%5D=6" class="c-link c-link-primary h01-h01-faq_content_link">礼装着物用　有料オプションへ</a>
  <a href="/rental/option/?mode=search&amp;sort=&amp;disp=3&amp;limit=&amp;rnd=fra&amp;keyword=&amp;keyword=&amp;option%5B%5D=121&amp;option%5B%5D=104&amp;option%5B%5D=105&amp;option%5B%5D=106&amp;option%5B%5D=122&amp;kimono%5B%5D=5" class="c-link c-link-primary h01-h01-faq_content_link">振袖用　有料オプションへ</a>
</p>

===========
商品詳細にあるカレンダーの○印が、着用日当日だけ付いているけれどその場合はレンタルできるの？
_______

<p class="h01-faq_content_txt">実際にご着用される日に○印が付いている場合はレンタルが可能です。<br>その場合も、ご着用日の2日前にお届けさせて頂きますので、ご安心くださいませ。</p>

===========
予約の確認のしかたは？
_______

<p class="h01-faq_content_txt">ご注文を承りましたら、確認メールを送らせていただきます。<br>
  万が一メールが届かない時は、当店フリーコール（0800-100-1365）までご連絡ください。<br>
  【営業時間】10:30～19:00（日曜は除く）</p>

===========
電話でも注文できるの？
_______

<p class="h01-faq_content_txt">お電話でのご注文を有料でお受けいたします。<br>
TEL：<span class="u-font-em"><a href="tel:08001001365">0800-100-1365</a></span>（日曜定休） <br>
受付時間：10:30〜13:00 14:00～19:00</p>
<p class="h01-faq_content_txt">会員登録からご注文までは1件につき550円、既に会員登録がお済になりご注文のみ承る場合は1件につき330円をご注文に加算させていただきます。</p>

===========
注文内容はいつまで変更できるの？
_______

<p class="h01-faq_content_txt">ご着用2週間を切ってのご注文内容のご変更はお受けできかねます。<br>
ご変更が必要な場合は有料（1,100円）にて承りますので当店営業時間内にお電話（<span class="u-font-em"><a href="tel:08001001365">0800-100-1365</a></span>）にてお知らせください。</p>
<p class="h01-faq_content_txt">ご変更内容によってはお受けできかねる場合がございますので何卒ご承知おきください。</p>

------------------------------------
前撮りプランについて
++++++++++++
===========
前撮りプランって何？
_______

<p class="h01-faq_content_txt">
    成人式、卒業式期間にご注文をいただいたお客様に前撮り用として、前撮り時にショールをサービスさせていただきます。
</p>

===========
成人式の1年前の前撮りでもいいの？
_______

<p class="h01-faq_content_txt">
  実際の成人式に着用する年度内の期間中のみ適用されます。<br>
  2020年1月の成人式の場合、2019年4月～2020年3月までが前撮りプランの適用可能期間となります。
</p>
<div class="c-note">
  <ul class="c-note_list">
    <li class="c-note_listItem">ただし、当サイトで成人式の期間内（1月1日～1月15日）を着用日として、振袖のレンタルをご注文頂いている方が対象です。</li>
  </ul>
</div>

------------------------------------
お支払い方法について
++++++++++++
===========
支払い方法の種類は？
_______

<p class="h01-faq_content_txt">下記の4種類がございます。<br>
  1.クレジットカード（JCB/AMEX/VISA/MASTER/Diners）　2.代金引換　3.銀行振込　4.後払い<br>
  ※ご注文日がご着用の21日以内の場合銀行振込を選択できない設定となっております。クレジットカード、代金引換もしくは、後払いよりお選びください。
  <a href="/guide/procedure/#section01" class="c-link c-link-primary h01-h01-faq_content_link">ご利用ガイド（お支払い方法）へ</a>
</p>

===========
クレジットカードの決済日はいつ？
_______

<p class="h01-faq_content_txt">クレジットカードへの請求は、ご注文が確定された時点で発生します。</p>

===========
後払いの用紙が届かないけど、どうして？
_______

<p class="h01-faq_content_txt">ご注文時に登録頂いた住所に後払いの用紙をお送りさせていただきます。<br>
お引越しをされた場合、ご登録住所を変更させていただきますので、ご連絡くださいませ。</p>

===========
後払いで注文しようとしたら、注文できない場合はある？
_______

<p class="h01-faq_content_txt">注文後のキャンセルを含め連続でご注文をいただくと一時的に後払いの利用に制限がかかることがございます。<br>
恐れ入りますが、後払いを選択した際にご注文ができない場合は下記へお問い合わせいただくようお願いいたします。<br><br>
【GMO後払いサポートデスク】<br>
Tel：<a href="tel:0570-666-350">0570-666-350</a><br class="u-hidden-lg">（平日10：00～17：00）<br>
E-Mail：<a href="mailto:support_ab@gmo-ps.com" style="text-decoration: none;">support_ab@gmo-ps.com</a>
</p>

------------------------------------
レンタル商品のお届け・送料について
++++++++++++
===========
どのような形で送られてくるの？
_______

<p class="h01-faq_content_txt">重さや持ち運びのサイズ感は<a href="/beginner/bag_size/">手持ち用バッグ・配送用バッグ・箱のサイズ感</a>をご確認ください。
</p>

===========
式場への配送もできるの？
_______

<p class="h01-faq_content_txt">結婚式場への配送も可能です。<br>
  ご注文時に、お届け先を指定してください。着付け先の美容室等へも配送が可能です。<br>
  （その場合は予め配送先へ連絡しておいてください。）
</p>

===========
何日前に届くの？
_______

<p class="h01-faq_content_txt">商品のご確認を安心して行っていただける2日前にお届けいたします。<br>3日前着などのご希望がございましたら、ご着用日の2週間前までにご連絡ください。</p>

===========
送料負担はいくら？
_______

<p class="h01-faq_content_txt">レンタル・下見・通販ともに、注文番号（同一配達日・同一配送先ご注文分）の合計額が税込10,000円以上の場合、送料は当社負担でお届けいたします。<br>
合計額が税込10,000円に満たない場合は、注文番号ごとに全国一律770円（税抜700円）発生いたします。<br>
レンタル・下見に限りご返却の際の送料は当社負担となります。着払い伝票をご利用ください。
</p>
<div class="c-note">
  <ul class="c-note_list">
    <li class="c-note_listItem">1つの受付番号で注文番号が2つ以上ある場合、送料がお客様負担のご注文と当社負担のご注文が発生することがございます。</li>
  </ul>
</div>

<div class="h01-content_section_table-sm_scroll">
  <table class="h01-content_section_table">
    <tbody><tr>
      <th class="h01-content_section_table_head"></th>
      <th class="h01-content_section_table_head">お届け時の送料</th>
      <th class="h01-content_section_table_head">返却時の送料<br>（レンタル・下見）</th>
    </tr>
    <tr>
      <th class="h01-content_section_table_head">合計額が税込10,000円に満たない場合</th>
      <td class="h01-content_section_table_data">770円（税抜700円）</td>
      <td rowspan="2" class="h01-content_section_table_data">当社負担<br>（お着物に同梱してある返却用の伝票をお使いください）</td>
    </tr>
    <tr>
      <th class="h01-content_section_table_head">注文番号<br class="u-hidden-lg">（同一配達日・同一配送先ご注文分）の<br>合計額が税込10,000円以上の場合</th>
      <td class="h01-content_section_table_data">当社負担</td>
    </tr>
  </tbody></table>
</div>


===========
天災などによる配送遅延のお知らせはありますか？
_______

<p class="h01-faq_content_txt">恐れ入りますがヤマト運輸のホームページからご確認いただけます。</p>
<p class="h01-faq_content_txt"><a href="https://www.kuronekoyamato.co.jp/" target="_blank" class="c-link-blank">ヤマト運輸ホームページ</a></p>

===========
不測の事態が発生した場合は？
_______

<p class="h01-faq_content_txt">自然災害の状況や交通トラブルによりどうしてもご指定頂いた日時にお届けできない可能性がございます。その際はオペレーターよりご相談させていただきます。</p>

------------------------------------
ご返却について
++++++++++++
===========
返却はどうすればよいの？
_______

<p class="h01-faq_content_txt">商品について：汗をかいても、洗濯不要です。肌着などそのままお返しください。<br>
  発送について：必ずヤマト運輸をご利用ください。
</p>
<div class="c-note">
  <ul class="c-note_list">
    <li class="c-note_listItem">お近くのヤマト運輸取扱いコンビニ等でご返却して頂く際は、同封の返却伝票は取り扱えないためコンビニにある赤い着払い伝票をお使いください。<br>
      クロネコヤマトフリーダイヤルに電話して集荷を依頼することもできます。</li>
    <li class="c-note_listItem">地域によってはすぐ集荷に伺えない場合がございますので事前にご連絡して頂くことをおすすめ致します。<br>
      フリーダイヤル：0120-01-9625（携帯電話からのご連絡は：0570-200-000）
      <a href="/guide/rental/#section12" class="c-link c-link-primary h01-h01-faq_content_link">ご利用ガイド（ご返却方法）へ</a>
    </li>
  </ul>
</div>
<p class="h01-faq_content_txt">過不足について：返却の際、商品の内容に過不足があった場合、確認後すぐにご連絡させて頂きます。なお、お客様の私物などが合った場合は、ご連絡後、1週間以内にご返却させて頂きます。</p>

===========
着物を返す時に、私物を一緒に送ってしまった場合はどうしたらいい？
_______

<p class="h01-faq_content_txt">
着物のご返却確認の際に貴重品（現金・カード類・貴金属類・電子機器・鍵など）を発見しましたら、まずは確認のご連絡をさせていただき、お送りさせていただきます。<br>
それ以外の私物は 誠に勝手ながら3ヶ月保管後、ご連絡がない場合は破棄させていただきます。</p>
<p class="h01-faq_content_txt">また、食料品など一部私物につきましては3ヶ月の保管期限を待たず破棄させていただく場合がございます。何卒ご了承くださいませ。</p>
<p class="h01-faq_content_txt">大変恐縮ではございますが、私物のご返却の送料はお客様負担でお願いいたします。</p>

===========
返却時、どのように収納したらよいの？
_______

<p class="h01-faq_content_txt">ご返却いただく際の収納方法については以下のページに詳細を記載しておりますのでご確認ください。
<a href="/please/storage" class="c-link c-link-primary h01-h01-faq_content_link">返却時の収納方法</a>
</p>

===========
着物を返却した後に連絡は来るの？
_______

<p class="h01-faq_content_txt">商品が当店に戻りましたら、返却完了メールをお送りしますのでご確認ください。<br>返却時の混雑状況によってはメールをお送りするまでに2週間程度かかる場合がございます。</p>

------------------------------------
キャンセル・交換について
++++++++++++
===========
キャンセル料はかかるの？
_______

<p class="h01-faq_content_txt">レンタル商品をキャンセルされる場合、ご着用日の11日前から取消手数料が発生いたします。<br>
  通販商品の購⼊をキャンセルされる場合はキャンセル料は発生いたしません。
</p>

===========
レンタル商品のキャンセル料はいくらかかる？
_______

<p class="h01-faq_content_txt">ご予約確定後、お客様のご都合によるキャンセルの場合は、下記の通り取消手数料が発生いたします。</p>
<dl class="h01-faq_content_definitionList">
  <dt class="h01-faq_content_definitionTerm c-heading-6">ご予約確定後から、ご着用日の12日前まで<br>
    <small>※但し着用日当日を含め12日無い場合は適応外となります。</small></dt>
  <dd class="h01-faq_content_definitionDesc">
    <ul class="c-bulletList">
      <li class="c-bulletList_listItem">取消手数料：無料</li>
    </ul>
  </dd>
</dl>
<dl class="h01-faq_content_definitionList">
  <dt class="h01-faq_content_definitionTerm c-heading-6">ご着用日の11日前から、7日前まで</dt>
  <dd class="h01-faq_content_definitionDesc">
    <ul class="c-bulletList">
      <li class="c-bulletList_listItem">取消手数料：各種割引適用前のレンタル料金の30％</li>
    </ul>
  </dd>
</dl>
<dl class="h01-faq_content_definitionList">
  <dt class="h01-faq_content_definitionTerm c-heading-6">ご着用日の6日前から</dt>
  <dd class="h01-faq_content_definitionDesc">
    <ul class="c-bulletList">
      <li class="c-bulletList_listItem">取消手数料：各種割引適用後のレンタル料金の100％</li>
    </ul>
  </dd>
</dl>
<p class="h01-faq_content_txt"><a href="/guide/rental/#section07" class="c-link c-link-primary h01-h01-faq_content_link">ご利用ガイド（キャンセルについて）へ</a></p>

===========
レンタル商品到着後に交換はできるの？
_______

<p class="h01-faq_content_txt">レンタル商品の当社出荷後のキャンセル・返品は受け付けておりません。<br>
  但し、お届けされた商品が不良の場合やご注文いただいた物と異なる場合等は、即日正しい商品をお送りいたします。場合によっては発送できない事もありますのでその際はレンタル料金を全額ご返金致します。
</p>

===========
私が着る前に着ていた人が、汚してしまった場合でもレンタルはできるの？
_______

<p class="h01-faq_content_txt">お手入れをさせて頂きますが、万が一ご着用に支障が出る場合にはお客様に事前に確認を頂き商品の変更をお願いしております。</p>

===========
仕立後の商品はキャンセルできるの？
_______

<p class="h01-faq_content_txt">仕立が開始後はキャンセル不可となりますのでご注意ください。</p>

------------------------------------
レンタル期間について
++++++++++++
===========
家に届いてから（もしくはホテルなどに届いた日から）5日後に着物が配送センターに届いていなければいけないの？
_______

<p class="h01-faq_content_txt">4泊5日の期間はお客様のお手元に置いておける日数となっております。<br>
5日目にヤマト運輸にお着物をお渡しいただければ問題ございません。</p>

------------------------------------
サイズについて
++++++++++++
===========
ふくよかなので自分の身長に関わらずLLサイズでいいの？
_______

<p class="h01-faq_content_txt">ふくよかな方はヒップ98㎝以上（MO.LO）と書かれている商品の中からお着物をお選び下さいませ。</p>

===========
記入してある身長以外の身長だと絶対に着られないの？
_______

<p class="h01-faq_content_txt">決してそのようなことはございませんが、より美しくお着物を着て頂けるサイズを提示させて頂いておりますので、なるべく適応身長内でお着物をお選びくださいませ。</p>

===========
襦袢のサイズが着物とあってないのはどうして？
_______

<p class="h01-faq_content_txt">通販の場合：長襦袢の商品名についているサイズ表記（例：訪問着・留袖用長襦袢<u>LLサイズ</u>）はきもの３６５の着物のサイズに合わせているため、襦袢そのものについているサイズタグの表記とは異なる場合もございます。<br>
商品詳細に表記してあるサイズをご確認いただく事をおすすめしております。</p>

------------------------------------
通販商品のお届け・送料について
++++++++++++
===========
送料負担はいくら？
_______

<p class="h01-faq_content_txt">注文番号（同一配達日・同一配送先ご注文分）の合計額が税込10,000円以上の場合、送料は当社負担でお届けいたします。<br>合計額が税込10,000円に満たない場合は、注文番号ごとに全国一律770円（税抜700円）発生いたします。</p>
<div class="c-note">
  <ul class="c-note_list">
    <li class="c-note_listItem">1つの受付番号で注文番号が2つ以上ある場合、送料がお客様負担のご注文と当社負担のご注文が発生することがございます。</li>
  </ul>
</div>

<div class="h01-content_section_table-sm_scroll">
  <table class="h01-content_section_table">
    <tbody><tr>
      <th class="h01-content_section_table_head"></th>
      <th class="h01-content_section_table_head">お届け時の送料</th>
    </tr>
    <tr>
      <th class="h01-content_section_table_head">合計額が税込10,000円に満たない場合</th>
      <td class="h01-content_section_table_data">770円（税抜700円）</td>
    </tr>
    <tr>
      <th class="h01-content_section_table_head">注文番号<br class="u-hidden-lg">（同一配達日・同一配送先ご注文分）の<br>合計額が税込10,000円以上の場合</th>
      <td class="h01-content_section_table_data">当社負担</td>
    </tr>
  </tbody></table>
</div>


===========
どのような形で送られてくるの？
_______

<p class="h01-faq_content_txt">シワにならないように細心の注意を測り、段ボールでお送りさせていただいております。<br>着物に影響がある湿度が含まれないよう、なるべく余分な紙や包装を避けてお送りしております。</p>

===========
天災などによる配送遅延のお知らせはありますか？
_______

<p class="h01-faq_content_txt">恐れ入りますがヤマト運輸のホームページからご確認いただけます。</p>
<p class="h01-faq_content_txt"><a href="https://www.kuronekoyamato.co.jp/" target="_blank" class="c-link-blank">ヤマト運輸ホームページ</a></p>

------------------------------------
通販商品のキャンセル・返品について
++++++++++++
===========
キャンセル料はかかるの？
_______

<p class="h01-faq_content_txt">通販商品の購⼊をキャンセルされる場合はキャンセル料は発生いたしません。</p>

===========
購入した商品の返品はできるの？
_______

<p class="h01-faq_content_txt">返品専用のフォームがご注文確定メールに記載されておりますので、必要事項をご記入の上ご申請ください。商品お届け後7日以内のご申請とお届け後10日以内のご発送をお願いいたします。<br>
  返品申請期間を過ぎた場合やご着用後のご返品、お客様のお手元で汚損ないし破損された場合など当社規定によりお受けできない場合があります。返品申請フォームよりご確認下さい。お客様のご都合による返品の場合には送料はお客様のご負担となります。ご了承ください。</p>

------------------------------------
着付け場所について
++++++++++++
===========
着付けの予約は会員登録が必要？
_______

<p class="h01-faq_content_txt">着付け場所のご予約のみの場合は、会員登録は不要です。</p>

===========
着付けの予約をしたいのだけれど、どうしたら良いの？
_______

<p class="h01-faq_content_txt">着付を希望するお店へ直接お電話またはメールをし、ご予約をお願いいたします。</p>

===========
着付けの予約をしたら着物の予約もできるの？
_______

<p class="h01-faq_content_txt">着付けのご予約と着物のご予約はまったく別のご予約になります。どちらもお客様自身でそれぞれのお手続きが必要です。きもの３６５で着物をレンタルし、きもの３６５内での掲載店舗で着付けをご希望の方は、その旨を着付け希望の店舗へ直接お伝えいただくとよりスムーズにご予約いただけます。</p>

===========
予約をした着付け場所にレンタルした着物を直接届けることはできるの？
_______

<p class="h01-faq_content_txt">レンタル着物を直接、着付け場所にお届けすることは可能ですが、事前にお客様自身で着付け場所に確認のお電話をしていただいた後、レンタル注文時の着物のお届け先を着付け場所の住所に設定してください。着物のお届け日は着用日の2日前になります。その旨も着付け場所にお伝えください。</p>

===========
着付け場所をもっと多くから検索できる？
_______

<p class="h01-faq_content_txt">きもの３６５ではありませんが、以下のサイトに多くの着付け可能な店舗が掲載されております。着付け場所の検索は以下のサイトをご利用ください。</p>
<ul class="c-links_gaibu">
    <li>
        <p><a href="https://beauty.hotpepper.jp/" target="_blank"><img src="/assets/img/banner/Beauty_Logo_270_90.gif" alt=""></a></p>
        <p><a href="https://beauty.hotpepper.jp/" target="_blank" class="c-link-blank">ホットペッパービューティー</a></p>

    </li>

    <li>
        <p><a href="https://www.beauty-navi.com/" target="_blank"><img src="/assets/img/banner/beautynavi_234_60.gif" alt=""></a></p>
        <p><a href="https://www.beauty-navi.com/" target="_blank" class="c-link-blank">美容室予約・ヘアスタイル検索サイト&nbsp;Beauty navi</a></p>
    </li>
</ul>

------------------------------------
七五三着物について
++++++++++++
===========
着物サイズが心配です。
_______

<p class="h01-faq_content_txt">基本的なサイズは下記のようになっています。<br>
【3歳】適用身長90㎝～100㎝<br>
【5歳】適用身長100㎝～115㎝<br>
【7歳】適用身長110㎝～125㎝<br>
商品名に年齢が入っていますので、着物の商品名をご確認ください。詳細は商品詳細内をご確認ください。</p>

===========
着物の寸法を変更するために縫い直しや解いたりしても良いですか？
_______

<p class="h01-faq_content_txt">肩上げや、裾上げ（見上げ）部分の寸法調整のために解いたり縫い直たりすることは、他のお客様のご利用に支障が出るといけませんので、お断りさせていただいております。<br>
肩上げはしておりますが、お子様の袖の長さが若干短いことや長いことをある程度は許容範囲とされております。商品ページに記載されている着物の寸法をご確認いただいたうえでご検討ください。<br>
ご不安な場合はお問い合わせください。</p>

===========
長襦袢に半襟は付いてますか？カット袖とはどんなもの？
_______

<p class="h01-faq_content_txt">長襦袢には半襟が縫い付けられた状態でお送りしております。<br>
長襦袢の袖をカットしておりますので、着付けがしやすくなっております。</p>

===========
足袋のサイズがわかりません。
_______

<p class="h01-faq_content_txt">足袋は、普段ご利用の靴のサイズを注文時に入力いただければ問題ありません。ご心配の場合は１サイズ大きめのサイズをご選択下さい。七五三のお子様足袋はストレッチ素材なので、伸び縮みすることできるので、簡単に履けます。<br>
足袋のサイズは13-14cm、15-16cm、17-18cm、19-20cm、21-22cmの5タイプとなります。</p>
<p class="h01-faq_content_txt">例）足袋サイズ16cmを選択した場合は15-16cm用の足袋がついてきます。</p>

===========
草履が痛くなりませんか？
_______

<p class="h01-faq_content_txt">比較的履きやすい草履となっておりますが、ご不安な方は、移動時は普段の運動靴をお履きいただき、お式のときだけ草履に履き替えるなどもおすすめです。<br>
※マジックテープ付きの靴の場合は着物の引き連れにご注意ください。<br>
マジックテープのないスリッポンなどのご着用をお勧めしております。</p>

===========
事前に用意しておくものはありますか？
_______

<p class="h01-faq_content_txt">七五三着物レンタルは全てフルセットになっております。<br>長襦袢の下に着る肌着のご用意をお願いいたします。</p>

===========
7歳女の子の帯は自分で結べますか？
_______

<p class="h01-faq_content_txt">7歳女の子の帯は全て作り帯となります。<br>
出来上がりの帯となり、解くこともできませんので、商品画像の帯の形以外に変更することはできません。</p>

===========
3歳・7歳女の子のセットに髪飾りはついていますか？
_______

<p class="h01-faq_content_txt">着物と一緒に髪飾りもお送りしております。<br>商品詳細ページの【セット内容】に掲載されている髪飾り写真はイメージです。実際お送りする商品と異なる場合があります。</p>
<p class="h01-faq_content_txt">お好みの髪飾りをお使いになりたい場合、販売品もご用意しております。<br>どうぞご覧ください。</p>
<p class="h01-faq_content_txt"><a href="https://www.kimono-365.jp/sell/option/list/?sort=create_datetime%3Adesc&amp;kimono%5B%5D=392&amp;option_sell%5B%5D=273&amp;sale_price_option%5B%5D=" class="c-link c-link-primary h01-h01-faq_content_link">七五三（子ども用）　髪飾り一覧</a></p>

===========
七五三着物の着付けは初心者でもできますか？
_______

<p class="h01-faq_content_txt">着付け方法のご案内を商品に同梱しておりますので、初心者の方でもお着付けは可能です。<br>
7歳のお着付けは事前に練習なさることをおすすめいたします。<br>
ご不安な場合は、プロにお任せください。</p>

===========
コーディネートに使用している帯・被布・袴、その他の小物を変更することはできますか？
_______

<p class="h01-faq_content_txt">原則、写真の通りのコーディネートでお送りしております。<br>コーディネートの変更をご希望の場合、有料オプションまたは、販売小物よりお選びください。</p>
<p class="h01-faq_content_txt"><a href="/rental/option/?kimono[]=392" class="c-link c-link-primary h01-h01-faq_content_link">七五三（子ども用）　有料オプション</a></p>
<p class="h01-faq_content_txt"><a href="/sell/option/list/?sort=create_datetime%3Adesc&amp;kimono%5B%5D=392" class="c-link c-link-primary h01-h01-faq_content_link">七五三（子ども用）販売小物一覧</a></p>

===========
衣裳を汚してしまったら、どうすればいいの？心配です・・・
_______

<p class="h01-faq_content_txt">飲み物などをこぼしてしまった場合や、汚れ等がついてしまった場合でも、決してこすらずハンカチ等で軽くおさえて吸い取る程度になさってください。</p>
<p class="h01-faq_content_txt"><a href="/guide/rental/#section09" class="c-link c-link-primary h01-h01-faq_content_link">詳細はこちら</a></p>
<p class="h01-faq_content_txt">マジックテープ付きの靴を履かれる場合は、マジックテープが着物に付かないようご注意ください。着物の裾部分が引き連れを起こす可能性があります。もしそのような場合は修繕費を頂くか買取をお願いしております。</p>
<p class="h01-faq_content_txt"><a href="/guide/rental/#section11" class="c-link c-link-primary h01-h01-faq_content_link">詳細はこちら</a></p>

===========
商品の小物を紛失した場合はどうなりますか？
_______

<p class="h01-faq_content_txt">ハコセコ・懐剣・扇子・髪飾りを紛失された場合は安心パック対象外となっており、実費を請求させていただいております。<br>
例えば筥迫(はこせこ)を紛失した場合、筥迫のみで仕入れることができないので、筥迫セットで仕入れが必要となります。<br>
筥迫と帯は同じ柄の物が多く、筥迫だけ全く違う物にすることができません。また、ブランド物の小物を紛失された場合は、一式請求させていただいております。<br>
結果1万円から3万円ぐらいかかることもあります。
紛失しないようにお気をつけ下さい。</p>

===========
ハコセコとビラカンの取り扱いについて
_______

<p class="h01-faq_content_txt">ハコセコとビラカンはセットになっているので外さないようご協力ください。万が一紛失された場合は請求させていただきます</p>

===========
5歳袴のへらがない場合はどうしたらいいの？
_______

<p class="h01-faq_content_txt">袴のヘラはなくても、サスペンダーがあれば問題ありませんので、ご安心ください。サスペンダーはセット内容に含まれております。</p>

===========
7歳の子に補整は必要？
_______

<p class="h01-faq_content_txt">基本は寸胴にした方が美しいとされています。また、紐などで苦しくないようにある程度補整した方が良いとされております。補整用のタオルをご準備ください。</p>

===========
懐剣や羽織紐はどう扱ったらいいの？
_______

<p class="h01-faq_content_txt">懐剣や羽織紐などの小物は解かないでそのままご使用ください。</p>

------------------------------------
その他
++++++++++++
===========
新型コロナウイルス感染症対策ついて
_______

<p class="h01-faq_content_txt">きもの３６５では、新型コロナウイルスの対策として、下記の安全対策を徹底して行っております。</p>

<div class="c-note">
<p>①着物など衣類に残るウイルスの感染力は2日間</p>
    <ul class="c-bulletList">
    <li class="c-bulletList_listItem">着物などの衣類関係は最長2日間ウイルスの感染力があると言われています。<br>当店ではご着用の後、2日間経過してからお着物の点検やクリーニングを行い、最短でも2週間間隔をあけて次の方へお貸出しております。</li>
    <li class="c-bulletList_listItem">高温アイロン、高温スチーム、高温乾燥による高温殺菌処理を行っております。</li>
    </ul>
</div>

<div class="c-note">
<p>②小物類などプラスチック製品に残るウイルスの感染力は7日間</p>
    <ul class="c-bulletList">
    <li class="c-bulletList_listItem">プラスチック製品については最長7日間ウイルスの感染力があると言われています。当店ではご着用の後、最短でも2週間間隔をあけて次の方へお貸出しております。</li>
    <li class="c-bulletList_listItem">長襦袢、肌襦袢、裾除け、足袋など直接肌に触れる小物はすべて試験で効果が認められた界面活性剤配合の洗剤、除菌漂白剤を使用して洗濯しております。</li>
    <li class="c-bulletList_listItem">熱耐性のあるものは高温アイロン、高温スチーム、高温乾燥による高温殺菌処理も同時に行っておりますし、熱耐性のないものは除菌スプレーや除菌ペーパーなどにより除菌処理もしております。</li>
    </ul>
</div>

<div class="c-note">
<p>③弊社社員についても毎日検温と体調チェックを行います。</p>
    <ul class="c-bulletList">
    <li class="c-bulletList_listItem">社内感染がおこらないように弊社従業員におきましては、毎日の検温とマスクの着用、アルコール消毒とソーシャルディスタンス、換気対策と体調管理を徹底して行うマニュアルを作成、実施しております。また、社内感染にも常に細心の注意を払っており、万全の態勢で作業に従事しております。</li>
    <li class="c-bulletList_listItem">常に最新の情報を取得し、最新の情報に基づいたマニュアルへ変更し実施しております。</li>
    </ul>
</div>

===========
会員登録の認証メールや注文完了メールが届かない場合は？
_______

<p class="h01-faq_content_txt">何らかのご都合でメールを受信できない場合は、ご注文の際、備考欄に受信できない旨をご記入して頂けますと、お電話で対応させて頂きます。</p>

===========
商品を店舗でみられる？
_______

<p class="h01-faq_content_txt">当店は実店舗がございません。商品を事前にご確認されたい方は下見をご利用ください。</p>

===========
メールマガジンが届かない場合は？
_______

<p class="h01-faq_content_txt">月に数回の配信になりますので、会員登録をされたタイミングによっては翌月まで届かない場合があります。<br>
翌月になっても届かない場合は、<a href="/guide/service/#section06">ご利用ガイド</a>をご確認ください。magazine-noreply@kimono-365.net　からのメールを受信できるようドメイン設定をお願いいたします。</p>

===========
旧漢字が入りません。
_______

<p class="h01-faq_content_txt">大変申し訳ございませんが、当サイトは旧漢字に対応しておりません。<br>
  ご注文の際にご入力をいただきましても、弊店へ正しく送信されない場合があります。<br>
  お手数ですが、新字体をご利用いただきますようお願いいたします。</p>
              