<?php
$filePath = "/var/www/html/uramori.jp/public/uploads/image";
$urlPath = "https://uramori.jp/uploads/image/";

$files = dirToArray($filePath);
$content = readingFiles($files, $urlPath);
file_put_contents("urls.txt", $content);

function readingFiles($files, $urlPath) {
    $content = "";
    foreach ($files as $key => $file) {
        if (!is_array($file)) {
            $content .= $urlPath . $file . "\r\n";
            //print "File: " . $urlPath . $file . "\r\n";
        } else {
            $content .= readingFiles($file, $urlPath . $key ."/");
            $folderName = $urlPath . $key;
            $folderName = str_replace("https://uramori.jp/uploads/image/", "", $folderName);
            print "File: " . $folderName . "\r\n";
        }
    }
    return $content;
}

function dirToArray($dir)
{
    $result = array();
    $cdir = scandir($dir);
    foreach ($cdir as $key => $value) {
        if (!in_array($value, array(".", ".."))) {
            if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) {
                $result[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value);
            } else {
                $result[] = $value;
            }
        }
    }

    return $result;
}