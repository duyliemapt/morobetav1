<?php
$filePath = "/var/www/html/uramori.jp/public/uploads/image/";
$urlPath = "https://uramori.jp/uploads/image/";

$handle = fopen("https://uramori.jp/uploads/urls.txt", "r");
if ($handle) {
    while (!feof($handle)) {
        $str = trim(fgets($handle));
        if ($str != "" && $str != null) {
            $savePath = str_replace($urlPath, "", $str);
            $baseName = basename($savePath);
            $uri = str_replace($baseName, "", $savePath);
            if (!is_dir($filePath . $uri)) mkdir($filePath . $uri, 0777);
            $imgContent = file_get_contents($str);
            file_put_contents($filePath . $savePath, $imgContent);
            print "Path: " . $filePath . $savePath . "\r\n";
        }
    }
    fclose($handle);
}
